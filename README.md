# WOLDAT #

### Multiplayer 2D shooter ###

A multiplayer platform shooter made with Unity using C#-script.

A mix between Soldat and Worms with the best of two worlds.

* Local multiplayer in Worms-mode (turn based)
* Network multiplayer in Soldat-mode (realtime)

![woldat-2-600.jpg](https://bitbucket.org/repo/54xBEA/images/3910011125-woldat-2-600.jpg)

![woldat-1.jpg](https://bitbucket.org/repo/54xBEA/images/1092409072-woldat-1.jpg)

Graphics used in the gamee are taken from [Glitch](https://www.glitchthegame.com) under CC0 1.0 license.