﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;
public class CpuMovement : MonoBehaviour
{
    public AiTargetMode targetMode;
    public LayerMask obsticles;

    private float _moveSpeed = 5;
    private float _jumpForceX = 95;
    private float _jumpForceY = 270;
    private float _maxLedgeHeight = 2.0f;

	private float _fallOffCliffOffset = 1f;
	private float _jumpDistance = 3f;
	private float _jumpCliffDistance = 1f;
	private Vector2 _ledgeOffset = new Vector2(0, 0.5f);

    private float _preferedDirection = 1;
	private Vector2 _lastPos = Vector2.zero;
	private float _lastDirection = 1;
	private int _stompCount;
	private int _blockCount;

    private GameObject _target;
    private Rigidbody2D _body;
    private PlayerManager _manager;
	private List<Waypoint> _waypoints;
	private List<Waypoint> _frontier;
	private List<int> _explored;
	private Waypoint _targetWp;
	private Vector2 _lastPosition;

	class Waypoint
	{
		public Vector2 endUpPos
		{
			get
			{
				return _endUpPos != Vector2.zero ? _endUpPos : pos;
			}
			set
			{
				_endUpPos = value;
			}
		}

		public Vector2 pos;
		public WpType type;
		public Waypoint parent;

		public float traveledDistance;
		public float minDistanceToTarget;
		public bool targetReached;

		private Vector2 _endUpPos;

		public Waypoint()
		{
			pos = endUpPos = Vector2.zero;
			minDistanceToTarget = float.MaxValue;
			traveledDistance = 0f;
		}

		public override int GetHashCode()
		{
			return (int)((pos.x * 10)) + (int)(pos.y)*10;
		}

		public enum WpType { Ground, Ledge, Fall };
	}

	void Start ()
	{
	    _manager = GetComponent<PlayerManager>();
	    _body = GetComponent<Rigidbody2D>();

		_manager.CurrentPlayer = false;
		_waypoints = new List<Waypoint>();
		_explored = new List<int>();

	}

    private void AcquireTarget()
    {
        Debug.Log("Acquiring target...");
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        if (players.Length == 0)
            return;

        GameObject closestPlayer = players[0];
        double closestDistance = Double.MaxValue;
        foreach (GameObject player in players)
        {
            double distance = Vector3.Distance(player.transform.position, gameObject.transform.position);
            if (distance < closestDistance && distance > 0)
            {
                closestDistance = distance;
                closestPlayer = player;
            }
        }

        Debug.Log("Target acquired!");
        _target = closestPlayer;
    }

    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.U))
            PlanRoute();

		if (Input.GetKeyDown (KeyCode.Y)) {
			AcquireTarget ();
		}

	    if (_target == null)
	        return;
		
    }

    private void PlanRoute()
    {
        _waypoints.Clear();
		StopAllCoroutines ();
		StartCoroutine(goPlan());
    }

	IEnumerator goPlan()
	{
		yield return new WaitForSeconds (0.2f);
		Waypoint wp = new Waypoint ();
		wp.pos = transform.position;

		if (!Search(wp)) {

			yield return new WaitForSeconds (1);
			StartCoroutine (goPlan());
			yield break;
		}

		Debug.Log ("Constructing path");
			
		ConstructPath();

		_lastPosition = transform.position;
		for (int i = _waypoints.Count - 1; i > 0; i--)
		{
			Waypoint nextWp = _waypoints [i];
			Debug.DrawLine(_waypoints[i].pos, _waypoints[i - 1].endUpPos, Color.red, 1);
			if (!GotoWaypoint (nextWp)) {
				i++;
				_stompCount++;
			} else {
				_stompCount = 0;
			}
			if (nextWp.type == Waypoint.WpType.Ground)
				yield return new WaitForSeconds(0.15f);
			else
				yield return new WaitForSeconds(0.4f);

			if (_stompCount > 3) {
				StartCoroutine (goPlan());
				break;
			}
			
		}
	}

	private bool GotoWaypoint(Waypoint wp)
	{
		Vector2 myPos = transform.position;

		Debug.Log("Go to wp: " + wp.type + ", " + wp.pos + ", distance: " + Vector2.Distance (wp.endUpPos, myPos));
		if (wp.type == Waypoint.WpType.Ledge)
		{
			Jump (wp.endUpPos);
		}

		if (wp.type == Waypoint.WpType.Ground)
		{
			MoveTowards (wp.pos);
		}

		if (wp.type == Waypoint.WpType.Fall)
		{
			MoveTowards (wp.pos);
		}


		if (Vector2.Distance (wp.endUpPos, myPos) < 0.7f)
			return true;

		return false;
	}

	private void ConstructPath()
	{
		_waypoints.Clear();
		Waypoint wp = _targetWp;
		while (wp.parent != null)
		{
			_waypoints.Add(wp);
			wp = wp.parent;
		}
	}

	private bool Search(Waypoint start)
	{

		_frontier = new List<Waypoint>();
		_frontier.Add(start);
		_explored.Clear();

		int cnt = 0;
		while (_frontier.Count > 0)
		{
			Waypoint current = NextWaypoint();
			_explored.Add (current.GetHashCode());
			_frontier.Remove(current);

			Debug.Log ("Tests " + ++cnt);
			if (current.targetReached)
			{
				_targetWp = current;
				return true;
			}

			if (current.parent != null)
			{
				Debug.Log("Best wp at: " + current.endUpPos);
				//yield return new WaitForSeconds(0.1f);
			}

			foreach (Waypoint adjWp in ActionsFrom(current))
			{
				if (!_explored.Contains (adjWp.GetHashCode ()))
					_frontier.Add (adjWp);
				else
				{
					Debug.DrawLine (current.pos, adjWp.pos, Color.black, 3f);
				}
			}

			if (_frontier.Count > 1000 || cnt > 400)
			{
				return false;;
			}
		}
		return false;
	}

	Waypoint NextWaypoint()
	{
		Vector2 targetPos = _target.transform.position;
		Waypoint closestWp = _frontier[0];
		double shortestDistance = Double.MaxValue;

		foreach (Waypoint wp in _frontier)
		{
			if (wp.minDistanceToTarget < shortestDistance && wp.endUpPos != Vector2.zero && Random.Range(0,1) < 0.5f)
			{
				shortestDistance = wp.minDistanceToTarget;
				closestWp = wp;
			}
		}
		return closestWp;
	}

	private List<Waypoint> ActionsFrom(Waypoint wp, float[] bounds = null, Vector2[] ledges = null, int dir = -1)
	{
		
		List<Waypoint> actions = new List<Waypoint>();
		Vector2 targetPos = _target.transform.position;
		Vector2 pos = GroundBelowFall(wp.endUpPos + new Vector2 (0.1f, 0), dir) + new Vector2 (0.1f, 0); //wp.pos;

		if (bounds == null)
			bounds = GroundBounds (pos);
		if (ledges == null)
			ledges = ClosestHighGround (pos, bounds [0], bounds [1]);

		Vector2 ledge = (dir == 1) ? ledges[1] : ledges[0];
		float bound = (dir == 1) ? bounds[1] : bounds[0];

		float distance = Vector2.Distance(targetPos, pos);
		float xDistance = Mathf.Abs(targetPos.x - pos.x);
		float yDistance = Mathf.Abs(targetPos.y - pos.y);

		//Target in sight
		if (distance <= bound && !Physics2D.Raycast (pos + new Vector2 (0, 0.1f), (targetPos - (pos + new Vector2 (0, 0.1f))), bound, obsticles.value))
		{
			Waypoint action = new Waypoint();
			action.pos = targetPos;
			action.type = Waypoint.WpType.Ground;

			if (Vector2.Distance(action.endUpPos, targetPos) < 2f)
			{
				Debug.DrawLine(action.endUpPos, targetPos, Color.cyan, 10);
				action.targetReached = true;
			}
			
			actions.Add(action);
		}

		if (Vector2.Distance(ledge, pos) <= _jumpDistance) 
		{
			Waypoint action = new Waypoint();
			action.pos = ledge + new Vector2 (_ledgeOffset.x * dir, _ledgeOffset.y);
			action.type = Waypoint.WpType.Ledge;
			actions.Add(action);
		}
		else if (ledge != Vector2.zero)
		{
			Waypoint action = new Waypoint();
			action.pos = GroundBelowLedge(ledge, dir);
			action.endUpPos = ledge + _ledgeOffset;
			action.type = Waypoint.WpType.Ground;
			actions.Add(action);
		}


		RaycastHit2D block = Physics2D.Raycast(pos + new Vector2(0, 1f), new Vector2(1, 0) * dir, bound, obsticles.value);

		if (!block)
		{
			Waypoint action = new Waypoint ();
			action.pos = pos + new Vector2 ((bound + 1f) * dir, 0);
			action.type = Waypoint.WpType.Fall;
			action.endUpPos = GroundBelowFall(action.pos, dir);
			actions.Add(action);
		}
		else
		{	
			Waypoint action = new Waypoint();
			action.pos = pos + new Vector2(bound * dir, 0);
			action.type = Waypoint.WpType.Ground;
			actions.Add(action);
		}

		foreach (Waypoint action in actions)
		{
			action.traveledDistance = wp.traveledDistance + Vector2.Distance (wp.endUpPos, action.endUpPos);
			action.minDistanceToTarget = action.traveledDistance + Vector2.Distance(targetPos, action.endUpPos);
			action.parent = wp;
			//if (action.pos == Vector2.zero)
			//	actions.Remove (action);
		}

		if (dir == -1)
		{
			actions.AddRange(ActionsFrom(wp, bounds, ledges, 1));
		}

		return actions;
	}

	Vector2 GroundBelowLedge(Vector2 ledgePos, float dir)
	{
		dir *= -1; //Ground dir opposite of heading
		Vector2 groundPos = Vector2.zero;
		Vector2 offset = new Vector2 (dir * 0.2f, 0);
		RaycastHit2D hit = Physics2D.Raycast(ledgePos + offset, new Vector2(dir, -1), 2.5f, obsticles);

		if (hit)
			groundPos = hit.point;
		else
			Debug.Log ("No ground pos!");

		return groundPos;
	}


	Vector2 GroundBelowFall(Vector2 ledgePos, float dir)
	{
		Vector2 groundPos = Vector2.zero;
		Vector2 offset = new Vector2 (dir * _fallOffCliffOffset, 0);
		RaycastHit2D hit = Physics2D.Raycast(ledgePos + offset, new Vector2(0, -1), 100, obsticles);

		if (hit)
			groundPos = hit.point;
		else
			Debug.Log ("No ground below fall!");

		return groundPos;
	}

    /// <summary>
    /// Calculate moveable ground bounds
    /// </summary>
    /// <returns></returns>
	private float[] GroundBounds(Vector2 pos)
    {
        Vector2 offset = new Vector2(0, 0);
        float stepSize = 0.2f;
        float leftBound = 0, rightBound = 0;
        while (leftBound == 0 || rightBound == 0)
        {
            Vector2 dir = new Vector2(0, -1);
            float distance = 0.5f + Mathf.Abs(offset.x) * 0.1f;

            offset.x += stepSize;
            offset.x *= 1.1f;
			RaycastHit2D hit = Physics2D.Raycast(pos + offset, dir, distance, obsticles);
            if (!hit)
            {
				Debug.DrawRay(pos + offset, dir * distance, Color.green, 1);
                if (rightBound == 0)
                {
                    rightBound = offset.x / 1.1f - stepSize;
                    stepSize *= -1;
                    offset = new Vector2(0, 0);
                    if (rightBound == 0)
                        rightBound = stepSize;
                }
                else
                {
                    leftBound = offset.x / 1.1f - stepSize;
                    if (leftBound == 0)
                        leftBound = stepSize;
                }
            }
            else
				Debug.DrawRay(pos + offset, dir * distance, Color.magenta, 0.5f);
        }

		leftBound = Mathf.Abs(leftBound);
		rightBound = Mathf.Abs(rightBound);

		return new float[] { leftBound, rightBound };
        //StartCoroutine(ClosestHighGround(leftBound - 1, rightBound + 1));
    }

    /// <summary>
    /// Calculate nearest ledges to higher ground
    /// </summary>
    /// <param name="leftBound"></param>
    /// <param name="rightBound"></param>
    /// <returns></returns>
	private Vector2[] ClosestHighGround(Vector2 pos, float leftBound, float rightBound)
    {
        Vector2 scanStart = new Vector2(0, 0);
        Vector2 scanDirection = new Vector2(1, 0);
        Vector2 leftLedge = Vector2.zero;
        Vector2 rightLedge = Vector2.zero;

        RaycastHit2D hit = Physics2D.Raycast(pos, scanDirection, 0, obsticles);
        scanStart.y += 0.2f;
        do
        {
            float distance = (scanDirection.x > 0) ? rightBound : leftBound;

			distance += _jumpCliffDistance;

            //Debug.DrawRay(pos + scanStart, scanDirection * distance, Color.magenta, 0.5f);
            hit = Physics2D.CircleCast(pos + scanStart, 0.2f, scanDirection, distance, obsticles);

            scanStart.y += 0.1f;
            scanDirection.x *= -1;

            if (hit)
            {
				//Ignore unreachable cliffs for iterative search
				if (Vector2.Distance(hit.point, pos) > _jumpDistance)
				{
					//continue;
				}
				if ((hit.point - pos).normalized.y < 0.2f)
				{
					//Debug.DrawLine(pos, hit.point, Color.red, 1);
//					Debug.Log("Angle to low");
					continue;
				}
                if (Mathf.Abs(pos.x - hit.point.x) < 1.0f || Mathf.Abs(pos.y - hit.point.y) < 0.5f) //To close to jump
                {
                    PlatformEffector2D platform = hit.collider.GetComponent<PlatformEffector2D>();
                    if (platform == null || !platform.useOneWay)
                    {
                        continue;
                    }
                }
                if (scanDirection.x > 0)
                {
                    if (rightLedge == Vector2.zero ||
                        Vector2.Distance(pos, rightLedge) > Vector2.Distance(pos, hit.point))
                    {
                        rightLedge = hit.point;
                    }
                }
                else
                {
                    if (leftLedge == Vector2.zero ||
                        Vector2.Distance(pos, leftLedge) > Vector2.Distance(pos, hit.point))
                    {
                        leftLedge = hit.point;
                    }
                }
            }
        } while ((rightLedge == Vector2.zero || leftLedge == Vector2.zero) && scanStart.y < _maxLedgeHeight);

		return new Vector2[] { rightLedge, leftLedge };
    }

    private void MoveTowards(Vector2 towards)
    {
        Vector2 myPos = gameObject.transform.position;
		float modifier = 1;

		if (Vector2.Distance (myPos, towards) < 1f)
			modifier = 0.25f;
		
        _body.velocity = new Vector2((towards.x > myPos.x ? 1 : -1) * _moveSpeed * modifier, _body.velocity.y);


        if (_body.velocity.x > 0 && !_manager.FacingRight)
            Flip();
        if (_body.velocity.x < 0 && _manager.FacingRight)
            Flip();
    }

	private IEnumerator CoJump(Vector2 towards)
	{
		yield return new WaitForSeconds(0.56f);
		Jump(towards);
		yield break;
	}

	private IEnumerator CheckProgress()
	{
		Vector2 lastPos = transform.position;
		yield return new WaitForSeconds(1.5f);
		if (lastPos.y < (transform.position.y - 0.2f))
			_preferedDirection *= -1;
		
		yield break;
	}

    private void Jump(Vector2 towards = default(Vector2), bool doubleJump = false)
    {
        if (!_manager.Grounded)
            return;

        Vector2 myPos = gameObject.transform.position;
        if (towards == default(Vector2))
            towards = myPos;


		float modifier = 1;

		if (Vector2.Distance (myPos, towards) < 1f)
			modifier = 0.25f;

		_body.AddForce(new Vector2((towards - myPos).normalized.x * _jumpForceX * modifier, _jumpForceY));

        if (doubleJump)
            CoJump(towards);

		StopCoroutine(CheckProgress());
		CheckProgress();

        if (_body.velocity.x > 0 && !_manager.FacingRight)
            Flip();
        if (_body.velocity.x < 0 && _manager.FacingRight)
            Flip();
    }

    public void Flip()
    {
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
        _manager.FacingRight = !_manager.FacingRight;
    }

    public enum AiTargetMode
    {
        Closest,
        LockOn
    };
}
