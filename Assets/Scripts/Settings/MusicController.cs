﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicController : MonoBehaviour
{
    [Header("This script requires 2 audioSources attached to object")]
    public List<AudioClip> musicClips;
    public bool Randomize;
    public float FadeTime = 10;
    public static MusicController Controller;

    private int _currentClip;

    private AudioSource[] _audioSources;
    private int _currentSource;
    private bool _triggerNext;
    private bool _stopped;

    public static void Next()
    {
        if (Controller != null)
            Controller.SwitchClip();
    }

    /// <summary>
    /// Check music hotkeys
    /// Shift + M               :   Next song
    /// Ctrl/Cmd + Shift + M    :   ToggleMusic
    /// </summary>
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            {
                if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.LeftCommand) ||
                    Input.GetKey(KeyCode.RightControl) || Input.GetKey(KeyCode.RightCommand))
                    ToggleMusic();
                else
                    SwitchClip();
            }
        }
    }

	void Start ()
	{
	    Controller = this;

	    _audioSources = GetComponents<AudioSource>();
	    _currentClip = 0;
	    _currentSource = 0;

        StartCoroutine(ClipRoutine());
	}

    private IEnumerator ClipRoutine()
    {
        int currentTime;
        float clipLength;
        while (true)
        {
            //Set up next clip
            if (Randomize)
                _currentClip = Random.Range(0, musicClips.Count);
            else
                _currentClip++;

            if (_currentClip >= musicClips.Count)
                _currentClip = 0;

            _currentSource = 1 - _currentSource;
            _audioSources[_currentSource].clip = musicClips[_currentClip];

            //Fade in clip
            _audioSources[_currentSource].Play();
            for (int i = 0; i < 101; i++)
            {
                float volume = (float)i * 0.01f;
                _audioSources[_currentSource].volume = volume;
                _audioSources[1 - _currentSource].volume = 1 - volume;

                yield return new WaitForSeconds(FadeTime/100);
            }
            _audioSources[1 - _currentSource].Stop();

            clipLength = _audioSources[_currentSource].clip.length;
            currentTime = 0;

            _triggerNext = false;
            //Wait for track to alomost end
            while (currentTime < clipLength - FadeTime - 5 && !_triggerNext)
            {
                yield return new WaitForSeconds(0.5f);
                currentTime = _audioSources[_currentSource].timeSamples/_audioSources[_currentSource].clip.frequency;
            }
        }
    }

    private IEnumerator StopMusic()
    {
        for (int i = 0; i < 101; i++)
        {
            _audioSources[_currentSource].volume -= 0.01f;
            _audioSources[1 - _currentSource].volume -= 0.01f;

            yield return new WaitForSeconds(FadeTime / 200);
        }
        _audioSources[1 - _currentSource].Stop();
        _audioSources[_currentSource].Stop();
    }

    public void SwitchClip()
    {
        _triggerNext = true;
        if (_stopped)
            ToggleMusic();
    }

    public void ToggleMusic()
    {
        _stopped = !_stopped;

        if (_stopped)
        {
            StopCoroutine(StopMusic());
            StopCoroutine(ClipRoutine());

            StartCoroutine(StopMusic());
        }
        else
        {
            StartCoroutine(ClipRoutine());
        }
    }
}
