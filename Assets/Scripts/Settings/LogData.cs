﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;

public enum LOG_TYPE { StartGame, JoinGame, EndGame, WeaponChange, WeaponFire, NetworkDisconnect, AddKill, AddDeath, AddDmgDealt, AddDmgTaken, TeamKill }

public class LogData
{
    public LOG_TYPE Type;
    public Dictionary<string, string> Fields { get { return _fields; }} 

    private Dictionary<string, string> _fields;

    public LogData(LOG_TYPE type, string key = null, string value = null)
    {
        Type = type;
        _fields = new Dictionary<string, string>();
        if (key != null)
        {
            _fields.Add(key, value);
        }
    }

    public void AddField(string key, string value)
    {
        _fields.Add(key, value);
    }

    public void AddField(string key, int value)
    {
        _fields.Add(key, value.ToString());
    }

    public void AddField(string key, bool value)
    {
        _fields.Add(key, value ? "1" : "0");
    }
}

