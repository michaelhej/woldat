﻿using System;
using UnityEngine;

public partial class Logdat : MonoBehaviour
{
    public static void SetGameId(string gameId)
    {
        _gameId = gameId;
    }

    public static void StartGame()
    {
        if (_gameId == null)
            _gameId = Guid.NewGuid().ToString();

        if (Log == null)
        {
            Debug.Log("Attach WorldManager to a script");
            GameObject.Find("GameManager").AddComponent<Logdat>();
        }

        LogData data = new LogData(LOG_TYPE.StartGame);
        data.AddField("playmode", Woldat.Settings.PlayMode.ToString());
		data.AddField("isnetwork", Woldat.Settings.IsNetwork);
		data.AddField("map", Application.loadedLevel);

		if (PhotonNetwork.inRoom)
			data.AddField("gamename", PhotonNetwork.room.name);

         Log.LogIt(data);
    }

    public static void EndGame()
    {
        if (PhotonNetwork.isMasterClient || PhotonNetwork.offlineMode)
        {
            LogData data = new LogData(LOG_TYPE.EndGame);
            Log.LogIt(data);
        }
        else
        {
            DisconnectGame();
        }
    }

    public static void DisconnectGame()
    {
        LogData data = new LogData(LOG_TYPE.NetworkDisconnect);
        data.AddField("player", PhotonNetwork.playerName);
        Log.LogIt(data);
    }

    public static void JoinGame()
    {
        if (Log == null)
            Debug.Log("Attach WorldManager to a script");

        LogData data = new LogData(LOG_TYPE.JoinGame);
        data.AddField("player", PhotonNetwork.playerName);
        Log.LogIt(data);
    }

    public static void WeaponChange(AvalibleWeapons wpn)
    {
        LogData data = new LogData(LOG_TYPE.WeaponChange);
        data.AddField("weapon", (int)wpn);
        data.AddField("player", Woldat.Player.GetComponent<PlayerManager>().PlayerName);
        data.AddField("gametime", (int)Time.timeSinceLevelLoad);
        Log.LogIt(data);
    }
    public static void AddKill(PhotonPlayer player)
    {
        LogData data = new LogData(LOG_TYPE.AddKill);
        data.AddField("player", player.name);
        Log.LogIt(data);
	}
	public static void AddDeath(PhotonPlayer player)
	{
		LogData data = new LogData(LOG_TYPE.AddDeath);
		data.AddField("player", player.name);
		Log.LogIt(data);
	}

	
	public static void Teamkill(int teamid, int killedteamid, PhotonPlayer killer, PhotonPlayer killed)
	{
		LogData data = new LogData(LOG_TYPE.TeamKill);
		data.AddField("teamid", teamid);
		data.AddField("killedteamid", killedteamid);
		data.AddField("killer", killer.name);
		data.AddField("killed", killed.name);
		Log.LogIt(data);
	}
	
	public static void AddDmgDealt(PhotonPlayer player, int dmg)
    {
        LogData data = new LogData(LOG_TYPE.AddDmgDealt);
        data.AddField("player", player.name);
        data.AddField("dmg", dmg);
        Log.LogIt(data);
    }
    public static void AddDmgTaken(PhotonPlayer player, int dmg)
    {
        LogData data = new LogData(LOG_TYPE.AddDmgTaken);
        data.AddField("player", player.name);
        data.AddField("dmg", dmg);
        Log.LogIt(data);
    }

    public static void WeaponChange(string wpn)
    {
        foreach (AvalibleWeapons val in Enum.GetValues(typeof(AvalibleWeapons)))
        {
            if (val.ToString() == wpn)
            {
                WeaponChange(val);
                return;
            }
        }
        Debug.Log("No such weapon found!");
    }
}
