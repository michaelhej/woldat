﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventoryController : MonoBehaviour
{
    class Point
    {
        public int x, y;
        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }

    public static float DetonationTime;
    public static bool RemindDetonationTime;
    public Font NotificiationFont;
    public int PlayerNumber = 0;

    private string _hoveredWeapon;
    private string _lastWeapon;
	private KeyCode[] _weaponKeys;
	private string[] _weaponAssign;
	private bool _showing;
    private int _currentWeaponIndex = 0;
    private bool _showDetonationTime;

    private Button[,] _buttonGrid;
    private Point _navPosition;
    private Point _lastNavPosition;
    private Vector2 _lastNavAxis;

    public PlayerManager Manager { get; set; }

    private PlayerManager _manager
    {
        get
        {
            if (Manager == null)
            {
                if (Woldat.LocalPlayer == null)
                    return null;

                GameObject player = Woldat.LocalPlayer[PlayerNumber];
                if (player == null)
                    player = GameObject.FindGameObjectWithTag("Player");
                return player ? player.GetComponent<PlayerManager>() : null;
            }
            return Manager;
        }
    }

    void Start()
    {
        InitButtonList();

		_weaponKeys = new KeyCode[5];
		_weaponAssign = new string[5];
        
		_weaponKeys[0] = KeyCode.Alpha1;
		_weaponKeys[1] = KeyCode.Alpha2;
		_weaponKeys[2] = KeyCode.Alpha3;
		_weaponKeys[3] = KeyCode.Alpha4;
		_weaponKeys[4] = KeyCode.Alpha5;

		GetComponent<Canvas>().enabled = _showing = false;
        gameObject.name = "Inventory" + PlayerNumber;

		AddCanvasGroup();
	}

    private void InitButtonList()
    {
        _navPosition = new Point(0, 0);
        _lastNavPosition = new Point(0, 0);
        _lastNavAxis = new Vector2(0, 0);
        _buttonGrid = new Button[6, 6];

        int x = 0;
        int y = -1;
        foreach (Button btn in GetComponentsInChildren<Button>())
        {
            if (x % 4 == 0)
            {
                x = 0;
                y++;
                if (y == 4)
                    x++;
            }
            _buttonGrid[x, y] = btn;
            x++;
        }
    }


    /// <summary>
    /// Show detonation time of grenade weapons
    /// </summary>
	void OnGUI()
	{
		if (_showDetonationTime)
		{
			GUI.contentColor = Color.black;
			GUI.skin.label.font = NotificiationFont;
			GUI.skin.label.fontSize = 13;
	        GUI.Label(new Rect(5, 5, 500, 20), "Detonation time: " + DetonationTime);
		}
	}

    private void SetDetonationTime(float t)
    {
        DetonationTime = t;
        _showDetonationTime = true;
        CancelInvoke("HideDetonationTime");
        Invoke("HideDetonationTime", 3);
    }

    private void HideDetonationTime()
    {
        _showDetonationTime = false;
    }

    /// <summary>
    /// Add a canvas group to each button (to enable fading)
    /// </summary>
    void AddCanvasGroup()
	{
		foreach (string wpn in Woldat.AvalibleWeapons)
		{
            GetBtn(wpn, "/Image").AddComponent<CanvasGroup>();
		}
	}

    private GameObject GetBtn(string btnName, string child = "")
    {
        return GameObject.Find(gameObject.name + "/Panel/Buttons/Btn" + btnName + child);

    }

    void Update()
	{
        if (Woldat.DisableInput || _manager == null)
	    {
	        GetComponent<Canvas>().enabled = false;
	        return;
	    }

        NavigateInventory();

	    //Show detonation time on weapon-switch
	    if (RemindDetonationTime)
	    {
            if (DetonationTime > 0)
	         SetDetonationTime(DetonationTime);
	        RemindDetonationTime = false;
	    }

        //Set detonation time
		if (Input.GetKey(KeyCode.T) || Input.GetKey(KeyCode.R))
	    {
	        for (int i = 0; i < _weaponKeys.Length; i++)
	        {
	            if (Input.GetKeyDown(_weaponKeys[i]))
	            {
                    if (_showDetonationTime && DetonationTime == (i + 1))
                        SetDetonationTime((i + 0.5f));
                    else
                        SetDetonationTime(i + 1);
	                return;
	            }
	        }
	    }

        //Display inventory
        if (_manager.GetButtonDown("Inventory"))
        {
            foreach (Button btn in GetComponentsInChildren<Button>())
                btn.interactable = true;

			GetComponent<Canvas>().enabled = _showing = true;
			ShowAmmo(); 

            /*
            GameObject activeBtn = GetBtn("Bazooka");
            if (!string.IsNullOrEmpty(_lastWeapon))
                activeBtn = GetBtn(_lastWeapon);

             * */
            Button activeBtn = _buttonGrid[_navPosition.x, _navPosition.y];
            GetComponentInChildren<EventSystem>().SetSelectedGameObject(activeBtn.gameObject);
		}

        if (_manager.GetButtonUp("Inventory"))
		{
		    StartCoroutine(CloseInventory());

            if (Woldat.Settings.InputMode == WoldatSettings.WoldatInputMode.GamePad)
                SetWeapon(_hoveredWeapon);
		}

        //Navigate
        NavigateInventory();

        //Set hotkey for hovered button
		if (_showing && _hoveredWeapon != "")
		{
			for (int i = 0; i < _weaponKeys.Length; i++)
			{
				if (Input.GetKeyDown(_weaponKeys[i]))
				{
					RemoveAssign(_hoveredWeapon);
					RemoveAssign(_weaponAssign[i]);

					AddAssign (i, _hoveredWeapon);

					_weaponAssign[i] = _hoveredWeapon;
				}
			}
		}
        //Select weapon when hotkey pressed
		else
		{
			for (int i = 0; i < _weaponKeys.Length; i++)
			{
				if (Input.GetKeyDown(_weaponKeys[i]))
				{
					if (_weaponAssign[i] != null && _weaponAssign[i] != "")
						SetWeapon( _weaponAssign[i] );
				}
			}
		}
	}

    private void NavigateInventory()
    {
		if (_manager.InputKey("Inventory") != "Fire" && !_manager.GetButton("Inventory"))
            return;

        if (_manager.GetAxis("NavX") != _lastNavAxis.x || _manager.GetAxis("NavY") != _lastNavAxis.y)
        {
            _lastNavPosition.x = _navPosition.x;
            _lastNavPosition.y = _navPosition.y;

            if (_manager.GetAxis("NavX") > 0)
                _navPosition.x++;
            else if (_manager.GetAxis("NavX") < 0)
                _navPosition.x--;

            if (_manager.GetAxis("NavY") < 0)
                _navPosition.y++;
            else if (_manager.GetAxis("NavY") > 0)
                _navPosition.y--;

            _lastNavAxis.x = _manager.GetAxis("NavX");
            _lastNavAxis.y = _manager.GetAxis("NavY");

            if (_navPosition.x < 0)
                _navPosition.x = 0;

            if (_navPosition.x > 3)
                _navPosition.x = 3;

            if (_navPosition.y < 0)
                _navPosition.y = 0;

            if (_navPosition.y > 4)
                _navPosition.y = 4;

            //Bottom row, only two items
            if (_navPosition.y == 4 && _navPosition.x == 0)
                _navPosition.x = 1;

            if (_navPosition.y == 4 && _navPosition.x == 3)
                _navPosition.x = 2;

            Button selectedButton = _buttonGrid[_navPosition.x, _navPosition.y];

            if (selectedButton == null)
            {
                _navPosition.x = _lastNavPosition.x;
                _navPosition.y = _lastNavPosition.y;
                return;
            }

            GetComponentInChildren<EventSystem>().SetSelectedGameObject(selectedButton.gameObject);
        }

    }

    /// <summary>
    /// Close inventory, only after button release
    /// (to prevent missing weapon change if to quick on keyboard)
    /// </summary>
    /// <returns></returns>
    private IEnumerator CloseInventory()
    {
        while (Input.GetMouseButton(0) || Input.GetButton("Submit"))
        {
            yield return new WaitForEndOfFrame();
        }
        GetComponent<Canvas>().enabled = _showing = false;
        foreach (Button btn in GetComponentsInChildren<Button>())
            btn.interactable = false;

    }

    void RemoveAssign(string wpn)
	{
		if (string.IsNullOrEmpty(wpn))
			return;

		int index = -1;
		for (int i = 0; i < _weaponAssign.Length; i++)
		{
			if (_weaponAssign[i] == wpn)
			{
				index = i;
				break;
			}
		}

        GameObject btn = GetBtn(wpn, "/Text/Shortcut");

		if (btn == null)
			return;

		Text label = btn.GetComponentInChildren<Text>();
		label = label.GetComponentInChildren<Text>();
		label.text = "";

		if (index > -1)
			_weaponAssign[index] = "";
	}

	void AddAssign(int index, string wpn)
	{
        GameObject btn = GetBtn(wpn, "/Text/Shortcut");
		
		if (btn == null)
			return;

		Text label = btn.GetComponentInChildren<Text>();
		label.text = "[ " + (index + 1) + " ]";

		_weaponAssign[index] = wpn;
	}

	void ShowAmmo()
	{
		foreach (string wpn in Woldat.AvalibleWeapons)
		{
			int ammo = AmmoController.WeaponAmmo(PlayerNumber)[wpn];

			if (ammo == 0)
            {
                GetBtn(wpn).GetComponent<Button>().interactable = false;
				GetBtn(wpn, "/Image").GetComponent<CanvasGroup>().alpha = 0.15f;
            }
			else
            {
                GetBtn(wpn).GetComponent<Button>().interactable = true;
                GetBtn(wpn, "/Image").GetComponent<CanvasGroup>().alpha = 1f;
            }

            Text txt = GetBtn(wpn, "/Text/Ammo").GetComponent<Text>();
			if (ammo == -1)
				txt.text = "∞";
			else
				txt.text = ammo.ToString();
		}
	}

	public void SetWeapon(string wpn)
	{
	    if (string.IsNullOrEmpty(wpn))
	        return;

        if (AmmoController.WeaponAmmo(PlayerNumber)[wpn] == 0)
			return;

	    _lastWeapon = wpn;

		if (Woldat.LocalPlayer[PlayerNumber] != null)
            Woldat.LocalPlayer[PlayerNumber].GetComponent<WeaponChanger>().SetWeapon(wpn);
	}

    public void ButtonEnter(string wpn)
    {
        //GetComponentInChildren<EventSystem>().SetSelectedGameObject(GetBtn(wpn));
        _hoveredWeapon = wpn;
    }
    public void ButtonSelect(string wpn)
    {
        _hoveredWeapon = wpn;
    }

	public void ButtonExit()
	{
		_hoveredWeapon = "";
	}
}
