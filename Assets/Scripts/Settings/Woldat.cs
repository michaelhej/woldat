﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public enum AvalibleWeapons { Bazooka, Minigun, WpnGrenade, WpnBanana, Banooka, Revolver, Lazer, Airstrike, WpnDragonball, Sniper, WpnCrazyCat, WpnDynamite, WpnSmokeGrenade, FlameThrower, WpnHolyGrenade, GrenadeLauncher, RayBay, Shotgun }

public class Woldat : MonoBehaviour
{
    public static bool DisableInput;
    public static bool KjellKod;
    private static WoldatSettings _settings;
    public static List<GameObject> LocalPlayer;

    public static GameObject Player
    {
        get { return LocalPlayer[0]; }
        set
        {
            if (LocalPlayer == null)
            { 
                LocalPlayer = new List<GameObject>();
                LocalPlayer.Add(value);
            }
            else
            {
                LocalPlayer[0] = value;
            }
        }
    }

    private static Chat _chat;

    public static void SetChat(Chat chat)
    {
        _chat = chat;
    }

    public static void PrintChat(string txt)
    {
        if (_chat != null)
            _chat.AddLine(txt);
    }

    public static void SendChat(string txt, int sendToTeam = -1)
    {
        if (Player.GetComponent<PlayerManager>() == null)
            return;

        string senderColor = "black";
        int team = (int)Player.GetComponent<PlayerManager>().GetData("Team");

        if (team == 1)
            senderColor = "magenta";
        if (team == 2)
            senderColor = "yellow";

        Player.GetComponent<PhotonView>()
            .RPC("SendChat", PhotonTargets.AllBufferedViaServer, txt, "black",
                PhotonNetwork.player.name, senderColor, sendToTeam);
    }

    public static List<string> AvalibleWeapons
	{
		get
		{
			List<string> wpnList = new List<string>();
			foreach (AvalibleWeapons val in Enum.GetValues(typeof(AvalibleWeapons)))
			{
				wpnList.Add(val.ToString());
			}
			return wpnList;
		}
	}

	public static WoldatSettings Settings
	{
		get
		{ 
			if (_settings == null)
			{
				_settings = new GameObject("WoldatSettings").AddComponent<WoldatSettings>();
				DontDestroyOnLoad(_settings);
			}
			return _settings;
		}
	}

    /// <summary>
    /// Converts angle to directional vector2
    /// </summary>
    /// <param name="angle"></param>
    /// <param name="facingRight">is player facing right</param>
    /// <returns></returns>
	public static Vector2 EulerToVector2(float angle, bool facingRight = false)
	{
		if (facingRight)
			angle = 180 - angle;

		var radians = angle * (Mathf.PI / 180);
		
		var dirVector = new Vector2 ();
		dirVector.x = Mathf.Cos(radians);
		dirVector.y = Mathf.Sin(radians);
		
		dirVector.Normalize();

		return dirVector;
	}

    /// <summary>
    /// Instantiate projectile over the locally or over the network
    /// </summary>
    /// <param name="ResourceToInstantiate">Resource from resources folder</param>
    /// <param name="position"></param>
    /// <param name="rotation"></param>
    /// <param name="group">network group</param>
    /// <param name="data">instantiaton data to send to object</param>
    /// <returns></returns>
	public static GameObject Instantiate(string ResourceToInstantiate, Vector3 position, Quaternion rotation, int group = 0, object[] data = null)
	{
        Debug.Log("Instantiating " + ResourceToInstantiate + ", Photon: " + (Settings.IsNetwork && PhotonNetwork.inRoom));
		if (Settings.IsNetwork && PhotonNetwork.inRoom)
			return PhotonNetwork.Instantiate(ResourceToInstantiate, position, rotation, (byte)group, data);
		else
			return Instantiate(Resources.Load(ResourceToInstantiate), position, rotation) as GameObject;
	}

    /// <summary>
    /// Instantiates a projectile and sends it necessary variables
    /// </summary>
    /// <param name="ResourceToInstantiate">Projectile</param>
    /// <param name="shootingPlayer">Player launching the projectile</param>
    /// <param name="force">force (0-1)</param>
    /// <param name="velocity">players current velocity (added to projectile)</param>
    /// <param name="position"></param>
    /// <param name="rotation"></param>
    /// <param name="group">network group</param>
    /// <returns></returns>
	public static GameObject InstantiateProjectileWithForce(string ResourceToInstantiate, PhotonPlayer shootingPlayer, float force, Vector2 velocity, Vector3 position, Quaternion rotation, int group = 0)
	{
		GameObject projectile = Instantiate(ResourceToInstantiate, position, rotation, group, new object[] {force, velocity, Time.time});

        //If local play, fire it manually (without sending instantiaton data)
		if (!Settings.IsNetwork)
            projectile.GetComponent<IProjectile>().Fire(force, velocity);

        return projectile;
	}
}
