﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldManager : MonoBehaviour
{
    public static bool Initiated { get; set; }
	public static Vector2 TopLeftBoundary { get; set; }
    public static Vector2 LowerRightBoundary { get; set; }
    public static Vector2 PlaneSpawnPosition { get; set; }
    public static Vector2 PlaneSpawnPositionRight { get; set; }
    public static Vector2 ObjectSpawnTopLeft { get; set; }
    public static Vector2 ObjectSpawnLowerRight { get; set; }

    public static bool DisableAirstrikes { get; set; }

[Header("This script will also add the inventory")]
    public bool _DisableAirstrikes = false;

[Header("Values can be overridden by a BoxCollider2D")]
	public Vector2 _TopLeftBoundary;
	public Vector2 _LowerRightBoundary;
	public Vector2 _PlaneSpawnPosition;

[Header("Worms and random object spawn")]
	public Vector2 _ObjectSpawnTopLeft;
	public Vector2 _ObjectSpawnLowerRight;


	public static Vector2 RandomObjectPosition {
		get {
			return new Vector2(Random.Range(ObjectSpawnTopLeft.x, ObjectSpawnLowerRight.x), 
			                   Random.Range(ObjectSpawnTopLeft.y, ObjectSpawnLowerRight.y)); 
		}
	}

	private GameObject _mapMenu;

    void Awake()
    {
        if (GetComponent<NetworkManager>().enabled == false)
        {
            Woldat.Settings.IsNetwork = false;
            PhotonNetwork.offlineMode = true;
        }

        DisableAirstrikes = _DisableAirstrikes;

        //If object has a collider, use it as world boundaries
        BoxCollider2D collider = GetComponent<BoxCollider2D>();
        if (collider != null)
	    {
            TopLeftBoundary = collider.transform.position + (Vector3)collider.offset;
	        LowerRightBoundary = TopLeftBoundary;

            TopLeftBoundary -= new Vector2(collider.size.x / 2, -collider.size.y / 2);
            
            LowerRightBoundary += new Vector2(collider.size.x / 2, -collider.size.y / 2);

            PlaneSpawnPosition = TopLeftBoundary + new Vector2(-3, -1);
            PlaneSpawnPositionRight = new Vector2(LowerRightBoundary.x + 3, TopLeftBoundary.y - 1);

            ObjectSpawnTopLeft = TopLeftBoundary + new Vector2(3, -1);
            ObjectSpawnLowerRight = new Vector2(LowerRightBoundary.x - 3, TopLeftBoundary.y - 1);

	        SetCameraBounds();
	    }
	    else
	    {
            TopLeftBoundary = _TopLeftBoundary;
            LowerRightBoundary = _LowerRightBoundary;
            PlaneSpawnPosition = _PlaneSpawnPosition;
            ObjectSpawnTopLeft = _ObjectSpawnTopLeft;
            ObjectSpawnLowerRight = _ObjectSpawnLowerRight;
	    }
	    gameObject.AddComponent<Logdat>();


        InitGame();

        Initiated = true;
	}

    private void Start()
    {
        if (GameObject.FindGameObjectWithTag("Inventory") == null)
            Instantiate(Resources.Load("MenuButtons/Inventory"), Vector3.zero, Quaternion.identity);

        if (GameObject.Find("ChatCanvas") == null)
            Instantiate(Resources.Load("MenuButtons/ChatCanvas"), Vector3.zero, Quaternion.identity);
		
		_mapMenu = Instantiate(Resources.Load("MenuButtons/MapMenuCanvas"), Vector3.zero, Quaternion.identity) as GameObject;
		Instantiate(Resources.Load("MenuButtons/StatsCanvas"), Vector3.zero, Quaternion.identity);
		Instantiate(Resources.Load("MusicManager"), Vector3.zero, Quaternion.identity);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
            Woldat.Settings.InputMode = WoldatSettings.WoldatInputMode.MouseKeyboard;
        else if (Input.GetButtonDown("Start"))
            Woldat.Settings.InputMode = WoldatSettings.WoldatInputMode.GamePad;


        if (Input.GetButton("P2_Start"))
        {
            Woldat.Settings.InputMode = WoldatSettings.WoldatInputMode.GamePad;
            if (Woldat.LocalPlayer.Count == 1)
            {
                GameObject player = TurnController.SpawnNewPlayer();
                Woldat.LocalPlayer.Add(player);
                player.GetComponent<PlayerManager>().CurrentPlayer = true;
                player.GetComponent<PlayerManager>().PlayerPrefix = "P2_";
                player.GetComponent<PlayerManager>().InventoryId = 1;


                GameObject inv = Instantiate(Resources.Load("MenuButtons/Inventory"), Vector3.zero, Quaternion.identity) as GameObject;
                inv.GetComponent<InventoryController>().Manager = player.GetComponent<PlayerManager>();
                inv.GetComponent<InventoryController>().PlayerNumber = 1;

                Vector3 position = inv.transform.Find("Panel").GetComponent<RectTransform>().position;
                position.x += 600;
                inv.transform.Find("Panel").GetComponent<RectTransform>().position = position;
            }
        }

        //Hide and lock the cursor if no UI is shown
        if (Input.GetKey(KeyCode.Tab) || Input.GetKey(KeyCode.LeftShift))
        {
            Cursor.visible = true;
            return;
        }

        if (Cursor.lockState != CursorLockMode.Confined || Cursor.visible)
        {
			if (_mapMenu != null && _mapMenu.GetComponent<Canvas>().enabled)
			    return;

            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = false;
        }
    }

    //Initiate TeamComponent
    private void InitGame()
    {
        Woldat.Instantiate(Resources.Load("SpawningObject/CargoPlane"), WorldManager.PlaneSpawnPositionRight, Quaternion.identity);

        if (!Woldat.Settings.IsNetwork)
        {
            GetComponent<NetworkManager>().enabled = false;
            TurnController.PlayersInTeams = Woldat.Settings.PlayersInTeams;

            List<Team> teams = new List<Team>();

            for (int i = 0; i < Woldat.Settings.Teams; i++)
            {
                Team t = new Team();
                t.Id = i;
                t.Name = "Team " + (i + 1);
                t.TeamColor = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
                teams.Add(t);
            }

            TurnController.Teams = teams;
        }
    }

    private void SetCameraBounds()
    {
        Camera.main.GetComponent<CameraFollow>().WorldMinX = TopLeftBoundary.x;
        Camera.main.GetComponent<CameraFollow>().WorldMinY = LowerRightBoundary.y;
        Camera.main.GetComponent<CameraFollow>().WorldMaxX = LowerRightBoundary.x;
        Camera.main.GetComponent<CameraFollow>().WorldMaxY = TopLeftBoundary.y;
        Camera.main.GetComponent<CameraFollow>().ReBound();
    }
}
