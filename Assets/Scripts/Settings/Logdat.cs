﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public partial class Logdat : MonoBehaviour
{
	private static string _gameId;
	private static Logdat Log;

    private string encryptionKey = "3314494145543673";
    private static string url = "http://logdat.piq.nu/log.php";

    void Awake()
	{
		Log = this;
	}

    public void LogIt(LogData data)
    {
        if (Woldat.Settings.IsNetwork)
            StartCoroutine("SendLogToServer", data);
    }

    public IEnumerator SendLogToServer(LogData data)
    {
        WWWForm form = new WWWForm();

        //If no gameId is set, wait for it
        while (string.IsNullOrEmpty(_gameId))
            yield return new WaitForSeconds(0.5f);

        //Add game-id and encrypted game-id for server-side-verification
        form.AddField("gameId", _gameId);
        form.AddField("_gameId", Crypt.Encrypt(_gameId, encryptionKey));

        //Add LogData-fields to post-fields
        form.AddField("type", data.Type.ToString());

        //Iterate all fields
        foreach (string key in data.Fields.Keys)
        {
            form.AddField("field" + key, data.Fields[key]);
        }

        //Create www-handler and send request to server
        WWW www = new WWW(url, form);
        yield return www;
    }

    void OnApplicationQuit()
    {
        EndGame();
    }
}
