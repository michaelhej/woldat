﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class MainMenu : MonoBehaviour
{
	public CanvasGroup panelCanvas;
	public CanvasGroup blackCanvas;

	public Toggle ToggleNetwork;
    public Toggle ToggleNetworkPublic;
    public GameObject PanelRooms;

    public GameObject NetworkButton;
    public InputField CreateRoomField;
    public InputField NameInput;
    public GameObject NetworkPanel;

    private RoomInfo _roomToJoin;
	private bool _fadingIn;
    private bool _fadingOut;
    private bool _startAfterFade;
    private int _mapToLoad = 1;

	void Awake()
	{
        NetworkPanel.SetActive(false);
		panelCanvas.alpha = 0;
		blackCanvas.alpha = 1;
	}

	void Start()
	{
		_fadingIn = true;
        NameInput.text = System.Environment.UserName;
	}

    void Update()
	{
		if (_fadingIn)
		{
			if (panelCanvas.alpha < 1 && Time.time > 0.5f)
			{
				panelCanvas.alpha = Mathf.MoveTowards(panelCanvas.alpha, 1, (Time.deltaTime * 1));
				blackCanvas.alpha = Mathf.Clamp(1 - panelCanvas.alpha*1, 0, 1);
			}

			if (panelCanvas.alpha >= 1)
				_fadingIn = false;
		}

		if (_fadingOut)
		{
			blackCanvas.alpha = Mathf.MoveTowards(blackCanvas.alpha, 1, (Time.deltaTime));
			panelCanvas.alpha = 1 - blackCanvas.alpha;

			if (blackCanvas.alpha == 1)
			{
                //Play
                if (_startAfterFade)
                   StartNewGame(_mapToLoad);
			}
		}
	}

    public void SetMap(int map)
    {
        if (_fadingOut)
            return;

        _mapToLoad = map;
        StartGame();
    }

	public void StartGame()
	{
	    if (_fadingOut)
	        return;

        //Remove menu players
        GameObject[] menuPlayer = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in menuPlayer)
        {
            Destroy(player);
        }

        //Apply game settings
	    Woldat.Settings.PlayerName = NameInput.text;
		Woldat.Settings.IsPublicGame = ToggleNetworkPublic.isOn;
		Woldat.Settings.IsNetwork = ToggleNetwork.isOn;
        PhotonNetwork.offlineMode = !Woldat.Settings.IsNetwork;

        if (_roomToJoin != null)
        {
            PhotonNetwork.automaticallySyncScene = true;
            Woldat.Settings.NetworkRoom = _roomToJoin.name;
            PhotonNetwork.JoinRoom(_roomToJoin.name);
            _fadingOut = true;
	    }
        else if (CreateRoomField.text == "" && ToggleNetwork.isOn)
        {
            StartCoroutine(FlashInputField());
        }
        else if (CreateRoomField.text != "")
        {
            Hashtable netInfo = new Hashtable();
            netInfo.Add("m", _mapToLoad);
            Woldat.Settings.NetworkHashtable = netInfo;
            Woldat.Settings.NetworkRoom = CreateRoomField.text;
            _startAfterFade = _fadingOut = true;
        }
        else
        {
            _startAfterFade = _fadingOut = true;
        }
	}

    private IEnumerator FlashInputField()
    {
        ColorBlock cb = CreateRoomField.colors;
        Color defaultColor = cb.normalColor;
        for (int i = 0; i < 3; i++)
        {
            cb.normalColor = new Color(1, 0, 0);
            CreateRoomField.colors = cb;

            yield return new WaitForSeconds(0.125f);

            cb.normalColor = defaultColor;
            CreateRoomField.colors = cb;

            yield return new WaitForSeconds(0.125f);
        }
    }

    /// <summary>
	/// Sets the play mode.
	/// </summary>
	/// <param name="mode">0 = worms, 1 = soldat</param>
	public void SetPlayMode(int mode)
	{
		if (mode == 0)
		{
			Woldat.Settings.PlayMode = WoldatSettings.WoldatPlayMode.Worms;
			ToggleNetwork.interactable = false;
			ToggleNetwork.isOn = false;

            GameObject.Find("TeamsSlider").GetComponent<Slider>().enabled = true;
            GameObject.Find("PlayersSlider").GetComponent<Slider>().enabled = true;
		}
		else
		{
			Woldat.Settings.PlayMode = WoldatSettings.WoldatPlayMode.Soldat;
            ToggleNetwork.interactable = true;
            GameObject.Find("TeamsSlider").GetComponent<Slider>().value = 2;
            GameObject.Find("PlayersSlider").GetComponent<Slider>().value = 8;

		    GameObject.Find("TeamsSlider").GetComponent<Slider>().enabled = false;
		    GameObject.Find("PlayersSlider").GetComponent<Slider>().enabled = false;
		}
	}

    public void SetTeams(GameObject o)
    {
        Slider slider = o.GetComponent<Slider>();
        Text label = o.GetComponentInChildren<Text>();

        Woldat.Settings.Teams = (int)slider.value;
        if (slider.value == 1)
            label.text = "FFA";
        else
            label.text = slider.value.ToString();
    }

    public void SetTeamPlayers(GameObject o)
    {
        Slider slider = o.GetComponent<Slider>();
        Text label = o.GetComponentInChildren<Text>();

        Woldat.Settings.PlayersInTeams = (int)slider.value;
        label.text = slider.value.ToString();
    }

	public void StartNewGame(int scene)
	{
        GameObject[] menuPlayer = GameObject.FindGameObjectsWithTag("Player");

        foreach (GameObject player in menuPlayer)
        {
            Destroy(player);
        }

        AmmoController.ResetAmmo();
        if (Woldat.Settings.IsNetwork)
            PhotonNetwork.LoadLevel(scene);
        else
    		Application.LoadLevel(scene);
	}

    public void NetworkCheck()
    {
        //Woldat.Settings.IsNetwork = ToggleNetwork.isOn;
        if (ToggleNetwork.isOn)
        {
            NetworkConnect();
            NetworkPanel.SetActive(true);
        }
        else
        {
			PhotonNetwork.Disconnect();
            NetworkPanel.SetActive(false);
        }
    }

    public void NetworkConnect()
    {
        if (!PhotonNetwork.connected)
            PhotonNetwork.ConnectUsingSettings("0.1");
    }
    void OnJoinedLobby()
    {

    }

    public void NetworkPopulateRoomList()
    {
        foreach (Transform child in PanelRooms.transform)
        {
            Destroy(child.gameObject);
        }
        RoomInfo[] rooms = PhotonNetwork.GetRoomList();

        for (int i = 0; i < rooms.Length; i++)
        {
            GameObject o = Instantiate(NetworkButton, new Vector3(0, i * 25 + 50, 0), Quaternion.identity) as GameObject;
            o.transform.SetParent(PanelRooms.transform, false);
            o.GetComponentInChildren<Text>().text = rooms[i].name;
            RoomInfo room = rooms[i];
            o.GetComponent<Button>().onClick.AddListener(() => { JoinRoom(room); });
        }
    }
    public void JoinRoom(RoomInfo room)
    {
        _roomToJoin = room;
        StartGame();
    }

    public void CreateRoom()
    {
        string roomName = CreateRoomField.text;

        Debug.Log("Creating " + roomName);

        RoomOptions ro = new RoomOptions();
        ro.isVisible = ToggleNetworkPublic.isOn;

        PhotonNetwork.JoinOrCreateRoom(roomName, ro, TypedLobby.Default);
    }

    private void OnReceivedRoomListUpdate()
    {
        Debug.Log("Got room list");
        NetworkPopulateRoomList();
    }

    void OnJoinedRoom()
    {
        Woldat.Settings.NetworkHashtable = PhotonNetwork.room.customProperties;
    }
}
