﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class TurnController : MonoBehaviour
{
    private const int RoundTime = 60;
	[Header("Teams")]
    public static List<Team> Teams;
    public static int PlayersInTeams;
    public static int TickTime;

    private static int _teamTurn;
    private Dictionary<int, int> _teamPlayerTurn; 

    private GameObject _timeObject;
    private static bool _switch;
    private PlayerManager _currentPlayer;
    
	void Start()
	{
	    if (Woldat.Settings.IsNetwork)
	        return;

	    _timeObject = Woldat.Instantiate("MenuButtons/TimeCanvas", Vector3.zero, Quaternion.identity) as GameObject;
        _teamPlayerTurn = new Dictionary<int, int>();
	    _teamTurn = 0;

		SpawnPlayers();
	    StartCoroutine(Tick());
		Woldat.Settings.PlayMode = WoldatSettings.WoldatPlayMode.Worms;
	}

    /// <summary>
    /// Create teams and spawn players
    /// </summary>
	void SpawnPlayers()
	{
		for (int i = 0; i < Teams.Count; i++)
		{
			Teams[i].Id = i;
		    _teamPlayerTurn.Add(i, 0);
			for (int j = 0; j < PlayersInTeams; j++)
			{
			    GameObject player = SpawnNewPlayer();
                player.GetComponent<PlayerManager>().PlayerTeam = Teams[i];
                player.GetComponent<PlayerManager>().PlayerTeamNumber = j;
                player.GetComponent<PlayerManager>().InventoryId = i;
                player.GetComponent<PlayerManager>().SwitchCharacter((i % 2) + 1);
			}
		}
		NextTurn();
	}

    public static GameObject SpawnNewPlayer()
    {
        Vector3 spawnPosition = WorldManager.RandomObjectPosition;
        GameObject player = Instantiate(Resources.Load("Player/Player"), spawnPosition, Quaternion.identity) as GameObject;
        player.GetComponent<PlayerManager>().PlayerName = RandomName();
        player.GetComponent<PlayerManager>().CurrentPlayer = false;

        return player;
    }

    private IEnumerator Tick()
    {
        Text timeText = _timeObject.transform.Find("TimeText").GetComponent<Text>();
        Text timeTextShadow = _timeObject.transform.Find("TimeTextShadow").GetComponent<Text>();

        TickTime = RoundTime;
        while ( true )
        {
            TickTime--;

            timeText.text = TickTime.ToString();
            timeTextShadow.text = TickTime.ToString();

            yield return new WaitForSeconds(1);

            if (TickTime == 5 && _currentPlayer != null)
                _currentPlayer.StartCoroutine(_currentPlayer.Retreat(5));

            if (TickTime == 0 || _switch)
            {
                _switch = false;
                TickTime = RoundTime;
                NextTurn();
            }
        }
    }

    /// <summary>
    /// Iterates all players and selects next player
    /// </summary>
    /// <param name="allDead"></param>
    public void NextTurn()
    {
        int deadPlayers = 0;

        SwitchPlayer:
        int teamNo = _teamTurn % Teams.Count;
        int playerNo = _teamPlayerTurn[teamNo] % PlayersInTeams;

        if (deadPlayers == PlayersInTeams)
            _teamTurn++;

		foreach(GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            PlayerManager playerControl = (PlayerManager)player.GetComponent("PlayerManager");

            //Childobjects would not have attached PlayerManager
            if (playerControl == null)
                continue;

            playerControl.CurrentPlayer = false;

		    if (playerControl.PlayerTeam.Id == teamNo)
		    {
                if (playerControl.PlayerTeamNumber == playerNo)
		        {
                    //If player dead, continue with next player in team
		            if (playerControl.IsDead)
		            {
                        _teamPlayerTurn[teamNo]++;
		                deadPlayers++;
                        goto SwitchPlayer;
		            }

                    //Make current player and attach camera
		            Camera.main.GetComponent<CameraFollow>().SetMainTarget(player.transform);
		            Woldat.Player = player;
                    _currentPlayer = playerControl;
                    StopCoroutine(playerControl.SwitchTo());
                    StartCoroutine(playerControl.SwitchTo());
		        }
		    }
        }
        _teamPlayerTurn[teamNo]++;

	    //Next team
		_teamTurn++;
	}

    public static void NextPlayer()
    {
        _switch = true;
    }

    /// <summary>
    /// Generate a cool random worm-name
    /// </summary>
    /// <returns></returns>
	static string RandomName()
	{
		List<string> names = new List<string>();

		names.Add("Konstapel Kuk");
		names.Add("Boggy B");
		names.Add("Spadge");
		names.Add("Clagnut");
        names.Add("Zoolander");
        names.Add("Mongster");
        names.Add("Chuck Norris");
        names.Add("Red Bird");
        names.Add("Yellow Duck");
        names.Add("Miggy");
        names.Add("Bogsy");
        names.Add("Mojo-jojo");
        names.Add("Bitsy");
        names.Add("Peewee");
        names.Add("Jetslam");
        names.Add("Beefcake");
        names.Add("Fulp");
        names.Add("Carrot");
        names.Add("Purple Nip");
        names.Add("Boggy Pete");
        names.Add("Boggy The Kid");
        names.Add("Trooper");
        names.Add("Tinkle");

		return names[ Random.Range(0, names.Count) ];
	}
}
