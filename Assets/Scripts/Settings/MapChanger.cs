﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MapChanger : MonoBehaviour
{
	private Canvas _canvas;

	void Awake ()
	{
		_canvas = GetComponent<Canvas>();
		_canvas.enabled = false;
	}

	void Update ()
	{
		if (PhotonNetwork.isMasterClient || !PhotonNetwork.inRoom)
		{
			if (Input.GetKeyUp(KeyCode.Escape))
			{
                Cursor.visible = _canvas.enabled = !_canvas.enabled;
                foreach (Button btn in GetComponentsInChildren<Button>())
                    btn.interactable = Cursor.visible;
			}
		}
	}

	public void ChangeMap(int map)
	{
		if (PhotonNetwork.inRoom)
			PhotonNetwork.LoadLevel(map);
		else
			Application.LoadLevel(map);
	}

	public void ExitToMenu()
	{
		if (PhotonNetwork.connected)
			PhotonNetwork.Disconnect();

		Application.LoadLevel(0);
	}
}
