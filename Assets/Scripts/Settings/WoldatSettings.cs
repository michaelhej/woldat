﻿using UnityEngine;
using System.Collections;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class WoldatSettings : MonoBehaviour
{
    //Properties
    public int Teams { get; set; }
    public int PlayersInTeams { get; set; }
	public WoldatPlayMode PlayMode { get; set; }
	public WoldatWormsAimMode WormsAimMode { get; set; }
    public WoldatInputMode InputMode { get; set; }
	public bool IsNetwork {get; set;}
    public string NetworkRoom { get; set; }
    public bool IsPublicGame { get; set; }
    public Hashtable NetworkHashtable { get; set; }
    public string PlayerName { get; set; }

	//Default values
	public WoldatSettings()
	{
	    NetworkHashtable = new ExitGames.Client.Photon.Hashtable();
		Teams = 2;
	    PlayersInTeams = 2;
		PlayMode = WoldatPlayMode.Soldat;
		WormsAimMode = WoldatWormsAimMode.Mouse;
        InputMode = WoldatInputMode.GamePad;
        IsNetwork = true; 
        PlayerName = System.Environment.UserName;
        NetworkRoom = System.Environment.UserName + "s room";
	}
	
	//Enums
	public enum WoldatPlayMode { Worms, Soldat, Local }
	public enum WoldatWormsAimMode { Keyboard, Mouse }
    public enum WoldatInputMode { MouseKeyboard, GamePad }
}