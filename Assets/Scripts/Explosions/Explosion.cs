﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Debug = UnityEngine.Debug;

public class Explosion : MonoBehaviour
{
    public PhotonPlayer ShootingPlayer { get; set; }

    //Variables set in inspector
[Header("Impact damage")]
	public float Radius;
	public float Force;
	public float UpForce;
    public ForceMode2D _ForceMode = ForceMode2D.Impulse;
	public float Damage;
	public bool DistanceScale = true;
    public float TimeToLive = 10;
    public bool Ripple = false;

[Header("Periodic damage")]
    public float PeriodicInterval;
    public float PeriodicDamage;
    public float PeriodicRadius;
    public bool PeriodicDistanceScale;
    public float PeriodicLifeTime;
    public bool PeriodicTimeScale;

[Header("Spawn object")]
    public string InstantiateObject;

    //Private variables
	private List<RaycastHit2D> _collisions;
	private List<GameObject> _actedUpon;
    private float _startTime;
    private bool _initialExplosion = true;
    private void Start()
    {
        _startTime = Time.time;
        Explode();
        if (PeriodicInterval > 0 && PeriodicDamage > 0 && PeriodicRadius > 0)
        {
            Damage = PeriodicDamage;
            Radius = PeriodicRadius;
            DistanceScale = PeriodicDistanceScale;
            Force = 0;
            UpForce = 0;
            _initialExplosion = false;
            InvokeRepeating("Explode", PeriodicInterval, PeriodicInterval);
        }

        if (InstantiateObject != "")
            if (PhotonNetwork.isMasterClient || !PhotonNetwork.inRoom)
                Woldat.Instantiate(InstantiateObject, transform.position, Quaternion.identity);

        Destroy(gameObject, TimeToLive);
    }

    void Explode()
	{
		_collisions = new List<RaycastHit2D>();
        _actedUpon = new List<GameObject>();
		Vector3 explosionOrigin = transform.position;

		//CircleCasts at origin of position and increses radius until castRadius = explosionRadius
		//(If Radius was used directly the point of collision would be at the edge of the CircleCast
		for (int i = 1; i <= 20;i++) //20 steps
		{
			float castRadius = (Radius / 20) * i; //Increments circle radius
			RaycastHit2D[] xhits = Physics2D.CircleCastAll(explosionOrigin, castRadius, new Vector2(0, 0));
			foreach(RaycastHit2D c in xhits)
				if (!_actedUpon.Contains(c.collider.gameObject)) //Ignores if aldready in 
				{
    				_collisions.Add(c);
                    _actedUpon.Add(c.collider.gameObject);
				}
		}

		//Loops through all objects in explosion radius
		foreach(RaycastHit2D c in _collisions)
        {

            Vector3 forcePosition = explosionOrigin - (Vector3)c.point;
            var distance = forcePosition.magnitude; //Distance from origin
            distance = Mathf.Clamp(distance, Radius * 0.15f, Radius); //Radius under .15 sould count as direct hit

            float distanceScale;
            if (DistanceScale)
                distanceScale = (Radius - distance) / Radius;
            else
                distanceScale = 1;

			//Blow up other projectiles
            if (c.collider.gameObject.tag == "Projectile" && _initialExplosion)
			{
				GameObject proj = c.collider.gameObject;
			    Impact impact = proj.GetComponent<Impact>();
                if (impact != null && impact.enabled && impact.Volatile && !impact.WaitingToDetonate)
                {
                    //Not enough dmg to detonate
                    if (Damage*distanceScale < impact.VolatileThreshold)
                        return;

                    //Instead of blowing upp all projectiles at once, this is to give a ripple-effect on many projectiles stacked together
                    if (Ripple)
                    {
                        impact.WaitingToDetonate = true;
                        StartCoroutine(DetonateVolatile(proj)); 
                    }
                    else
                    {
                        impact.TimedDetonation();
                    }
                }
			}

			if (c.collider.gameObject.tag == "Player" || c.collider.gameObject.tag == "WorldObject")
			{
			    if (c.rigidbody == null)
			        continue;

				var forceVector = forcePosition / distance;

				forceVector.Normalize();
				//forceVector.y = Mathf.Clamp(forceVector.y, 0, 0.8f);

				forceVector *= Force * -1;
				
				forceVector.y += UpForce;

				//If downward force, apply 80 % upwards
				if (forceVector.y < 0)
					forceVector.y *= -0.8f;

			    if (Damage * distanceScale < 1)
			        continue;

				float torque = forceVector.normalized.x * -10 * distanceScale * forceVector.normalized.sqrMagnitude;

				bool wasKinematic = c.rigidbody.isKinematic;

				if (wasKinematic)
					c.rigidbody.isKinematic = false;

				//Send the bastad flying
				c.rigidbody.AddForce(forceVector * distanceScale, _ForceMode);
				c.rigidbody.fixedAngle = false;
				c.rigidbody.AddTorque(torque);

				ApplyDamage(c.collider.gameObject, Damage * distanceScale);
			}
		}

        if (PeriodicTimeScale && PeriodicDamage > 0)
        {
            Damage = PeriodicDamage * (1 - (Time.time - _startTime) / PeriodicLifeTime);
        }
	}

    IEnumerator DetonateVolatile(GameObject proj)
    {
        yield return new WaitForSeconds(0.1f);
        if (proj != null)
            proj.GetComponent<Impact>().TimedDetonation();
    } 

    void ApplyDamage(GameObject player, float dmg)
    {
		if (player.GetComponent<DealDamage>() != null)
            player.GetComponent<DealDamage>().TakeDamage(dmg, ShootingPlayer);
	}
}
