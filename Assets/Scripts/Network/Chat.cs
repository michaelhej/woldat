﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Chat : MonoBehaviour
{
    private Canvas _canvas;
    private CanvasGroup _canvasGroup;
    private InputField _input;
    private Text _text;

    private int _sendToTeam = -1;
    private void Awake()
    {
        _canvas = GetComponent<Canvas>();
        _input = GetComponentInChildren<InputField>();
        _text = GetComponentInChildren<Text>();
        _canvasGroup = _text.GetComponent<CanvasGroup>();
    }

    // Use this for initialization
	void Start ()
    {
	    Woldat.SetChat(this);
	    ToggleInput();
    }

    public void AddLine(string txt)
    {
        _canvasGroup.alpha = 1;
        StopCoroutine("FadeOut");
        StartCoroutine("FadeOut");

        if (txt != "")
            _text.text += "\r\n" + txt;
    }

    private IEnumerator FadeOut()
    {
        yield return new WaitForSeconds(10);
        while (_canvasGroup.alpha >= 0)
        {
            _canvasGroup.alpha -= 0.05f;
            yield return new WaitForSeconds(0.05f);
        }
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            ToggleInput();
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            _canvas.enabled = false;
        }

        if (Input.GetKeyUp(KeyCode.Tab))
        {
            _canvas.enabled = true;
        }
    }

    private void ToggleInput()
    {
        Woldat.DisableInput = _input.enabled = !_input.enabled;
        if (_input.enabled)
        {
            _input.GetComponent<CanvasGroup>().alpha = 1;
            _input.ActivateInputField();

            if (Input.GetKey(KeyCode.LeftAlt) | Input.GetKey(KeyCode.RightAlt))
                _sendToTeam = (int) PhotonNetwork.player.customProperties["Team"];
            else
                _sendToTeam = -1;

            if (_sendToTeam == -1)
                _input.placeholder.GetComponent<Text>().text = "Send to everyone...";
            else
                _input.placeholder.GetComponent<Text>().text = "Send to team mates...";
            AddLine(""); //Show chat window
        }
        else
        {
            _input.GetComponent<CanvasGroup>().alpha = 0;
        }
    }

    public void SendChat()
    {
        if (_input.text.ToLower() == "kjellkodab")
        { 
            KejllkodAB();
            return;
        }

        if (_input.text != "" && PhotonNetwork.inRoom)
            Woldat.SendChat(_input.text, _sendToTeam);

        _input.text = "";
    }

    //hehe
    private void KejllkodAB()
    {
        Woldat.KjellKod = true;
        foreach (string wpn in Woldat.AvalibleWeapons)
        {
            AmmoController.SetAmmo(Woldat.Player.GetComponent<PlayerManager>().InventoryId, wpn, -1);
        }
    }
}
