﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class NetworkManager : MonoBehaviour
{
	bool wasConnected = false;

	void Start()
	{
        InitiatePlayerProperties();
	    InitRoomProperties();
        
        //Creating the room
        if (Woldat.Settings.IsNetwork && PhotonNetwork.connected && PhotonNetwork.insideLobby)
        {
            JoinOrCreate();
        }
        else if (Woldat.Settings.IsNetwork && !PhotonNetwork.connected)
		{
            PhotonNetwork.ConnectUsingSettings("0.2");
        }
		else if (PhotonNetwork.inRoom)
		{
			Spawn();
		}
	}

    private void InitRoomProperties()
    {
        Woldat.Settings.Teams = 2;

        Woldat.Settings.NetworkHashtable["Teams"] = Woldat.Settings.Teams;
        Woldat.Settings.NetworkHashtable["ScoreTeam1"] = 0;
        Woldat.Settings.NetworkHashtable["ScoreTeam2"] = 0;
        Woldat.Settings.NetworkHashtable["GameId"] = Guid.NewGuid().ToString();
    }

    private void InitiatePlayerProperties()
    {
        PhotonNetwork.playerName = Woldat.Settings.PlayerName;//System.Environment.UserName + " " + System.DateTime.Now.ToLongTimeString();
        Hashtable defaultProperties = new Hashtable();
        defaultProperties.Add("Kills", 0);
        defaultProperties.Add("Deaths", 0);
        defaultProperties.Add("DmgDealt", 0);
        defaultProperties.Add("DmgTaken", 0);
        PhotonNetwork.player.SetCustomProperties(defaultProperties);
    }

    private void JoinOrCreate()
    {
        PhotonNetwork.automaticallySyncScene = true;

        RoomOptions ro = new RoomOptions()
        {
            isVisible = Woldat.Settings.IsPublicGame,
            maxPlayers = 8,
            customRoomProperties = Woldat.Settings.NetworkHashtable,
        };
        PhotonNetwork.JoinOrCreateRoom(Woldat.Settings.NetworkRoom, ro, TypedLobby.Default);
    }

	void Update()
	{
		if (!PhotonNetwork.connected && wasConnected)
		{
			Debug.Log("Connection lost!");
			
			PhotonNetwork.ConnectUsingSettings("0.2");
		}
		wasConnected = PhotonNetwork.connected & !PhotonNetwork.offlineMode;
	}

	//This function call is required since updating to photon 1.6
	//and seems to replace OnJoinedLobby, which is not called after upgrading
	void OnConnectedToMaster()
	{
		JoinOrCreate();
	}

	//Redundant (not used) in Photon < 1.6
	void OnJoinedLobby()
	{
	    JoinOrCreate();
	}

	void OnJoinedRoom()
	{
	    if (PhotonNetwork.isMasterClient)
        {
			if (PhotonNetwork.room.name == "")
				PhotonNetwork.room.name = Woldat.Settings.NetworkRoom;

            if (Woldat.Settings.NetworkHashtable.Count == 0)
                InitRoomProperties();

            PhotonNetwork.room.SetCustomProperties(Woldat.Settings.NetworkHashtable);

            Logdat.SetGameId(PhotonNetwork.room.customProperties["GameId"].ToString());
            Logdat.StartGame();
            Logdat.JoinGame();
	    }
	    else
	    {
	        Debug.Log("Joined gameid: " + PhotonNetwork.room.customProperties["GameId"].ToString());
            Logdat.SetGameId(PhotonNetwork.room.customProperties["GameId"].ToString());
            Logdat.JoinGame();
	    }
	    Woldat.Settings.NetworkHashtable = PhotonNetwork.room.customProperties;
	    Spawn();
	}

	void Spawn()
	{
		//Remove all objects we own
		PhotonNetwork.DestroyPlayerObjects(PhotonNetwork.player);

        //Reset team
        Hashtable someCustomPropertiesToSet = new Hashtable() { { "Team", -1 } };
        PhotonNetwork.player.SetCustomProperties(someCustomPropertiesToSet);

        //Instantiate player
        GameObject player = PhotonNetwork.Instantiate("Player/Player", WorldManager.RandomObjectPosition, Quaternion.identity, 0);
	    player.GetComponent<PlayerManager>().PlayerName = PhotonNetwork.playerName;
        Woldat.Player = player;

        //Assign team
        Dictionary<int, int> numPlayers = PlayersInTeams();

	    if (numPlayers[1] > numPlayers[2])
	    {
	        Woldat.Player.GetComponent<PlayerManager>().UpdateData("Team", 2);

	        someCustomPropertiesToSet = new Hashtable() {{"Team", 2}};
	        PhotonNetwork.player.SetCustomProperties(someCustomPropertiesToSet);
	    }
	    else
	    {
	        Woldat.Player.GetComponent<PlayerManager>().UpdateData("Team", 1);

            someCustomPropertiesToSet = new Hashtable() { { "Team", 1 } };
            PhotonNetwork.player.SetCustomProperties(someCustomPropertiesToSet);
	    }

	}

    /// <summary>
    /// Return dictionary of players in teams (-1 and 0 reserved)
    /// </summary>
    /// <returns></returns>
    private Dictionary<int, int> PlayersInTeams()
    {
        Dictionary<int, int> numPlayers = new Dictionary<int, int>();

        //Populate all teams
        for (int i = -1; i <= Woldat.Settings.Teams; i++)
        {
            numPlayers.Add(i, 0);
        }

        foreach (PhotonPlayer player in PhotonNetwork.playerList)
	    {
            int team = player.customProperties.ContainsKey("Team") ? (int)player.customProperties["Team"] : -1;

            numPlayers[team] = numPlayers[team] + 1;
	    }
        return numPlayers;
    }
}
