﻿using UnityEngine;
using System.Collections;

public partial class NetworkController : MonoBehaviour
{

	void OnServerInitialized()
	{
		Debug.Log("Server initialized");
		//SpawnPlayer();
	}

	void OnConnectToServer()
	{

	}

	void OnFailedToConnect()
	{

	}
	
	void OnPlayerConnected(NetworkPlayer player)
	{
		//SpawnPlayer();
	}
	
	void OnPlayerDisconnected(NetworkPlayer player)
	{
		Network.RemoveRPCs(player);
		Network.DestroyPlayerObjects(player);
	}
	
	void OnApplicationQuit()
	{
		if (Network.isServer)
		{
			Network.Disconnect(200);
			MasterServer.UnregisterHost();
		}
		
		if (Network.isClient)
		{
			Network.Disconnect(200);
		}
	}
	
	void OnMasterServerEvent(MasterServerEvent masterServerEvent)
	{
		if (masterServerEvent == MasterServerEvent.RegistrationSucceeded)
		{
			Debug.Log("Success!");
		}
	}

	void OnFailedToConnectToMasterServer(NetworkConnectionError info)
	{

	}

	void OnNetworkInstantiate(NetworkMessageInfo info)
	{

	}
}
