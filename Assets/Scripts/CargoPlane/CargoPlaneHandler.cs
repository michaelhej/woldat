﻿using UnityEngine;
using System.Collections;

public class CargoPlaneHandler : MonoBehaviour {

	public GameObject CargoPlane;

	private Vector2 _planePosition;
	private Vector3 _tempVector;

	// Update is called once per frame
	void Update ()
	{
		//när lådan närmar sig marken på avstånd ska en fallskärm skapas
		RaycastHit2D farDistance = Physics2D.Raycast (transform.position, -Vector2.right, 4.5f);
		//RaycastHit2D closeDistance = Physics2D.Raycast (transform.position, -Vector2.right, 1.0f);
		//RaycastHit2D nearlyGrounded = Physics2D.Raycast (transform.position, -Vector2.right, 0.3f);



		if (farDistance.collider != null) 
		{
			//_parachuteObject = Instantiate(Resources.Load("SpawningObject/" + Parachute), transform.position, transform.rotation) as GameObject;
			
			_tempVector = transform.position;
			_tempVector.x += 0.005f;
			_tempVector.y += 0.467f;
			//_parachuteObject.transform.position = _tempVector;
			//_parachuteObject.transform.parent = transform;
			
			Rigidbody2D crateBody = GetComponent<Rigidbody2D>();
			
			//crateBody.gravityScale = 0f;	//0.0001f
			crateBody.velocity = crateBody.velocity * 0.15f;
		}
	}
}
