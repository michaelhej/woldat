﻿using UnityEngine;
using System.Collections;

public class SpawnPlane : MonoBehaviour {

	public float SpawnInterval;
		
	//public float spawnMinY; //behövs inte samma y hela tiden
	//public float spawnMaxY; //behövs inte samma y hela tiden
	public float spawnHeight;
	
	SpawnCrate sc;
	
	private float _time;
	//private float _spawnPositionY; //ta bort

	// Use this for initialization
	void Start () {
		//float spawnMinX = -20;	//start bana
		//float spawnMaxX = 20;	//end bana
		//float _spawnPositionY = -20; // ska gå från -20 till 20

		//float spawnHeight = sc.spawnHeight; //får samma height som crate
		//removed spawnHeight = 5;

		_time = SpawnInterval;
		_time += Time.deltaTime;
		if (_time >= SpawnInterval) 
		{
			//slumpar X positionen där planet ska skapas			//Ska tas bort. ska ej slumpas, start y=längst till vänster
			//_spawnPositionY = Random.Range (spawnMinY, spawnMaxY);
			//_time = 0f;
			
			//skapar planet på en random plats på kartan
			Instantiate (Resources.Load("SpawningObject/CargoPlane"), new Vector3 (15, spawnHeight, 0), Quaternion.identity);
			//Debug.Log ("Spawning rotation: " + createdCrate.transform.rotation.eulerAngles);
		}
	}
	
	// Update is called once per frame
	void Update () {

	}
}
