﻿using UnityEngine;
using System.Collections;

public class MovingObject : MonoBehaviour
{
    public Vector2 Speed;
    public float SpeedLimit;
    public bool Continous;
    public float StepSpeed;
    public float StartDelay;

    private Rigidbody2D _body;
    private int _obsticles = 0;

    void Awake()
    {
        _body = GetComponent<Rigidbody2D>();
        if (!GetComponent<PhotonView>().isMine)
            enabled = false;
    }
    void Start()
    {
        if (!Continous)
            StartCoroutine(Push());
    }

    IEnumerator Push()
    {
        yield return new WaitForSeconds(StartDelay);
        while (true)
        {
            if (_body.velocity.sqrMagnitude < SpeedLimit * SpeedLimit || SpeedLimit == 0)
                _body.AddForce(Speed);
            yield return new WaitForSeconds(StepSpeed);
        }
    }

    void FixedUpdate()
    {
        RaycastHit2D ray = Physics2D.Raycast(transform.position, Speed, 1.5f, (1 << 11) | (1 << 12)); //Ground, SolidObject
        if (ray)
            _obsticles++;
        else
            _obsticles = 0;

        if (_obsticles > 20)
        {
            _obsticles = 0;
            Speed = new Vector2(Speed.x * -1, Speed.y);
        }

        if (Continous)
            _body.velocity = Speed;
    }
}
