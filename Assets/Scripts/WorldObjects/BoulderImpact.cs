﻿using UnityEngine;
using System.Collections;

public class BoulderImpact : MonoBehaviour
{
	public float CrushDamage = 0;
	public bool PlayAudioSourceOnImpact;
	public string ProjectileSpawnOnImpact;
	public float SpawnCooldown = 5;

	private bool OnCooldown = false;
	// Use this for initialization
	void ResetCooldown()
	{
		OnCooldown = false;
	}


	void OnCollisionEnter2D(Collision2D collision)
	{
		float impactMagnitude = collision.relativeVelocity.sqrMagnitude;

		if (collision.gameObject.tag == "Player")
		{
			GameObject player = collision.gameObject;
			if (player.GetComponent<PhotonView>().isMine)
			{
				float objectVelocity = GetComponent<Rigidbody2D>().velocity.y;
				float damageScale = Mathf.Clamp((objectVelocity / 7), 0, 1);

				if (damageScale > 0.2f && player.GetComponent<DealDamage>() != null)
				{
					player.GetComponent<DealDamage>().TakeDamage(CrushDamage * damageScale);
				}
				Debug.Log("Dealing damage: " + damageScale + ", mgt: " + impactMagnitude);
			}
		}

		if (collision.gameObject.tag != "World")
			return;
		

		if (Mathf.Abs(GetComponent<Rigidbody2D>().velocity.y) > 2)
			if (PlayAudioSourceOnImpact)
				GetComponent<AudioSource>().Play();

		if (ProjectileSpawnOnImpact != "" && !OnCooldown && impactMagnitude > 0.2f && GetComponent<PhotonView>() != null)
		{
			if (GetComponent<PhotonView>().isMine)
				Woldat.Instantiate("Projectile/" + ProjectileSpawnOnImpact, collision.contacts[0].point, Quaternion.identity);
			OnCooldown = true;
			Invoke("ResetCooldown", SpawnCooldown);
		}

		impactMagnitude *= 0.01f; //1 % of collision squared magnitude
		impactMagnitude = Mathf.Min(impactMagnitude, 1.5f); //Max 1.5 sec

        /*
		if (impactMagnitude > 0.1f && Camera.main.GetComponent<CameraShake>().shake < impactMagnitude)
		{
			Camera.main.GetComponent<CameraShake>().shakeAmount = impactMagnitude / 4;
			Camera.main.GetComponent<CameraShake>().shake = 0.2f;//shakeMagnitude;
		}
         * */
	}
}
