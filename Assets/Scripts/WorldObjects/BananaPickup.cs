﻿using UnityEngine;
using System.Collections;

public class BananaPickup : MonoBehaviour
{
	public GameObject Explosion;
	public GameObject Wheel;
	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag != "Player")
			return;

		
		Vector3 spawnPostion = transform.position;//Camera.main.transform.position;
		spawnPostion += new Vector3(0, 2, 0);
		Instantiate(Explosion, transform.position, transform.rotation);

		//If I picked up the banana, spin the wheeeeeel!
		if (collider.gameObject.GetComponent<PhotonView>().isMine)
		{
			//Camera.main.GetComponent<CameraFollow>().SetSize(4);
			//Camera.main.GetComponent<CameraFollow>().ZoomOutOnPosition(spawnPostion, 1f, 6);
			GameObject wheelObject = Instantiate(Wheel, spawnPostion, Quaternion.identity) as GameObject;
		    wheelObject.GetComponent<WheelSpin>().WheelPlayer = collider.gameObject;
		}
		
		Destroy(gameObject);

	}
}
