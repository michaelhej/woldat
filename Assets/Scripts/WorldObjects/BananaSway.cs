﻿using UnityEngine;
using System.Collections;

public class BananaSway : MonoBehaviour
{

	public float SwayDistance = 1;
	public float SwayAngle = 20;
	public bool DropToGround = true;

	Vector3 startPosition;
	Vector3 nextPosition;
	float startAngle;
	float nextAngle;

	Rigidbody2D body;
	
	Vector3 velocity = Vector3.zero;
	float anuglarVelocity = 0;
	// Use this for initialization
	void Start ()
	{
		body = GetComponent<Rigidbody2D>();
		if (DropToGround)
		{
			body.isKinematic = false;
			body.gravityScale = 0.025f;
		}
		else
		{
            body.gravityScale = 0;

            startPosition = transform.position;
            startAngle = transform.eulerAngles.z;
            nextPosition = startPosition;
            nextAngle = transform.eulerAngles.z;
		}
	}

	void Update()
	{
		if (body.gravityScale > 0)
		{
			if (Physics2D.Raycast(transform.position, new Vector2(0, -1), 1f, (1 << 11)))
			{
				body.velocity = Vector2.zero;
				body.gravityScale = 0;
				body.isKinematic = true;
				
				startPosition = transform.position;
				startAngle = transform.eulerAngles.z;
				nextPosition = startPosition;
				nextAngle = transform.eulerAngles.z;
			}
		}
		else
		{
			if (Vector3.Distance(transform.position, nextPosition) < 0.1f)
				nextPosition = startPosition + new Vector3(Random.Range(-SwayDistance, SwayDistance), Random.Range(-SwayDistance, SwayDistance), transform.position.z);

			if (Mathf.Abs(transform.eulerAngles.z - nextAngle) < 0.1f)
				nextAngle = startAngle + Random.Range(-SwayAngle, SwayAngle);

			transform.position = Vector3.SmoothDamp(transform.position, nextPosition, ref velocity, 3);

			Vector3 angles = transform.eulerAngles;
			angles.z = Mathf.SmoothDampAngle(transform.eulerAngles.z, nextAngle, ref anuglarVelocity, 4);
			transform.eulerAngles = angles;
		}
	}
}
