﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Metadata.W3cXsd2001;

public class ObjectSpawner : MonoBehaviour
{
    public bool OnImpact;
    public bool OnTrigger;
    public float Cooldown;
    public float Interval;
    [Header("Ex: Projectile/Banana")]
    public string SpawnObject;
    public Vector3 SpawnPosition;
    public bool RelativePosition = true;
    public float DetonateObjectAfterMin;
    public float DetonateObjectAfterMax;

    private float _lastSpawn;
    void Start()
    {
        if (Interval < Cooldown)
            Debug.Log("Warning: Cooldown less than interval");

        if (Interval > 0)
            StartCoroutine(SpawnRoutine());
    }

    IEnumerator SpawnRoutine()
    {
        while(true)
        { 
            yield return new WaitForSeconds(Interval);
            Spawn();
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (OnImpact)
            Spawn();
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (OnTrigger)
            Spawn();
    }

    void Spawn()
    {
        //Not master
        if (PhotonNetwork.inRoom && !PhotonNetwork.isMasterClient)
            return;

        //Not connected to room
        if (Woldat.Settings.IsNetwork && !PhotonNetwork.inRoom)
            return;

        //Cooldown
        if (Time.realtimeSinceStartup - _lastSpawn < Cooldown)
            return;

        Vector3 objectSpawnPosition = SpawnPosition;

        if (RelativePosition)
            objectSpawnPosition += transform.position;

        GameObject obj = Woldat.Instantiate(SpawnObject, objectSpawnPosition, Quaternion.identity);

        if (DetonateObjectAfterMin > 0)
            StartCoroutine(Detonate(obj));

        _lastSpawn = Time.realtimeSinceStartup;
    }

    IEnumerator Detonate(GameObject obj)
    {
        yield return new WaitForSeconds(Random.Range(DetonateObjectAfterMin, DetonateObjectAfterMax));

        if (obj.GetComponent<Impact>() != null)
            obj.GetComponent<Impact>().TimedDetonation();
    }
}
