﻿using UnityEngine;
using System.Collections;

public class SpawnCargoPlane : MonoBehaviour {

	public float SpawnInterval;
	public float SpawnHeight;
	public float TimeBetweenDrops;
	public float TimeBetweenSwayingBanana;

	//private GameObject _cargoPlane;
	private float _time;

	// Use this for initialization
	void Start () 
	{
		_time = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (_time >= SpawnInterval) 
		{
			Woldat.
		    Instantiate(Resources.Load("SpawningObject/CargoPlane"), new Vector3(50, SpawnHeight, 0), Quaternion.identity);
			_time = 0;
		}
		_time += Time.deltaTime;
	}
}
