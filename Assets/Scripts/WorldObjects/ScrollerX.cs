﻿using UnityEngine;
using System.Collections;

public class ScrollerX : MonoBehaviour {

	private float scrollSpeed;
	public float directionSpeed;

	float startX;
	public float distance;
	
	// Use this for initialization
	void Start () {
		startX = transform.position.x;
		scrollSpeed = -directionSpeed;
	}

	// Update is called once per frame
	void Update () {

		if(startX - transform.position.x > distance) {
			scrollSpeed = directionSpeed;
		}
		
		else if(startX - transform.position.x < -distance) {
			scrollSpeed = -directionSpeed;
		}
		
		transform.Translate(scrollSpeed*Time.deltaTime,0,0);
	}
}