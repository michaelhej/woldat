﻿using UnityEngine;
using System.Collections;

public class ObjectNetworkRenderer : MonoBehaviour
{
	
	[Header("This script requires PhotonView obervation!")]
	public float smoothness = 15f;

	private Vector3 netPosition;
	private Quaternion netRotation;
	private Vector2 netVelocity;
	private float netAngularVelocity;
	private bool netSending = false;

	
	private Rigidbody2D body;
	
	void Awake()
	{
		body = GetComponent<Rigidbody2D>();
	}

	// Use this for initialization
	void Start ()
	{
		if (GetComponent<PhotonView>() != null)
		{
			if (!GetComponent<PhotonView>().isMine)
			{
				StartCoroutine("UpdateData");
			}
		}
	}
	//Smoothly transition to receiving location
	IEnumerator UpdateData()
	{
		while (true)
		{
			//Debug.Log(transform.position + " -> " + netPosition);
			
			if (netSending)
			{
				if (Mathf.Abs(transform.position.sqrMagnitude - netPosition.sqrMagnitude) > 0.25f)
					transform.position = Vector3.Lerp(transform.position, netPosition, Time.deltaTime * smoothness);
				transform.rotation = Quaternion.Lerp(transform.rotation, netRotation, Time.deltaTime * smoothness);
				//body.velocity = Vector2.MoveTowards(body.velocity, netVelocity, Time.deltaTime * smoothness);
				//body.angularVelocity = Mathf.Lerp(body.angularVelocity, netAngularVelocity, Time.deltaTime * smoothness);
				
				body.velocity = netVelocity;
				body.angularVelocity = netAngularVelocity;
			}
			
			/*
			transform.position = netPosition;
			transform.rotation = netRotation;
			body.velocity = netVelocity;
			body.angularVelocity = netAngularVelocity;
			*/
			
			yield return null;
		}
	}
	
	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting) //Send position
		{
			stream.SendNext(transform.position);
			stream.SendNext(transform.rotation);
			stream.SendNext(body.velocity);
			stream.SendNext(body.angularVelocity);
			stream.SendNext(true);
		}
		else //Receive position
		{
			netPosition = (Vector3)stream.ReceiveNext();
			netRotation = (Quaternion)stream.ReceiveNext();
			netVelocity = (Vector2)stream.ReceiveNext();
			netAngularVelocity = (float)stream.ReceiveNext();
			netSending = (bool)stream.ReceiveNext();
		}
	}
}
