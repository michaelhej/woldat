﻿using UnityEngine;
using System.Collections;

public class WheelSpin : MonoBehaviour
{
	public GameObject Pointer;
	public Sprite[] QuarterSprites;
	public AudioClip SpinningSound;
	public AudioClip[] WinningSounds;
    public GameObject WheelPlayer;

	Rigidbody2D body;
	bool spinning = false;

	delegate void WinningDelegate();
	WinningDelegate Winnings;

	SpriteRenderer spriteRenderer;
	AudioSource _sound;
	Sprite originalSprite;
	Sprite winningSprite;

	//PRICES
	void FlyingGorilla()
	{
		Woldat.Instantiate("SpawningObject/GorillaOnAPlane", WorldManager.PlaneSpawnPosition, Quaternion.identity);
	}
	
	void BanookaAmmo()
	{
		AmmoController.AddAmmo(WheelPlayer.GetComponent<PlayerManager>().InventoryId, "Banooka", 10);
	}

	void RockBanana()
	{
		Woldat.Instantiate("SpawningObject/RockBanana", WorldManager.RandomObjectPosition, Quaternion.identity);
	}
	
	void BananaAmmo()
	{
        AmmoController.AddAmmo(WheelPlayer.GetComponent<PlayerManager>().InventoryId, "WpnBanana", 10);
	}


	void Awake()
	{
		transform.localScale = Vector3.zero;
	}

	void Start()
	{
		_sound = GetComponent<AudioSource>();
		body = Pointer.GetComponent<Rigidbody2D>();
		spriteRenderer = GetComponent<SpriteRenderer>();
		originalSprite = spriteRenderer.sprite;

		StartCoroutine("FlipForward");
	}

	IEnumerator FlipForward()
	{
		yield return new WaitForSeconds(0.2f);
		Vector3 scale = transform.localScale;
		while( true )
		{
			scale.x += 0.05f;
			scale.y += 0.05f;

			transform.localScale = scale;
			yield return new WaitForSeconds(0.01f);

			if (scale.x >= 1)
				break;
		}
		Spin();
	}

	void Spin()
	{
		float spinForce = Random.Range(11f, 22f);
		Debug.Log("Spinning with: " + spinForce + " force");
		body.AddTorque(-spinForce, ForceMode2D.Impulse);
		spinning = true;
		_sound.clip = SpinningSound;
		_sound.Play();
	}
	
	// Update is called once per frame
	void Update ()
	{
		Pointer.transform.localPosition = Vector3.zero;
		
		if (Mathf.Abs(body.angularVelocity) <= 5f && spinning)
		{
			body.angularDrag += 0.1f;
		}

		if (Mathf.Abs(body.angularVelocity) <= 10f && spinning)
		{
			if (_sound.isPlaying)
				_sound.Stop();

			DoneSpinning(Pointer.transform.eulerAngles.z);
			body.angularVelocity = 0;
			spinning = false;
		}
	}

	void FixedUpdate()
	{
		if (spinning)
			UpdateSprite(Pointer.transform.eulerAngles.z);
	}

	void UpdateSprite(float angle)
	{
		if (angle > 270)
		{
			spriteRenderer.sprite = QuarterSprites[0];
		}
		else if (angle > 180)
		{
			spriteRenderer.sprite = QuarterSprites[1];
		}
		else if (angle > 90)
		{
			spriteRenderer.sprite = QuarterSprites[2];
		}
		else if (angle > 0)
		{
			spriteRenderer.sprite = QuarterSprites[3];
		}
	}

	void DoneSpinning(float angle)
	{
		if (angle > 270)
		{
			Winnings += BananaAmmo;
			_sound.clip = WinningSounds[0];
			winningSprite = QuarterSprites[0];
		}
		else if (angle > 180)
		{
			Winnings += RockBanana;
			_sound.clip = WinningSounds[1];
			winningSprite = QuarterSprites[1];
		}
		else if (angle > 90)
		{
			Winnings += BanookaAmmo;
			_sound.clip = WinningSounds[2];
			winningSprite = QuarterSprites[2];
		}
		else if (angle > 0)
		{
			Winnings += FlyingGorilla;
			_sound.clip = WinningSounds[3];
			winningSprite = QuarterSprites[3];
		}
		_sound.Play();
		StartCoroutine("FlipBack");
	}

	IEnumerator FlipBack()
	{
		for (int i = 0; i < 4; i++)
		{
			spriteRenderer.sprite = originalSprite;
			yield return new WaitForSeconds(0.25f);
			spriteRenderer.sprite = winningSprite;
			yield return new WaitForSeconds(0.25f);
		}

		GetComponentInChildren<ParticleSystem>().Stop();
		yield return new WaitForSeconds(1);
		Vector3 scale = transform.localScale;
		while( true )
		{
			scale.x -= 0.05f;
			scale.y -= 0.05f;
			transform.localScale = scale;
			yield return new WaitForSeconds(0.005f);
			
			if (scale.x <= 0.05f)
				break;
		}
		Winnings();
		Destroy(gameObject);
	}
}
