﻿using UnityEngine;
using System.Collections;

public class PhysicsChanger : MonoBehaviour
{

	void Start ()
	{
	    StartCoroutine(Sweeper());
	}

    IEnumerator Sweeper()
    {
        yield return new WaitForSeconds(2);
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        foreach (GameObject player in players)
        {
            if (player.GetComponent<MovementControl>() == null)
                continue;

            player.GetComponent<Rigidbody2D>().gravityScale = 1.2f;
            player.GetComponent<Rigidbody2D>().mass = 0.45f;
            player.GetComponent<MovementControl>().JumpSpeed = 6;
            player.GetComponent<MovementControl>().WalkSpeed = 4;
        }
    }
}
