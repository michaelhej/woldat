﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Teleporter : MonoBehaviour
{
    public Vector3 TargetLocation;
    public GameObject OriginEffect;
    public GameObject TargetEffect;
    public Vector3 EffectOffset;

    private float _cooldown = 4;
    private static Dictionary<GameObject, float> CooldownList; 


    void Awake()
    {
        if (CooldownList == null)
            CooldownList = new Dictionary<GameObject, float>();
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        GameObject other = collider.gameObject;

        if (CooldownList.ContainsKey(other))
        {
            if (Time.realtimeSinceStartup - CooldownList[other] < _cooldown)
            {
                return;
            }
        }
        else
        {
            CooldownList.Add(other, Time.realtimeSinceStartup);
        }
        if (other.tag == "Player")
        {
            if (other.GetComponent<PlayerManager>().IsMine)
            {
                CooldownList[other] = Time.realtimeSinceStartup;

                if (OriginEffect != null)
                    Instantiate(OriginEffect, transform.position + EffectOffset, Quaternion.identity);

                if (TargetEffect != null)
                    Instantiate(TargetEffect, TargetLocation + EffectOffset, Quaternion.identity);
                StartCoroutine(Teleport(other));
            }
        }
    }

    /// <summary>
    /// Teleport the player
    /// </summary>
    /// <param name="other"></param>
    private IEnumerator Teleport(GameObject other)
    {
        yield return new WaitForSeconds(0.5f);
        other.transform.position = TargetLocation;
    }
}
