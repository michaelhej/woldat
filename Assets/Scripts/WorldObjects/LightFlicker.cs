﻿using UnityEngine;
using System.Collections;

public class LightFlicker : MonoBehaviour
{
    private float startRange;
    private float startIntensity;


    private Light _light;
	// Use this for initialization
	void Start ()
	{
	    _light = GetComponent<Light>();
        startRange = _light.range;
        startIntensity = _light.intensity;
        Flick();
	}

    private void Flick()
    {
        _light.range = startRange + Random.Range(-startRange, startRange) * 0.15f;
        _light.intensity = startIntensity + Random.Range(-startIntensity, startIntensity) * 0.1f;
        Invoke("Flick", Random.Range(0,0.3f));
    }

}
