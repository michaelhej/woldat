﻿using UnityEngine;
using System.Collections;

public class ScrollerY : MonoBehaviour {
	
	private float scrollSpeed;
	public float directionSpeed;
	
	float startY;
	public float distance;
	
	// Use this for initialization
	void Start () {
		startY = transform.position.y;
		scrollSpeed = -directionSpeed;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(startY - transform.position.y > distance) {
			scrollSpeed = directionSpeed;
		}
		
		else if(startY - transform.position.y < -distance) {
			scrollSpeed = -directionSpeed;
		}
		
		transform.Translate(0,scrollSpeed*Time.deltaTime,0);
	}
}