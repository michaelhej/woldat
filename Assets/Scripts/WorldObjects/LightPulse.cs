﻿using UnityEngine;
using System.Collections;

public class LightPulse : MonoBehaviour
{
	private Light _light;
	public float maxIntensity = 5f;
	public float minIntensity = 0f;
	public float pulseSpeed = 0.5f; 	//(1 / pulseSpeed) sekunder
	private float targetIntensity = 1f;
	private float currentIntensity;
	
	 
	void Start(){
		_light = GetComponent<Light>();
	}

	void Update(){
		currentIntensity = Mathf.MoveTowards(_light.intensity,targetIntensity, Time.deltaTime*pulseSpeed);
		if(currentIntensity >= maxIntensity) {
			currentIntensity = maxIntensity;
			targetIntensity = minIntensity;
		}
		else if(currentIntensity <= minIntensity) {
			currentIntensity = minIntensity;
			targetIntensity = maxIntensity;
		}
		_light.intensity = currentIntensity;
	}
}