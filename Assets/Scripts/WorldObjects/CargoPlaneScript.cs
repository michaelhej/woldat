﻿using UnityEngine;
using System.Collections;

public class CargoPlaneScript : MonoBehaviour {

	public float SpawnInterval;
	public float SpawnHeight;
	public float TimeBetweenDrops;
	public float DropsBetweenSwayingBanana;
	
	private Vector3 _tempVector;
	private bool _enableDrop;
	private bool _oldEnableDrop;
	private int _count;
	private int _dropCount;
	private float _lastDropTime;

	private GameObject[] _cratesOnMap;
	private WorldManager _worldManager;

	// Use this for initialization
	void Start () 
	{
		_enableDrop = false;
		_count = 0;
		_dropCount = 0;
		_worldManager = GameObject.Find("GameManager").GetComponent<WorldManager>();
	}
	
	void FixedUpdate ()
	{
	    if (PhotonNetwork.inRoom && !PhotonNetwork.isMasterClient)
	        return;

		_oldEnableDrop = _enableDrop;

		//Check if plane over world
	    if (!Physics2D.Raycast(transform.position, -Vector2.up))
	        return;

		//Find all existing crates
		_cratesOnMap = GameObject.FindGameObjectsWithTag ("CrateContainer");

		//If plane inside boundaries
	    _enableDrop = transform.position.x < _worldManager._LowerRightBoundary.x &&
	                  transform.position.x > _worldManager._TopLeftBoundary.x;

	    //If flew outside
		if (!_enableDrop && _oldEnableDrop) 
		{
			GetComponent<SpriteRenderer>().enabled = false;
			StartCoroutine(PauseToNextSpawnTime());
		}

		//Moving...
		_tempVector = transform.position;
		_tempVector.x -= 0.3f;
		transform.position = _tempVector;

		//Drop crate
		if ((_cratesOnMap.Length <= 10 && _enableDrop) && (_count % Random.Range(10, 40) == 0) && (Time.time >= (_lastDropTime + TimeBetweenDrops)))
		{
			if(_dropCount % 20 == 0)
			{
				Woldat.Instantiate("SpawningObject/SwayingBanana", transform.position, Quaternion.identity);
			}
			else
			{
				_tempVector = transform.position;
				_tempVector.y -= 0.3f;

				Woldat.Instantiate("SpawningObject/DropDownCrate", _tempVector, Quaternion.identity);
			}
			_lastDropTime = Time.time;
			_dropCount++;
		}
		_count++;
	}

	IEnumerator PauseToNextSpawnTime()
    {
		yield return new WaitForSeconds(SpawnInterval);

		_tempVector = transform.position;
		_tempVector.x = 50;
		transform.position = _tempVector;
		GetComponent<SpriteRenderer> ().enabled = true;
	}
}
