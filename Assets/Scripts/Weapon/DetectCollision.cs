﻿using UnityEngine;
using System.Collections;

public class DetectCollision : MonoBehaviour
{
    private bool hasHit;
	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == "Boundary" || other.tag == "World") 
		{
			Destroy(gameObject);
		}
	}
	
	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "World") 
		{
			Destroy(gameObject);
		}

		if (collider.gameObject.tag == "Player")
		{
		    if (collider.gameObject.GetComponent<DealDamage>() == null)
		        return;

			PhotonPlayer player = null;
			if (GetComponent<IProjectile>() != null)
				player = GetComponent<IProjectile>().ShootingPlayer;

			float ProjectileDamage = gameObject.GetComponent<ProjectileBullet>().BulletDamage;
			collider.gameObject.GetComponent<DealDamage>().TakeDamage(ProjectileDamage, player);

            if (hasHit)
                return;
            hasHit = true;

			Destroy(gameObject);
		}
	}
}
