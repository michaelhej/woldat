﻿using UnityEngine;
using System.Collections;

public class ProjectileBullet : MonoBehaviour, IProjectile
{
    public PhotonPlayer ShootingPlayer { get; set; }
	private Rigidbody2D _body;
	public float _bulletSpeed = 0.0f;
	public float BulletDamage;
	public float Damage { get; set; }

	private float angleMagnitude = 0;

	void Awake()
	{
		_body = GetComponent<Rigidbody2D>();
	}

	public void Fire(float force, Vector2 velocity)
	{
		Destroy (this.gameObject, 2f);
		float escapeAngle = transform.eulerAngles.z;
		
		escapeAngle += 180;
		var radians = escapeAngle * (Mathf.PI / 180);
		
		var escapeAngleAsDirectionVector = new Vector2 ();
		escapeAngleAsDirectionVector.x = Mathf.Cos (radians);
		escapeAngleAsDirectionVector.y = Mathf.Sin (radians);
		
		escapeAngleAsDirectionVector.Normalize();
		
		//Vinkelskillnad från 0 (rakt upp)
		angleMagnitude = escapeAngle - 450f;
		if (angleMagnitude > -180)
			angleMagnitude = Mathf.Abs(angleMagnitude);
		
		if (angleMagnitude < 0)
			angleMagnitude += 360;
		
		angleMagnitude /= 180;
		angleMagnitude = 1 - angleMagnitude;
		
		_body.velocity = (escapeAngleAsDirectionVector * _bulletSpeed);
		//_body = GetComponent<Rigidbody2D>();
	}

	public void SetBulletSpeed(float BulletSpeed)
	{
		_bulletSpeed = BulletSpeed;
	}
}
