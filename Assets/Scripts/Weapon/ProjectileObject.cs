using UnityEngine;
using System.Collections;

public class ProjectileObject : MonoBehaviour, IProjectile
{
    public float Force { get; set; }
    public PhotonPlayer ShootingPlayer { get; set; }
	
	[Header("This script requires PhotonView obervation!")]
    public float ForceScalar = 350f;
    public bool Spin = false;
    public bool FlipWhenTurning = false;
	public ForceMode2D _ForceMode = ForceMode2D.Force;

    //Private variables
	private Rigidbody2D _body;
    private bool _facingRight;
	private Vector3 _netPosition;
	private Quaternion _netRotation;
	private Vector2 _netVelocity;
	private float _netAngularVelocity;
	private bool _netSending;


    private float _lerpSmoothness = 7f;// 15f;

	void Awake()
	{
		_body = GetComponent<Rigidbody2D>();
	}

	void Start()
	{
        if (_body.fixedAngle)
            transform.rotation = Quaternion.identity;
	   
        //Instantiaton data
        // data[0] = force scale (0-1)
        // data[1] = player velocity
		object[] data = GetComponent<PhotonView>().instantiationData;
		if (data != null)
		{
			float force = (float)data[0];
			Vector2 velocity = (Vector2)data[1];
			Fire(force, velocity);
		    ShootingPlayer = GetComponent<PhotonView>().owner;
		}

		if (GetComponent<PhotonView>() != null)
		{
			if (!GetComponent<PhotonView>().isMine)
			{
				StartCoroutine("UpdateData");
			}
		}

	    if (ShootingPlayer != null && GetComponent<Impact>() != null)
	        GetComponent<Impact>().ShootingPlayer = ShootingPlayer;
	}

    private void Update()
    {
        if (FlipWhenTurning && _body.velocity.x < 0 && _facingRight)
        {
            Flip();
        }
        if (FlipWhenTurning && _body.velocity.x > 0 && !_facingRight)
        {
            Flip();
        }
    }


    void Flip()
    {
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
        _facingRight = !_facingRight;
    }

    [PunRPC]
	void SetPositionimmediately()
	{
		if (!_netSending)
			return;
		
		transform.position = _netPosition;
		transform.rotation = _netRotation;
		_body.velocity = _netVelocity;
		_body.angularVelocity = _netAngularVelocity;
	}

	[PunRPC]
	void Detonate(object[] data)
	{
		if (GetComponent<Impact>() != null)
			GetComponent<Impact>().Spawn((Vector3) data[0], (Vector3)data[1]);
	}

	//Smoothly transition to receiving location
	IEnumerator UpdateData()
	{
		while (true)
		{
			//Debug.Log(transform.position + " -> " + netPosition);

			if (_netSending)
			{
				if (Mathf.Abs(transform.position.sqrMagnitude - _netPosition.sqrMagnitude) > 0.25f)
					transform.position = Vector3.Lerp(transform.position, _netPosition, Time.deltaTime * _lerpSmoothness);

                _body.velocity = Vector3.Lerp(_body.velocity, _netVelocity, Time.deltaTime * _lerpSmoothness);
                _body.angularVelocity = Mathf.Lerp(_body.angularVelocity, _netAngularVelocity, Time.deltaTime * _lerpSmoothness);
			}
			yield return null;
		}
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting) //Send position
		{
			stream.SendNext(transform.position);
			stream.SendNext(transform.rotation);
			stream.SendNext(_body.velocity);
			stream.SendNext(_body.angularVelocity);
			stream.SendNext(true);
		}
		else //Receive position
		{
			_netPosition = (Vector3)stream.ReceiveNext();
			_netRotation = (Quaternion)stream.ReceiveNext();
			_netVelocity = (Vector2)stream.ReceiveNext();
			_netAngularVelocity = (float)stream.ReceiveNext();
			_netSending = (bool)stream.ReceiveNext();
		}
	}

    /// <summary>
    /// Sends the projectile flying
    /// </summary>
    /// <param name="force">force scale</param>
    /// <param name="velocity">player current velocity (projectile initial velocity)</param>
	[PunRPC]
	public void Fire(float force, Vector2 velocity)
	{

        //Gets escape angle as vector2
		float escapeAngle = transform.eulerAngles.z;
        escapeAngle += 180;
        Vector2 directionAngle = AngleAsVector2(escapeAngle);

		float exitForce = force * ForceScalar;

		_body.drag *= 1.1f - force;
		_body.velocity = velocity;
		_body.AddForce(directionAngle * exitForce, _ForceMode);
		
		//Add spin to projectile
		if (Spin)
		{
			float angularVelocity = force;
			if (escapeAngle < 450 && escapeAngle > 270)
				angularVelocity *= -1;

			_body.AddTorque(angularVelocity);
		}

        //Send the projectile pointing towards the ground as it falls
        if (GetComponent<ProjectileAngularTrajectory>() != null)
        {
			GetComponent<ProjectileAngularTrajectory>().force = force;
			GetComponent<ProjectileAngularTrajectory>().Begin();
        }
	}

    private Vector2 AngleAsVector2(float angle)
    {
        var radians = angle * (Mathf.PI / 180);

        Vector2 escapeAngleAsDirectionVector = new Vector2();
        escapeAngleAsDirectionVector.x = Mathf.Cos(radians);
        escapeAngleAsDirectionVector.y = Mathf.Sin(radians);

        escapeAngleAsDirectionVector.Normalize();
        return escapeAngleAsDirectionVector;
    }
}
