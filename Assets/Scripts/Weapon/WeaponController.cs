using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WeaponController : MonoBehaviour
{
    //Inspector properties
	public WeaponUseMode useMode;
	public WeaponFireMode fireMode;
	public bool chargeUp;
    private float particleSystemDuration = 2.1f;
	public float reloadTime;
	public float recoil;

[Header("Weapon Script (Imporant)")]
	public Object IWeaponScript;
	public NetworkFireMode networkFire;

[Header("Cosmetics")]
	public bool HideSpriteOnReload;
	public SpriteRenderer WeaponSprite;
    public Vector2 GubbeOffset = Vector2.zero;

[Header("Worms Mode")]
    public float TimeToFire = 0;

	public enum NetworkFireMode { SyncProjectile, RPCFire }
	public delegate void OnFire();
    public event OnFire FireEvent;

	//Properties
	public bool show { get; set; }
	public bool CurrentPlayer {
		get {
			return (_movement != null) && _movement.CurrentPlayer;
		}
	}
	public bool EnableFire {
		get {
			if (!_isMine)
				return false;
			if (_reloading)
				return false;
			if (Input.GetKey(KeyCode.LeftShift))
			    return false;
			if (useMode == WeaponUseMode.Still)
				return (_movement != null) && (!_movement.Moving && _movement.Grounded);
			else
				return (_movement != null) && !_movement.Tumbling;
		}
	}
	public bool ShowForceIndicator {
		get {
			if (!chargeUp)
				return false;
			if (fireMode != WeaponFireMode.Automatic && _triggerNeedReset)
				return false;
			else
				return Fireing;
		}
	}
	public bool facingRight {
		get {
			return (_movement != null) && _movement.FacingRight;
		}
	}
	public Vector2 velocity {
		get {
            return (_movement != null) ? _movement.Velocity : Vector2.zero;
		}
	}
	private bool ShowWeapon {
		get {
			if (!CurrentPlayer)
				return false;

			if (useMode == WeaponUseMode.Still)
				return (_movement != null) && (!_movement.Moving && _movement.Grounded);
			else
				return !_movement.Tumbling;
		}
	}
	private bool Fireing {
		get {
			return (_fireTime != 0);
		}
	}
	public string WeaponName {
		get {
			return gameObject.name.Replace("(Clone)", "");
		}
	}
	
    //Private variables
	private IWeapon _launcher;
	private PlayerManager _movement;
	private bool _lastShow;
	private float _fireTime;
	private float _force;
	private bool _triggerNeedReset;
    private bool _reloading;
    private bool _isMine = true;
    private int _groundLayers = (1 << 11) | (1 << 12) | (1 << 13);
    private bool _fired;

    /// <summary>
    /// Sets the player as parent to the weapon
    /// Called as RPC over network with PhotonView ID
    /// </summary>
    /// <param name="child">network id of child (weapon)</param>
    /// <param name="parent">network id of parent (player)</param>
    [PunRPC]
    void SetParent(int child, int parent)
    {
        var thischild = PhotonView.Find(child);
        var thisparent = PhotonView.Find(parent);
		bool wasFacingRight = false;

		if (thisparent.GetComponent<PlayerManager>().FacingRight)
		{
			wasFacingRight = true;
			thisparent.GetComponent<PlayerManager>().FacingRight = false;
		}
		thischild.transform.parent = thisparent.transform;

        _movement = GetComponentInParent<PlayerManager>();

        /*
        if (_movement.Character == 1)
            thischild.transform.localPosition = Vector3.zero;
        if (_movement.Character == 2)
            thischild.transform.localPosition = GubbeOffset;
        */
        SwitchCharacter(_movement.Character);
        
		thisparent.GetComponent<PlayerManager>().FacingRight = wasFacingRight;
    }

	void Awake()
	{
        _launcher = (IWeapon)IWeaponScript;
        _movement = GetComponentInParent<PlayerManager>();
        _isMine = GetComponent<PhotonView>().isMine;

	    if (Woldat.KjellKod)
	    {
            useMode = WeaponUseMode.Always;
	        chargeUp = false;
	        reloadTime = Mathf.Min(0.25f, reloadTime);
	    }
	}

	public void SwitchCharacter(int character)
	{
	    return;

		if (character == 1)
        {
            transform.localPosition = Vector3.zero;
            if (transform.Find("ManHand") != null)
			    transform.Find("ManHand").gameObject.SetActive(false);
            if (transform.Find("WormHand") != null)
			    transform.Find("WormHand").gameObject.SetActive(true);
		}
		if (character == 2)
		{
            transform.localPosition = GubbeOffset;
            if (transform.Find("ManHand") != null)
                transform.Find("ManHand").gameObject.SetActive(true);
            if (transform.Find("WormHand") != null)
                transform.Find("WormHand").gameObject.SetActive(false);
		}
	}

	void Start()
	{
        //Set max-aim-duration if using qucik force indicator
	    AimController aimController = GetComponent<AimController>();
        if (aimController != null)
	    {
            if (aimController.ForceIndicator != null && aimController.QuickForceIndicator)
	            particleSystemDuration = 0.95f;
	    }
	}

	void Update()
	{
        if (_movement == null)
            _movement = GetComponentInParent<PlayerManager>();

		//If not controlling, abort controls
		if (!CurrentPlayer)
		{
			show = ShowWeapon;
			return;
		}

        if (!_isMine || Woldat.DisableInput)
			return;

        if (AmmoController.WeaponAmmo(_movement.InventoryId)[WeaponName] == 0)
        {
            show = ShowWeapon;
            return;
        }

		//Reset the trigger on key down
        if (_movement.GetButtonDown("Fire"))
		{
			//if reloading, reset the trigger
			//preventing the need to time fire button with the reloadTime
			//EnableFire will still be held up by reloading
			if (EnableFire || _reloading)
			{
				if (fireMode != WeaponFireMode.Automatic)
					_triggerNeedReset = false;
			}
		}
		
		//Increase force
        if (EnableFire && !_triggerNeedReset && _movement.GetButton("Fire"))
		{
			//If weapon doesn't use charge, just fire with full force
			if (!chargeUp)
			{
				_force = 1;
				Fire ();
			}
			else
			{
				if (_fireTime == 0)
					_fireTime = Time.time;

				//2.1 is the duration of the chargeUpParticleSystem
				_force = Mathf.Min(Time.time - _fireTime, particleSystemDuration);
                _force = _force / particleSystemDuration;
				
				if (_force >= 1) //Max force, fire
				{
					Fire();
				}
			}
		}
		
		//Release fire button, fire away
        if (!_triggerNeedReset && _movement.GetButtonUp("Fire"))
		{
			//Debug.Log(Time.time + "Fireup " + Fireing);
			if (fireMode != WeaponFireMode.Automatic)
				_triggerNeedReset = false;

			if (EnableFire && _force > 0)
				Fire();
		}
		
		//Reset firetime to prevent continous calls to Fire()
        if (!EnableFire || _triggerNeedReset || !_movement.GetButton("Fire"))
		{
			_fireTime = 0;
		}

		//Show the weapon, variable 'show' is used by WeaponNetworkRenderer
		show = ShowWeapon;
	}

	//Enable or disable weapon renderers
	void FixedUpdate()
	{
		if (show == _lastShow)
			return;
		
		foreach (Renderer r in GetComponentsInChildren<Renderer>())
			r.enabled = show;
		
		_lastShow = show;
	}

    /// <summary>
    /// Call Fire-function on the IWeapon script attached
	/// </summary>
    /// <param name="overrideForce">used by AI script to set aim-force</param>
	public void Fire(float overrideForce = -1)
	{
		if (AmmoController.WeaponAmmo(_movement.InventoryId)[WeaponName] == 0)
			return;

		if (FireEvent != null) {
			FireEvent();
		}

	    if (overrideForce > 0)
	        _force = overrideForce;

		if (_launcher == null)
		{
			Debug.Log("Launcher är null, något har fuckat ur?!");
			Debug.Log("Testar applya launcher igen...");
			_launcher = (IWeapon)IWeaponScript;
		}
		Vector3 spawnPosition = _launcher.ProjectileSpawn;

		//Check if there is clear bath between spawn point and player
		//To prevent from shooting throught objects when standing close
        if (Physics2D.Linecast(transform.position, spawnPosition, _groundLayers))
	        spawnPosition = transform.position;

	    var spawnAngle = transform.eulerAngles;

        if (facingRight)
    	    spawnAngle.z *= -1;

	    if (fireMode == WeaponFireMode.PointToWorld)
	        spawnPosition = GetComponent<AimController>().GetWorldAimPosition();

        //Checks network-fire-mode and calls RPC or instantiates synced network projectile
        if (Woldat.Settings.IsNetwork && networkFire == NetworkFireMode.RPCFire)
			GetComponent<PhotonView>().RPC("CallFire", PhotonTargets.All, _force, velocity, spawnPosition, spawnAngle);
        else
			CallFire(_force, velocity, spawnPosition, spawnAngle);

		//Apply recoil
        if (recoil > 0 && _movement != null)
			_movement.Recoil(recoil * _force, spawnAngle.z);

		//Reset shot
		_fireTime = 0;
		_force = 0;

        //Do not decrese ammo for infinite (-1) weapons
        if (AmmoController.WeaponAmmo(_movement.InventoryId)[WeaponName] > 0)
            AmmoController.WeaponAmmo(_movement.InventoryId)[WeaponName]--;

		if (reloadTime >= 0)
		{
			Invoke("Reload", reloadTime);
			_reloading = true;
		}

		if (fireMode != WeaponFireMode.Automatic)
			_triggerNeedReset = true;

        if (Woldat.Settings.PlayMode == WoldatSettings.WoldatPlayMode.Worms)
            _movement.StartCoroutine(_movement.Retreat(TimeToFire));
	}

	//PhotonView requires this function for monitored scripts
    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        
    }

    /// <summary>
    /// Calls the firescript from attached IWeapon script 
    /// </summary>
    /// <param name="_force"></param>
    /// <param name="_velocity"></param>
    /// <param name="_position"></param>
    /// <param name="_rotation"></param>
    [PunRPC]
    public void CallFire(float _force, Vector2 _velocity, Vector3 _position, Vector3 _rotation)
    {
        PhotonPlayer shootingPlayer = GetComponent<PhotonView>().owner;

		if (HideSpriteOnReload && WeaponSprite != null)
		{
			WeaponSprite.enabled = false;
			Invoke("ShowSprite", reloadTime);
		}

        _launcher.Fire(_force, _velocity, _position, _rotation, shootingPlayer);
    }

	void Reload()
	{
		_reloading = false;
	}

	void ShowSprite()
	{
		WeaponSprite.enabled = true;
	}
}