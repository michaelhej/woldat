﻿using UnityEngine;
using System.Collections;
using JetBrains.Annotations;

public interface IWeapon
{
	void Fire(float force, Vector2 velocity, Vector3 position, Vector3 rotation, PhotonPlayer shootinPlayer);
	Vector3 ProjectileSpawn { get; }
}

public interface IProjectile
{
	void Fire(float force, Vector2 velocity);
    PhotonPlayer ShootingPlayer { get; set; }
}

public enum WeaponUseMode { Always, Still }
public enum WeaponFireMode { Manual, Automatic, PointToWorld }