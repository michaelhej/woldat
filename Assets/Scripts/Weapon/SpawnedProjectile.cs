﻿using UnityEngine;
using System.Collections;

public class SpawnedProjectile : MonoBehaviour
{
	public Vector2 Direction;
	public float Force;
	public float Torque;
	public float Delay;

	void Start()
	{
		if (Delay > 0)
			Invoke("Fire", Delay);
		else
			Fire();
	}

	void Fire()
	{
		Direction.Normalize();
		GetComponent<Rigidbody2D>().AddForce(Direction * Force);
		GetComponent<Rigidbody2D>().AddTorque(Torque);
	}
	
	// Update is called once per frame
	void Update()
	{
	
	}
}
