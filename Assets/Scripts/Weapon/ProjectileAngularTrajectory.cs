using UnityEngine;
using System.Collections;

public class ProjectileAngularTrajectory : MonoBehaviour {

    public enum Trajectory { AngularForce, DampAngle }
    public Trajectory trajectory = Trajectory.DampAngle;

    //Force trajectory
    public float angularForceConstant = 140f;
    public float angularSlowForceConstant = 140f;

    //Damp trajectory
    public float dampConstant = 1.9f;
    public float dampAngularConstant = 1.75f;

    public float force = 1;

    private Rigidbody2D body;
    private float angleMagnitude = 0;
    private float escapeAngle;
    private Vector2 escapeAngleAsDirectionVector;

    private bool angleTriggered = false;
    private float lastAngle = 470;
    float velf = 0;

    bool hasBegun = false;

    void Awake()
    {
        body = GetComponent<Rigidbody2D>();
    }

    void Start()
    {

        escapeAngle = transform.eulerAngles.z;

        escapeAngle += 180;
        var radians = escapeAngle * (Mathf.PI / 180);

        escapeAngleAsDirectionVector = new Vector2();
        escapeAngleAsDirectionVector.x = Mathf.Cos(radians);
        escapeAngleAsDirectionVector.y = Mathf.Sin(radians);

        escapeAngleAsDirectionVector.Normalize();


        //Vinkelskillnad från 0 (rakt upp)
        angleMagnitude = escapeAngle - 450f;
        if (angleMagnitude > -180)
            angleMagnitude = Mathf.Abs(angleMagnitude);

        if (angleMagnitude < 0)
            angleMagnitude += 360;

        angleMagnitude /= 180;
        angleMagnitude = 1 - angleMagnitude;
    }

	public void Begin()
    {
        hasBegun = true;
        
        float angularVelocity = ((angularForceConstant + (1 - force) * angularSlowForceConstant) * body.gravityScale * angleMagnitude);

        if (escapeAngle < 450 && escapeAngle > 270)
            angularVelocity *= -1;

        if (trajectory == Trajectory.AngularForce)
            body.angularVelocity = angularVelocity;
            
	}

    void FixedUpdate()
    {
		if (!hasBegun)
			return;

        float angle = transform.eulerAngles.z;

		if (!angleTriggered)
		{
            if (angle > 90 && lastAngle < 90 && body.angularVelocity > 0)
                angleTriggered = true;

            if (angle < 90 && lastAngle > 90 && body.angularVelocity < 0)
                angleTriggered = true;

            lastAngle = angle;
        }
		else
		{
            transform.eulerAngles = new Vector3(0, 0, 90);
        }

        if (trajectory == Trajectory.DampAngle)
        {
            Vector3 angles = transform.eulerAngles;
            angles.z = Mathf.SmoothDampAngle(angles.z, 90, ref velf, dampConstant * (dampAngularConstant - angleMagnitude) * force);
            transform.eulerAngles = angles;
        }
    }
}
