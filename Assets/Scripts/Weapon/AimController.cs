﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AimController : MonoBehaviour
{
	
	public GameObject ForceIndicator;
	public Sprite AimSprite;
	public Vector2 ForceAimOffset;
    public bool QuickForceIndicator = false;
    public bool MouseClickAim = false;

	private float _maxAim = 125;
	private float _minAim = 0;
	
	private float _maxMouseAim = 0;
	private float _minMouseAim = 35;

	private ParticleSystem _indicatorParticle;
    private AudioSource _chargeAudio;
    private GameObject _spriteObject;
    private WeaponController _controller;
    private PlayerManager _movement;

	private bool _lastShow;
	private bool _hasIndicator = false;

    private Vector3 _worldAimPosition;

	void Start()
	{
	    if (transform.parent == null)
	        return;

	    if (transform.parent.gameObject.name == "MenuPlayer")
	    {
	        enabled = false;
	        return;
        }
        _controller = GetComponent<WeaponController>();
        _movement = GetComponentInParent<PlayerManager>();

		//Instantiate force indicator and make child
		if (ForceIndicator != null)
			CreateForceIndicator();

		if (AimSprite != null)
			CreateAim();
	}

	void CreateForceIndicator()
	{
		_hasIndicator = true;
		ForceIndicator = Instantiate(ForceIndicator, transform.position, Quaternion.identity) as GameObject;
		ForceIndicator.transform.parent = transform;
		_indicatorParticle = ForceIndicator.GetComponentInChildren<ParticleSystem>();
	    _chargeAudio = ForceIndicator.GetComponent<AudioSource>();

		ForceIndicator.transform.localPosition = ForceAimOffset * 0.01f;
		_indicatorParticle.Stop();
	}

	void CreateAim()
	{
		_spriteObject = Instantiate(new GameObject("Aim"), transform.position, transform.rotation) as GameObject;
		_spriteObject.tag = "Aim";
		_spriteObject.transform.parent = transform;
        _spriteObject.transform.localPosition = ( ForceAimOffset + new Vector2(-60f, 0) ) * 0.01f;
		
		SpriteRenderer aimRenderer = _spriteObject.AddComponent<SpriteRenderer>();
		aimRenderer.sprite = AimSprite;
		aimRenderer.sortingLayerName = "Weapon";

        if (MouseClickAim)
            aimRenderer.sortingLayerName = "Effects";
    }

    public void HideAim()
    {
        if (_spriteObject!= null && _spriteObject.GetComponentInChildren<SpriteRenderer>() != null)
            _spriteObject.GetComponentInChildren<SpriteRenderer>().enabled = false;
    }

	void Update()
	{
		if (!_hasIndicator)
			return;

		if (_controller.ShowForceIndicator && !_lastShow)
        {
            _indicatorParticle.gameObject.SetActive(true);
			_indicatorParticle.Play();
            _indicatorParticle.GetComponent<ParticleSystemRenderer>().enabled = true;
            _chargeAudio.Play();
			_lastShow = true;
		}

		if (!_controller.ShowForceIndicator && _lastShow)
		{
			_indicatorParticle.Stop();
			_indicatorParticle.Clear();
            _chargeAudio.Stop();
            _lastShow = false;
            _indicatorParticle.gameObject.SetActive(false);
		}
	}

	void FixedUpdate()
	{
	    if (_controller == null)
	        return;

		if (!_controller.CurrentPlayer)
			return;

		if (_hasIndicator)
			MoveForceIndicator();

	    if (MouseClickAim)
	    {
	        MouseAimWorld();
	        return;
	    }

        if (Woldat.Settings.InputMode == WoldatSettings.WoldatInputMode.GamePad)
            KeyboardAim(_movement.GetAxis("Aim") * 3f);
        else if (Woldat.Settings.PlayMode == WoldatSettings.WoldatPlayMode.Worms)
		{
			if (Woldat.Settings.WormsAimMode == WoldatSettings.WoldatWormsAimMode.Keyboard)
                KeyboardAim(_movement.GetAxis("Aim") * -2f);
			else if (Woldat.Settings.WormsAimMode == WoldatSettings.WoldatWormsAimMode.Mouse)
				MouseAimLimit();
		}
		else if (Woldat.Settings.PlayMode == WoldatSettings.WoldatPlayMode.Soldat)
		{
            MouseAimNoLimit();
		}
	}

    public Vector3 GetWorldAimPosition()
    {
        return _worldAimPosition;
    }

    private void MouseAimWorld()
    {
        if (_worldAimPosition == Vector3.zero)
            _worldAimPosition = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, Camera.main.nearClipPlane));

        if (Woldat.Settings.WormsAimMode == WoldatSettings.WoldatWormsAimMode.Keyboard)
        {
            _worldAimPosition.x += _movement.GetAxis("SideAim") * .3f;
            _worldAimPosition.y -= _movement.GetAxis("Aim") * .3f;
            _worldAimPosition.z = 9;
        }
        else
        {
            Vector3 mousePosition = Input.mousePosition;
            mousePosition.z = 9;
            _worldAimPosition = Camera.main.ScreenToWorldPoint(mousePosition);
        }


        if (_spriteObject != null)
            _spriteObject.transform.position = _worldAimPosition;
    }
    
    void MoveForceIndicator()
	{
        //Move ForceIndicator.
        //ParticleSystem does not work with scale.x

	    Vector3 angles = ForceIndicator.transform.eulerAngles;

        if (!_controller.facingRight)
			angles.z = transform.eulerAngles.z;

	    ForceIndicator.transform.eulerAngles = angles;
    }

	void MouseAimNoLimit()
	{
		MouseAimLimit();
	}
	
	//Keyboard aim
	void KeyboardAim(float direction)
	{
        if (direction == 0)
            return;

		Vector3 rotation = transform.localRotation.eulerAngles;
		
		float aimRotation = rotation.z + 90;
		
		rotation.z = ClampAngle(aimRotation + direction, _minAim, _maxAim) - 90;
		
		transform.eulerAngles = rotation;
	}

	//Mouse aim
	void MouseAimLimit()
	{
		Vector3 mouse_pos = Input.mousePosition;
        Vector3 object_pos = Camera.main.WorldToScreenPoint(transform.position);
		mouse_pos.x = mouse_pos.x - object_pos.x;
		mouse_pos.y = mouse_pos.y - object_pos.y;
		float angle = Mathf.Atan2(mouse_pos.y, mouse_pos.x) * Mathf.Rad2Deg;

	    SetAim(angle);
	}
	
	float ClampAngle(float angle, float min, float max)
	{
        if (angle < 90 || angle > 270) // if angle in the critic region...
        {
			if (angle > 180) angle -= 360;  // convert all angles to -180..+180
			if (max > 180) max -= 360;
			if (min > 180) min -= 360;
		}
		
		angle = Mathf.Clamp(angle, min, max);
		
		if (angle < 0)
			angle += 360;  // if angle negative, convert to 0..360
		
		return angle;
	}

    public void SetAim(float angle)
    {
        if (!_controller.facingRight)
        {
            if (angle < 90 - _maxMouseAim && angle > 0)
                angle = 90 - _maxMouseAim;
            if (angle > -90 - _minMouseAim && angle < 0)
                angle = -90 - _minMouseAim;

            angle += 180;
        }
        else
        {
            angle *= -1;
            angle = 360 - angle;
            angle = ClampAngle(angle, 310 + _maxMouseAim, 485 - _minMouseAim);
        }

        transform.eulerAngles = new Vector3(0, 0, angle);
    }
}
