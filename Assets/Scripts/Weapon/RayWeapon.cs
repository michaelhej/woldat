using UnityEngine;
using System.Collections;

public class RayWeapon : MonoBehaviour, IWeapon
{
    public float Force { get; set; }
	public float Range;
	public bool Trample;
    public float Damage;
	public bool OnlyMaxForce = true;
    public bool ParticleDamage = false;
    public Transform SpawnPosition;
    public GameObject ImpactEffect;
    public ParticleSystem RayEffect;


	[Header("Laser")]
	public bool DrawLaser;
	public Color LaserColor1;
	public Color LaserColor2;
	public float LaserWidth1 = 0.05f;
	public float LaserWidth2 = 0.05f;
	
	public Vector3 ProjectileSpawn {
		get
		{
			return SpawnPosition.position;
		}
		set {}}

    private WeaponController controller;
	private LineRenderer line;
	private float _force = 1;

	void Awake()
	{
		controller = GetComponent<WeaponController>();
		line = GetComponentInChildren<LineRenderer>();

		if (line == null)
			Debug.Log("No LineRenderer found!");
	}

	void Start()
	{
        if (ImpactEffect != null)
            ImpactEffect.SetActive(false);
        if (RayEffect != null)
            RayEffect.Stop();
	}

	[PunRPC]
	public void Fire(float force, Vector2 velocity, Vector3 position, Vector3 rotation, PhotonPlayer shootingPlayer = null)
	{
        if (force < 1 && OnlyMaxForce)
	        return;

		_force = Mathf.Clamp(force, 0.1f,1);

        //Play sound, but du not interrupt if there is a rayeffect (then sound is looping)
	    if (GetComponent<AudioSource>() != null && (!GetComponent<AudioSource>().isPlaying || RayEffect == null))
	    {
	        GetComponent<AudioSource>().volume = _force;
	        GetComponent<AudioSource>().Play();
	    }

	    var prev = rotation.z;

        line.enabled = false;
	    if (controller.facingRight)
	        rotation.z *= -1;
		else
			rotation.z = rotation.z - 180;

        Debug.Log("Prev: " + prev + " angle: " + rotation.z + " right: " + controller.facingRight);

		Vector2 direction = Woldat.EulerToVector2(rotation.z);


	    if (RayEffect != null)
	    {
	        RayEffect.Play();
	        StopAllCoroutines();
	        StartCoroutine("StopRay");
	    }

	    RaycastHit2D[] hits = Physics2D.RaycastAll(SpawnPosition.position, direction, Range * _force);

        if (hits == null)
			return;

		foreach (RaycastHit2D hit in hits)
		{
            if (hit.collider.tag == "World" || hit.collider.tag == "WorldObject")
            {
                HitImpact(hit.point);
                return;
            }

            if (hit.collider.tag == "Projectile")
            {
                GameObject projectile = hit.collider.gameObject;
                Impact impact = projectile.GetComponent<Impact>();
                if (impact != null && impact.Volatile && impact.enabled && impact.VolatileThreshold <= Damage)
                {
                    impact.TimedDetonation();
                }

                HitImpact(hit.point);

                if (!Trample)
                    return;	
            }

            //If player, apply damage
            if (!ParticleDamage && hit.collider.tag == "Player" && hit.collider.transform != gameObject.transform.parent)
            {
                GameObject player = hit.collider.gameObject;
                if (player.GetComponent<DealDamage>() != null)
                    player.GetComponent<DealDamage>().TakeDamage(Damage * _force, shootingPlayer);

                HitImpact(hit.point);
                
                if (!Trample)
                    return;	
            }
        }
		Vector3 v = direction;
		DrawLine(ProjectileSpawn, ProjectileSpawn + v * Range);
	}

    private void Update()
    {
        if (RayEffect == null)
            return;

        Vector3 rotation = transform.rotation.eulerAngles;
        line.enabled = false;

		if (!controller.facingRight)
			rotation.z = rotation.z - 180;

         RayEffect.gameObject.transform.eulerAngles = new Vector3(rotation.z - 180, -90, 0);
    }

    void HitImpact(Vector3 position)
	{
        if (ImpactEffect != null)
        { 
	        ImpactEffect.transform.position = position;
            ImpactEffect.SetActive(true);
        }

		DrawLine(ProjectileSpawn, position);

		Impact impact = GetComponent<Impact>();
		if (impact != null)
		{
			impact.Spawn(position, transform.position, false);
		}
	}
	
	void DrawLine(Vector3 pos1, Vector3 pos2)
	{
		if (!DrawLaser)
			return;
		line.SetColors(LaserColor1, LaserColor2);
		line.SetVertexCount(2);
		line.SetPosition(0, pos1);
		line.SetPosition(1, pos2);
		line.SetWidth(LaserWidth1 * _force, LaserWidth2 * _force);
		line.sortingLayerName = "Projectile";
		line.enabled = true;

        StopAllCoroutines();
    	StartCoroutine("FadeOut");
	}

    private IEnumerator StopRay()
    {
        yield return new WaitForSeconds(0.25f);

        if (GetComponent<AudioSource>() != null)
            GetComponent<AudioSource>().Stop();

        if (RayEffect != null)
            RayEffect.Stop();
    }

    IEnumerator FadeOut()
	{
		Color tColor1 = LaserColor1;
		Color tColor2 = LaserColor2;

		while (tColor1.a > 0 || tColor2.a > 0)
		{
			tColor1.a -= 0.1f;
			tColor2.a -= 0.1f;
			
			if (tColor1.a < 0)
				tColor1.a = 0;

			if (tColor2.a < 0)
				tColor2.a = 0;
			
			line.SetColors(tColor1, tColor2);

			yield return null;
		}
		line.enabled = false;

        if (ImpactEffect != null)
            ImpactEffect.SetActive(false);
        if (RayEffect != null)
            RayEffect.Stop();
	}

}
