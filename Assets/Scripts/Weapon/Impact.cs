﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework.Constraints;
using Random = UnityEngine.Random;

public class Impact : MonoBehaviour
{
    public PhotonPlayer ShootingPlayer { get; set; }
    public bool WaitingToDetonate { get; set; }

    //Variables set in inspector
	public GameObject Explosion;
	public bool OnImpact = true;
	public bool OnlyImpactOnWorld = false;
    public bool Volatile = true;
    public float VolatileThreshold = 0;

[Tooltip("Time to detonate. 0 = no timed detonation")]
	public float DetonationTime;
    public bool GlobalDetonationTime = true;

[Header("Cluster (Optional)")]
    public GameObject Cluster;
    public List<GameObject> RandomClusterList; 
	public int Count;

[Range(0,180)]
    public int Spread;
    public float ExplosiveForce;
    public ForceMode2D ClusterForceMode = ForceMode2D.Force;


[Header("Pre-detonate sound")]
    public AudioClip PreDetonateClip;
    public float PreDetonateTime;

    //Private variables
	private int _seed = 0;
    private int _layerMask = (1 << 11) | (1 << 12) | (1 << 13) | (1 << 14); //Ground, SolidObject, Crate, Projectile
    private PhotonView _photonView;
    private float _timeSinceSpawn;

    private void Awake()
    {
        _photonView = GetComponent<PhotonView>();
        _timeSinceSpawn = Time.timeSinceLevelLoad;
    }

    void Start()
	{
        if (DetonationTime > 0)
		{
            if (InventoryController.DetonationTime > 0 && GlobalDetonationTime)
		        DetonationTime = InventoryController.DetonationTime;

		    Invoke("TimedDetonation", DetonationTime);

            if (PreDetonateClip != null && PreDetonateTime <= DetonationTime)
                Invoke("PlayDetonateClip", DetonationTime - PreDetonateTime);
		}

        //If network object, sets a seed based on time (from spawning player) and photon-data
        if (Woldat.Settings.IsNetwork && _photonView != null)
		{
            int photonSeed = _photonView.instantiationId;
            photonSeed += _photonView.ownerId;
            photonSeed += _photonView.viewID;
		
			int timeSeed = 0;
            object[] data = _photonView.instantiationData;

			if (data != null)
				timeSeed = (int)((float)data[2] * 100);

            _seed = timeSeed + photonSeed + Count + Spread;
		}
	}

	/// <summary>
    /// Sync object position to other players
	/// </summary>
	private void SyncPosition()
	{
	    try
	    {
	        if (Woldat.Settings.IsNetwork && GetComponent<PhotonView>() != null)
	        {
	            if (GetComponent<PhotonView>().isMine)
	                GetComponent<PhotonView>().RPC("SetPositionimmediately", PhotonTargets.Others);
	        }
        }
        catch (Exception ex)
        {
            Debug.Log("Failed to set position for " + gameObject.name);
            Debug.Log(ex.Message);
        }
	}

	private void Detonate(Vector3 position)
	{
		if (!NetworkDetonate(position))
			Spawn(position, transform.position);
	}

    /// <summary>
    /// If applicable, send RPC command to detonate over netork, otherwise return false
    /// </summary>
    /// <param name="impactPosition"></param>
    /// <returns></returns>
    private bool NetworkDetonate(Vector3 impactPosition)
    {
        if (Woldat.Settings.IsNetwork && GetComponent<PhotonView>() != null)
        {
            if (GetComponent<PhotonView>().isMine)
                GetComponent<PhotonView>().RPC("Detonate", PhotonTargets.All, impactPosition, transform.position);
            return true;
        }
        return false;
    }

    /// <summary>
    /// Collision with other object
    /// </summary>
    /// <param name="collision"></param>
	private void OnCollisionEnter2D(Collision2D collision)
	{
	    if ((collision.gameObject.tag == "World" || collision.gameObject.tag == "WorldObject") || !OnlyImpactOnWorld)
	    {
	        SyncPosition();

	        if (!collision.contacts.Any())
	            return;

	        Vector3 instantiatePosition = collision.contacts[0].point;
	        if (OnImpact)
	            Detonate(instantiatePosition);
	    }
	}

    private void PlayDetonateClip()
    {
        //GetComponent<AudioSource>().PlayOneShot(PreDetonateClip);

        GetComponent<AudioSource>().clip = PreDetonateClip;
        GetComponent<AudioSource>().Play();
    }

    /// <summary>
    /// Timed detonation, detonate with current position as "collision-point"
	/// </summary>
	public void TimedDetonation()
	{
        //Prevents cluster from being detonated with their origin-explosion
	    if (Time.timeSinceLevelLoad - _timeSinceSpawn < 0.15f)
	        return;

		Detonate(transform.position);
	}

	/// <summary>
    /// Spawn clusters and/or explosion objects
	/// </summary>
	/// <param name="position">Impact position</param>
	/// <param name="transformPosition">Position of gameobject at moment of detonation</param>
	/// <param name="destroyOnImpact"></param>
	public void Spawn(Vector3 position, Vector3 transformPosition, bool destroyOnImpact = true)
	{
		//Make sure position is always the same across all network players
		transform.position = transformPosition;
        
        //Calculates angle between object and impact point
		Vector3 impactAngle = transform.position - position;
		impactAngle.Normalize();

        //Applies synced network seed
		if (_seed > 0)
            Random.seed = _seed;
		
		int spawnedClusters = 0;
		if (Cluster != null)
		{
			for (int i = 0; i < Count; i++)
            {
				//Randomize spread-direction of clusters
				Vector3 dir = new Vector2(Random.Range(-Spread, Spread), ( 180 - Random.Range(0, Spread * 2) ) );

                //Adds impact vector to spread vector
				dir.Normalize();
				dir += impactAngle * 0.5f;
				dir.Normalize();
				Vector3 spawnOffset = dir * 0.1f;

				//Offsets cluster slightly to make sure they don't spawn on each other
                int offsetTries = 0;
                while (Physics2D.OverlapCircle(position + spawnOffset, 0.1f, _layerMask, 0) && offsetTries < 15)
                {
                    spawnOffset += dir * 0.1f;
                    offsetTries++;
                }

			    // Make sure there is a clear path between origin and spawning clusters
				// to prevent from spawning cluster on other side of walls or objects
				if(Physics2D.Linecast(position + spawnOffset, position + impactAngle * 0.2f, (1 << 11) | (1 << 12)))
                    continue;

                //Set random cluster
                if (RandomClusterList != null && RandomClusterList.Count > 0)
                {
                    Cluster = RandomClusterList[Random.Range(0, RandomClusterList.Count)];
			    }

			    //Spawn clusters and send them flying
			    GameObject cluster = Instantiate(Cluster, position + spawnOffset, transform.rotation) as GameObject;
			    cluster.name = Cluster.name;
			    cluster.GetComponent<Rigidbody2D>().AddForce(dir * ExplosiveForce, ClusterForceMode);
			    cluster.GetComponent<Rigidbody2D>().AddTorque(dir.x * -0.5f);
			    cluster.GetComponent<Impact>().ShootingPlayer = ShootingPlayer;

			    spawnedClusters++;
			}

            //If no cluser were spawned, try changing position slightly
            //Note: destroyOnImpact is false for way-reapons, don't want to change weapon transform
            if (spawnedClusters == 0 && Count > 0 && Spread < 180 && destroyOnImpact)
            {
                Spread += 35;
                if (Spread > 180)
                    Spread = 180;
                Spawn(transform.position + new Vector3(0, -0.15f), transform.position + new Vector3(0, -0.15f));
                return;
            }
		}

		//Instantiate explosion-object
		if (Explosion != null)
		{
			GameObject explosion = Instantiate(Explosion, position, transform.rotation) as GameObject;
            if (ShootingPlayer != null)
	    	    explosion.GetComponent<Explosion>().ShootingPlayer = ShootingPlayer;
		}

		//Destroy on impact (default on projectiles)
		if (destroyOnImpact)
        {
            Destroy(gameObject);
			if (Woldat.Settings.IsNetwork && GetComponent<PhotonView>() != null && GetComponent<PhotonView>().isMine)
				PhotonNetwork.Destroy(gameObject);
			else
				Destroy(gameObject);
		}
	}

}
