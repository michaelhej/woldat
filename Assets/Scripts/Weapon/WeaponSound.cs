﻿using UnityEngine;
using System.Collections;

public class WeaponSound : MonoBehaviour {

    private WeaponController _weaponController;
    private PlayerManager _manager;
    private AudioSource _audioSource;
    
	void Start () {
        _weaponController = GetComponent<WeaponController>();
        _audioSource = GetComponent<AudioSource>();
	    _manager = GetComponentInParent<PlayerManager>();
        _audioSource.loop = true;
	}

	void Update()
	{
        if (_manager.GetButtonDown("Fire") && AmmoController.WeaponAmmo(_manager.InventoryId)[_weaponController.WeaponName] != 0)
		{
            if (_audioSource.isPlaying == false)
            {
                _audioSource.Play();
			}
		}
        else if (!_manager.GetButton("Fire") || AmmoController.WeaponAmmo(_manager.InventoryId)[_weaponController.WeaponName] == 0)
		{
            _audioSource.Stop();
		}
	}
}
