﻿using UnityEngine;
using System.Collections;

public class ExplosiveImpact : MonoBehaviour
{
    public GameObject Explosion;
	public float DetonateAfter;

	void Start()
	{
		if (DetonateAfter > 0)
			Invoke("TimedDetonation", DetonateAfter);
	}

	void OnCollisionEnter2D(Collision2D collision)
    {
        //Collider2D collider = collision.collider;
        Vector3 instantiatePosition = collision.contacts[0].point;
		Detonate(instantiatePosition);
    }

	void TimedDetonation()
	{
		Detonate(transform.position);
	}

	void Detonate(Vector3 position)
	{
		if (Explosion != null)
			Explosion = Woldat.Instantiate(Explosion, position, transform.rotation) as GameObject;
		Destroy(gameObject);
	}
}
