﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class AmmoController : MonoBehaviour
{
    private static Dictionary<int, Dictionary<string, int>> _ammo;
    private static int _currentTeam;

    /// <summary>
    /// Get current ammo for weapon
    /// </summary>
	public static Dictionary<string, int> WeaponAmmo_
	{
		get
		{
            //If WORMS-mode, ammo is per team
            if (Woldat.Settings.PlayMode == WoldatSettings.WoldatPlayMode.Worms)
		        _currentTeam = Woldat.Player.GetComponent<PlayerManager>().PlayerTeam.Id;

            if (_ammo == null)
			{
				_ammo = new Dictionary<int, Dictionary<string, int>>();
			}

		    if (!_ammo.ContainsKey(_currentTeam))
		    {
                _ammo.Add(_currentTeam, new Dictionary<string, int>());
                InitAmmo(_currentTeam);
		    }

            return _ammo[_currentTeam];
		}
	}

    public static Dictionary<string, int> WeaponAmmo(int player)
    {
        _currentTeam = player;

        if (_ammo == null)
        {
            _ammo = new Dictionary<int, Dictionary<string, int>>();
        }

        if (!_ammo.ContainsKey(_currentTeam))
        {
            _ammo.Add(_currentTeam, new Dictionary<string, int>());
            InitAmmo(_currentTeam);
        }

        return _ammo[_currentTeam];
    }

    public static void AddAmmo(int inventoryId, string wpn, int count, bool print = true)
    {
        Text txt = GameObject.Find("Btn" + wpn + "/Text").GetComponent<Text>();

        if (txt != null && print)
            Woldat.PrintChat("<color=brown>You got <b>" + txt.text + "</b> x <b>" + count + "</b></color>");

        if (WeaponAmmo(inventoryId)[wpn] > -1)
            WeaponAmmo(inventoryId)[wpn] = WeaponAmmo(inventoryId)[wpn] + count;
    }

    public static void SetAmmo(int inventoryId, string wpn, int count, bool print = true)
    {
        Text txt = GameObject.Find("Btn" + wpn + "/Text").GetComponent<Text>();
        string countString = count.ToString();

        if (count == -1)
            countString = "<color=lime>infinite</color>";

        if (txt != null && print)
            Woldat.PrintChat("<color=orange>You have <b>" + countString + " " + txt.text + "</b></color>");

        WeaponAmmo(inventoryId)[wpn] = count;
    }

    public static void ResetAmmo()
    {
        _ammo = null;
    }

    //Default values
    private static void InitAmmo(int inventoryId) 
	{
		//-1 = infinite
		WeaponAmmo(inventoryId).Add("Revolver", -1);
        WeaponAmmo(inventoryId).Add("WpnGrenade", -1);
        WeaponAmmo(inventoryId).Add("WpnSmokeGrenade", 3);
        WeaponAmmo(inventoryId).Add("Sniper", 5);
        WeaponAmmo(inventoryId).Add("Bazooka", 15);
        WeaponAmmo(inventoryId).Add("WpnDynamite", 2);
        WeaponAmmo(inventoryId).Add("FlameThrower", 150);
        WeaponAmmo(inventoryId).Add("Minigun", 150);
        WeaponAmmo(inventoryId).Add("WpnDragonball", 5);
        WeaponAmmo(inventoryId).Add("WpnBanana", 5);
        WeaponAmmo(inventoryId).Add("Lazer", 30);
        WeaponAmmo(inventoryId).Add("WpnHolyGrenade", 0);
        WeaponAmmo(inventoryId).Add("Banooka", 3);
        WeaponAmmo(inventoryId).Add("WpnCrazyCat", 0);
        WeaponAmmo(inventoryId).Add("Airstrike", 1);

        WeaponAmmo(inventoryId).Add("GrenadeLauncher", 2);			//.....
        WeaponAmmo(inventoryId).Add("RayBay", 10);			//.....
        WeaponAmmo(inventoryId).Add("Shotgun", 15);			//.....

        //Default ammo for WORMS-mode
	    if (Woldat.Settings.PlayMode == WoldatSettings.WoldatPlayMode.Worms)
        {
            SetAmmo(inventoryId, "FlameThrower", 0, false);
            SetAmmo(inventoryId, "Minigun", 10, false);
            SetAmmo(inventoryId, "Lazer", 2, false);
            SetAmmo(inventoryId, "Banooka", 0, false);
            SetAmmo(inventoryId, "WpnSmokeGrenade", 0, false);
            SetAmmo(inventoryId, "Bazooka", 20, false);
            SetAmmo(inventoryId, "RayBay", 0, false);
            SetAmmo(inventoryId, "GrenadeLauncher", 0, false);
            SetAmmo(inventoryId, "Shotgun", 2, false);
            SetAmmo(inventoryId, "Sniper", 0, false);
	    }
	}
}
