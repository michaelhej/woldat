﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class DealDamage : MonoBehaviour
{
    public bool ShowDamageTaken = true;
	private GameObject _damageCanvas;
	private Vector3 _tempVector;
    private Queue<int> _damageToBeTaken;

    private PhotonPlayer lastShootingPlayer;

    private void Start()
    {
        _damageToBeTaken = new Queue<int>();
        GetComponent<PlayerManager>().ReSpawn += ClearDamage;
    }

    public void TakeDamage(float dmg, PhotonPlayer shootingPlayer = null)
    {
        int damage = (int) dmg;
        if (shootingPlayer == null)
            Debug.Log("No shooting player. Assuming local game.");
        else if (GetComponent<PhotonView>().isMine)
        {
            //Update total damge taken and delt
            AddToPlayerStats("DmgDealt", damage, shootingPlayer);
            AddToPlayerStats("DmgTaken", damage, PhotonNetwork.player);

            //Send damage dealt and taken to logging-server
            Logdat.AddDmgDealt(shootingPlayer, (int)dmg);
            Logdat.AddDmgTaken(PhotonNetwork.player, (int)dmg);

            lastShootingPlayer = shootingPlayer;
        }
        _damageToBeTaken.Enqueue(damage);
        StartCoroutine(DisplayDamageTaken());
    }

    private IEnumerator DisplayDamageTaken()
    {
        Rigidbody2D body = GetComponent<Rigidbody2D>();

        //While the player is flying, wait...
        while (!body.fixedAngle)
        {
            yield return new WaitForEndOfFrame();
        }
        
        int damage = 0;
        //Take total accumulated damage at the same time (worms-style)
        while (_damageToBeTaken.Count > 0)
            damage += _damageToBeTaken.Dequeue();
        
        //Sends RPC to display the damage taken
		if (damage > 0 && gameObject.GetComponent<PlayerManager>().Health > 0)
        {
            if (PhotonNetwork.inRoom && GetComponent<PhotonView>().viewID > 0)
                GetComponent<PhotonView>().RPC("DisplayDamageTaken", PhotonTargets.All, damage);
            else
                DisplayDamageCanvas(damage);
        }
    }

    private void ClearDamage()
    {
        _damageToBeTaken.Clear();
    }

    public void DisplayDamageCanvas(int damage)
    {
		if (damage >= 1)
        {
            PlayerManager movement = gameObject.GetComponent<PlayerManager>();
            float PlayersCurrentHealth = movement.Health;

            if (ShowDamageTaken)
            { 
			    _damageCanvas = Instantiate (Resources.Load ("HealtBar/DamageCanvas"), transform.position, Quaternion.identity) as GameObject;
			    Destroy (_damageCanvas, 1.5f);
			
			    //Assign player to canvas
			    _damageCanvas.GetComponent<DamageIndicator>()._playerObject = gameObject;
			    _damageCanvas.transform.GetChild (0).GetComponent<Text>().text = "-" + damage;
            }

			if (damage >= PlayersCurrentHealth)
            {
				PlayersCurrentHealth = 0;
			} 
			else 
			{
				PlayersCurrentHealth -= damage;
			}

            movement.Health = PlayersCurrentHealth;

            if (PlayersCurrentHealth <= 0 && GetComponent<PhotonView>().isMine)
            {
                if (PhotonNetwork.inRoom)
                { 
                    GetComponent<PhotonView>().RPC("Die", PhotonTargets.All, lastShootingPlayer);
                    
                    //Adds kill and death
                    AddToPlayerStats("Kills", 1, lastShootingPlayer);
                    AddToPlayerStats("Deaths", 1, PhotonNetwork.player);

					if ((int)lastShootingPlayer.customProperties["Team"] != (int)PhotonNetwork.player.customProperties["Team"])
					{
	                    //Add team score for killing player
	                    AddTeamScore(lastShootingPlayer);
					}

                    //Send kills and death to logging-server
                    Logdat.AddKill(lastShootingPlayer);
                    Logdat.AddDeath(PhotonNetwork.player);

					//Add team kill
					Logdat.Teamkill((int)lastShootingPlayer.customProperties["Team"], (int)PhotonNetwork.player.customProperties["Team"], lastShootingPlayer, PhotonNetwork.player);

                    //Add log text
                    SendKillLog(lastShootingPlayer, PhotonNetwork.player);
                }
                else
                {
                    GetComponent<PlayerManager>().Die();
                }
            }
		}
	}

    private void SendKillLog(PhotonPlayer killer, PhotonPlayer killed)
    {

        string txt = "";
        txt += "<color=" + PlayerColor(killer) + ">";
        txt += killer.name + "</color>";

        txt += " killed ";

        txt += "<color=" + PlayerColor(killed) + ">";
        txt += killed.name + "</color>";

        
        Woldat.Player.GetComponent<PhotonView>()
            .RPC("SendChat", PhotonTargets.AllBufferedViaServer, txt, "custom", "", "", -1);
    }

    private string PlayerColor(PhotonPlayer player)
    {
        
        int team = (int) player.customProperties["Team"];

        if (team == 1)
            return "magenta";

        if (team == 2)
            return "yellow";

        return "black";
    }

    private void AddToPlayerStats(string data, int value, PhotonPlayer player)
    {
        if (!PhotonNetwork.inRoom || player == null)
            return;

        int totalDmg = player.customProperties.ContainsKey(data) ? (int)player.customProperties[data] : 0;
        totalDmg += value;

        Hashtable newTotalDmg = new Hashtable() { { data, totalDmg } };
        player.SetCustomProperties(newTotalDmg);
    }

    private void AddTeamScore(PhotonPlayer player)
    {
        if (!PhotonNetwork.inRoom)
            return;

        int team = player.customProperties.ContainsKey("Team") ? (int)player.customProperties["Team"] : -1;
        
        if (team == -1)
            return;
        string teamProp = "ScoreTeam" + team.ToString();

        int score = PhotonNetwork.room.customProperties.ContainsKey(teamProp) ? (int)PhotonNetwork.room.customProperties[teamProp] : 0;
        score += 1;

        Hashtable newTeamScore = new Hashtable() { { teamProp, score } };
        PhotonNetwork.room.SetCustomProperties(newTeamScore);
    }
}
