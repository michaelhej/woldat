﻿using UnityEngine;
using System.Collections;

public class Airstrike : MonoBehaviour
{
	public string Projectile;
	public Vector2 SpawnOffset;
	public float FlySpeed;
	public bool ZoomOutOnSpawn;
	
	[Header("Drop control")]
	public float DropInterval;
	
	[Header("Continous drops")]
	public bool ContinousDrops;
	public float startDelay;

	[Header("Fixed drops")]
	public Vector2 TargetPosition;
	public float DropDistanceToTarget;
    public float ProjectileVerticalSpeed;
    [Tooltip("Drop-array with number of projectiles dropped each drop")]
	public int[] Drops;
    public float BurstSpeed;
    [Header("Optional sounds")]
    public AudioClip DropSound;


	//public bool FlyFromLeft;

	private bool dropped = false;
	private int dropCount = 0;
	private Vector3 startPosition;

	// Use this for initialization
	void Start()
	{
        if (WorldManager.DisableAirstrikes)
        { 
            Destroy(gameObject);
            return;
        }
		GetComponent<Rigidbody2D>().velocity = new Vector2(FlySpeed, 0);
		if (GetComponent<PhotonView>().instantiationData != null)
		{
			object[] data = GetComponent<PhotonView>().instantiationData;
			TargetPosition = (Vector2)data[0];
		}

		if (ProjectileVerticalSpeed > 0)
		{
            DropDistanceToTarget = (transform.position.y - TargetPosition.y) / ProjectileVerticalSpeed;
			DropDistanceToTarget += (TargetPosition.y * -0.75f) * (1 - ProjectileVerticalSpeed);
		    DropDistanceToTarget -= 1.5f;
            Debug.Log("Drop distrance: " + DropDistanceToTarget);
		}

		if (ContinousDrops)
		{
			startPosition = transform.position;
			InvokeRepeating("DropOnce", startDelay, DropInterval);
		}
		
		if (ZoomOutOnSpawn && Woldat.Settings.PlayMode == WoldatSettings.WoldatPlayMode.Worms)
			Camera.main.GetComponent<CameraFollow>().ZoomOutOnPosition(TargetPosition, (TargetPosition.x / FlySpeed) + 3);
	}

	void DropOnce()
	{
		Vector3 dropPosition = startPosition;
		dropPosition.x += dropCount*DropInterval * FlySpeed;
		dropPosition += (Vector3)SpawnOffset;
		Instantiate(Resources.Load("Projectile/" + Projectile), dropPosition, Quaternion.identity);
		dropCount++;
	}

	// Update is called once per frame
	void Update()
	{
		if (transform.position.x >= TargetPosition.x - DropDistanceToTarget && !dropped)
		{
			if (Drops.Length > 0)
			{
				StartCoroutine("Drop");
				dropped = true;
                if (DropSound != null)
                    GetComponent<AudioSource>().PlayOneShot(DropSound, 1);
			}
		}

		if (transform.position.x > WorldManager.LowerRightBoundary.x)
			Destroy(gameObject, 1);
	}

	IEnumerator Drop()
	{
		Vector3 dropPosition = TargetPosition + SpawnOffset;
		dropPosition.x -= DropDistanceToTarget;
		dropPosition.y = transform.position.y - 0.2f;
		
		Debug.Log("Dropping " + Projectile + " at " + dropPosition);

		for (int i = 0; i < Drops.Length; i++)
		{
			for (int j = 0; j < Drops[i]; j++)
			{
				Instantiate(Resources.Load("Projectile/" + Projectile), dropPosition, Quaternion.identity);
				if (BurstSpeed > 0)
				{
					dropPosition.x += BurstSpeed * FlySpeed * 1.25f;
					yield return new WaitForSeconds(BurstSpeed);
				}
			}

			dropPosition.x += FlySpeed * DropInterval;
			yield return new WaitForSeconds(DropInterval);
		}
	}
}
