﻿using UnityEngine;
using System.Collections;

public class WeaponNetworkRenderer : MonoBehaviour
{
	float smoothness = 12f;

	Quaternion rotation;
	Vector3 scale;
	bool show;
	bool netSending = false;

	WeaponController controller;
	
	void Awake ()
	{
		controller = GetComponent<WeaponController>();

		//If another network players weapon, remove all controls
		if (!GetComponent<PhotonView>().isMine)
		{	
			//GetComponent<WpnLauncherController>().enabled = false;
			GetComponent<AimController>().enabled = false;

			//if (controller.IWeaponScript != null)
			//	Destroy(controller.IWeaponScript);

			StartCoroutine("UpdateData");
		}
	}

	//Smoothly transition to receiving location
	IEnumerator UpdateData()
	{
        while (true)
		{
			if (netSending)
			{
				//transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * smoothness);
				transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * smoothness);
				transform.localScale = scale;
			}
			
			yield return null;
		}
	}

	//On network update
	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting) //Send position
		{
			stream.SendNext(transform.rotation);
			stream.SendNext(transform.localScale);
			stream.SendNext(controller.show);
			stream.SendNext(true);
		}
		else //Receive position
		{
			rotation = (Quaternion)stream.ReceiveNext();
			scale = (Vector3)stream.ReceiveNext();
			controller.show = (bool)stream.ReceiveNext();
			netSending = (bool)stream.ReceiveNext();
		}
	}
}
