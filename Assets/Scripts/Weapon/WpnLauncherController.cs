﻿using UnityEngine;
using System.Collections;

public class WpnLauncherController : MonoBehaviour, IWeapon
{
	//Inspector variables
	public Transform SpawnPosition;
	public string Projectile;
    public bool HasDetonationTime = false;

    //Properties
	public Vector3 ProjectileSpawn
    {
        get
		{
			return SpawnPosition.position;
		}
    }

    //Private variables
	private WeaponController _controller;

	void Awake()
	{
		_controller = GetComponent<WeaponController>();
	}

    private void Start()
    {
        if (HasDetonationTime)
            InventoryController.RemindDetonationTime = true;
    }

    public void Fire(float force, Vector2 velocity, Vector3 position, Vector3 rotation, PhotonPlayer shootingPlayer)
	{
        //Flip rotation of instantiated object
        if (_controller.facingRight)
			rotation.z = 180 - rotation.z;

        //Instantiate projectile and add necessary variables
        GameObject o = Woldat.InstantiateProjectileWithForce("Projectile/" + Projectile, shootingPlayer, force, velocity, position, Quaternion.Euler(rotation));

        if (Woldat.Settings.PlayMode == WoldatSettings.WoldatPlayMode.Worms)
        {
            if (Camera.main.GetComponent<CameraFollow>() != null)
                Camera.main.GetComponent<CameraFollow>().OverrideTarget(o.transform);
        }
	}
}
