﻿using UnityEngine;
using System.Collections;

public class WpnSpawnPlane : MonoBehaviour, IWeapon
{
	//Inspector properties
	public string SpawnObject;
	
	public Vector3 ProjectileSpawn
    {
        get
		{
			return Camera.main.ScreenToWorldPoint(Input.mousePosition);
		}
    }
	
	public void Fire(float force, Vector2 velocity, Vector3 position, Vector3 rotation, PhotonPlayer shootingPlayer)
	{
		Vector2 targetPosition = position;
        Debug.Log("Fire at " + position);

		if (Woldat.Settings.IsNetwork)
			PhotonNetwork.Instantiate("SpawningObject/" + SpawnObject, WorldManager.PlaneSpawnPosition, Quaternion.identity, 0, new object[] {targetPosition});
		else
		{
			GameObject Airplane = Instantiate(Resources.Load("SpawningObject/" + SpawnObject), WorldManager.PlaneSpawnPosition, Quaternion.identity) as GameObject;
			Airplane.GetComponent<Airstrike>().TargetPosition = targetPosition;
		}

        //Detach weapon when using mouse-click weapons
        GetComponentInParent<WeaponChanger>().SetWeapon("");
	}
}
