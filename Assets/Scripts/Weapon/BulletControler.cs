﻿using UnityEngine;
using System.Collections;

public class BulletControler : MonoBehaviour, IWeapon
{
	//Used game componenets
	public Transform SpawnPosition;
	public string Projectile;
	public ParticleSystem PSystem;

	public float FireRate = 0.1f;
	public float BulletSpeed;
	
	public Vector3 ProjectileSpawn
    {
        get
		{
			return SpawnPosition.position;
		}
    }

	
	//Properties
	public float Damage { get; set; }
	public GameObject Player { get; set; }

	private WeaponController _controller;
    private PlayerManager _manager;


	void Start()
	{
		_controller = GetComponent<WeaponController>();
        _manager = GetComponentInParent<PlayerManager>();
	}

	void Update()
	{
	    if (_manager == null)
	        return;

        if (_manager.GetButtonUp("Fire") && PSystem != null) 
		{
			if(PSystem.isPlaying)
				PSystem.Stop(true);
		}
	}

	public void Fire(float force, Vector2 velocity, Vector3 position, Vector3 rotation, PhotonPlayer shootingPlayer)
	{
        if (_manager.GetButtonDown("Fire")) 
		{
			if(PSystem != null && PSystem.isPlaying)
				PSystem.Play(true);
		}
		
		if (_controller.facingRight)
			rotation.z = 180 - rotation.z;

		GameObject projectile = Instantiate(Resources.Load("Projectile/" + Projectile), position, Quaternion.Euler(rotation)) as GameObject;
		projectile.GetComponent<IProjectile>().Fire(force, velocity);
		projectile.GetComponent<IProjectile>().ShootingPlayer = GetComponent<PhotonView>().owner;
	}
}
