﻿using UnityEngine;
using System.Collections;

public class TerrainGenerator : MonoBehaviour
{

    public GameObject player;

    //Walls
    public GameObject[] TerrainTop;
    public GameObject[] TerrainMidTop;
    public GameObject[] TerrainMidBottom;
    public GameObject[] TerrainBottom;
    public GameObject[] TerrainOverlap;

    //Walkway
    public GameObject[] Walkway;

    //Random crap
    public GameObject[] Roots;


    private enum OverlapStyle { WestTop, EastTop, WestBottom, EastBottom, Mid1, Mid2, None }
    void Start()
    {
        Generate(0);
    }

    void Generate(int x)
	{
		GeneratePlatform(-10, 0, 20, 4);
		GeneratePlatform(10, 2, 4, 4);

		return;
        /*
		int lastX = 4;
        int lastY = 0;
        while (lastX < 50)
        {
            int[] lasts = RandomPlatform(lastX, lastY);
            lastX = lasts[0];
            lastY = lasts[1];
        }
        */
        //GenerateStraight(3, 1, 6, Walkway);

    }

    int[] RandomPlatform(int lastX, int lastY)
    {
        int xPos = Random.Range(lastX - 1, lastX + 2);
        int yPos = lastY;
        int length = Random.Range(2, 10);
        int height = Random.Range(5, 7);
        length += length % 2;

        if (Random.Range(-1, 1) >= 0)
            yPos += Random.Range(2, 4);
        else
            yPos -= Random.Range(1, 3);

        while (Vector2.Distance(new Vector2(lastX, lastY), new Vector2(xPos, yPos)) > 4 && yPos > lastY) 
        {
            xPos--;
            if (xPos < -1)
            {
                Debug.Log("ops");
                break;
            }
        }
        
        GeneratePlatform(xPos, yPos, length, height);

        int[] val = { xPos + length, yPos };
        return val;
    }

    void GeneratePlatform(int x, int y, int w, int h)
    {
        GenerateStraight(x, y, w, TerrainTop);

        int i = 1;
        while (i < h - 1)
        {
            GenerateStraight(x, y - i, w, (i % 2 == 0) ? TerrainMidTop : TerrainMidBottom);
            i++;
        }

        if (i < h)
            GenerateStraight(x, y - i, w, TerrainBottom);
    }

    void GenerateStraight(int x, int y, int length, GameObject[] terrain)
    {
        if (length % 2 == 1)
            throw new UnityException("Platform length must be a factor of 2");

        if (length < 2)
            throw new UnityException("Pltaform length must be >= 2");
        
        //Left edge
        MakeTerrain(terrain[0], new Vector3(x, y));

        //Middle walls
        for (int i = 0; i < length - 2; i+=2)
        {
            MakeTerrain(terrain[1], new Vector3(x + i + 1, y));
            MakeTerrain(terrain[2], new Vector3(x + i + 2, y));
        }

        //Right edge
        MakeTerrain(terrain[3], new Vector3(x + length - 1, y));

        if (length > 2 && terrain == TerrainBottom && Random.Range(0, 3) < 2)
        {
            //AddRoots(x, y, length);
        }
    }

    void MakeTerrain(GameObject o, Vector2 position)
    {
        Collider2D other = Physics2D.OverlapCircle(position, 0.1f);
        if (other != null)
        {
            Destroy(other.gameObject);
            OverlapStyle overlap = GetOverlap(o, other.gameObject);
            switch (overlap)
            {
                case OverlapStyle.WestTop:
                    o = TerrainOverlap[0];
                    break;
                case OverlapStyle.EastTop:
                    o = TerrainOverlap[1];
                    break;
                case OverlapStyle.WestBottom:
                    o = TerrainOverlap[2];
                    break;
                case OverlapStyle.EastBottom:
                    o = TerrainOverlap[3];
                    break;
                case OverlapStyle.Mid1:
                    o = TerrainMidTop[2];
                    break;
                case OverlapStyle.Mid2:
                    o = TerrainMidBottom[2];
                    break;
            }
        }
        
        Instantiate(o, position, Quaternion.identity);
    }

    OverlapStyle GetOverlap(GameObject o1, GameObject o2)
    {
        string names = o1.name + o2.name;

        if (names.Contains("West-0")
            && names.Contains("East"))
            return OverlapStyle.WestTop;

        if (names.Contains("East-0")
            && names.Contains("West"))
                return OverlapStyle.EastTop;

        if (names.Contains("East-3")
            && names.Contains("West"))
                return OverlapStyle.WestBottom;
        
        if (names.Contains("West-3")
            && names.Contains("East"))
                return OverlapStyle.EastBottom;
        
        if (names.Contains("West-1")
            && names.Contains("East"))
            return OverlapStyle.Mid1;

        if (names.Contains("West-2")
            && names.Contains("East"))
            return OverlapStyle.Mid2;
        /*
        if (names.Contains("Mid1-3")
            && names.Contains("East"))
            return OverlapStyle.EastBottom;

        if (names.Contains("Mid2-3")
            && names.Contains("East"))
            return OverlapStyle.EastBottom;
         * */
        
        return OverlapStyle.None;
    }

    void AddRoots(int x, int y, int length)
    {
        float xPos = x;
        xPos += Random.Range(1.5f, length - 1.5f);

        float yPos = y;
        yPos -= 1.2f;
        yPos -= Random.Range(0, 1);
        MakeTerrain(Roots[0], new Vector3(xPos, yPos));
    }
}