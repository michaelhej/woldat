﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnClouds : MonoBehaviour
{
	[Header("Sprite array")]
	public Sprite[] Clouds;
	public float CloudScale = 1;
	
	[Header("Spawn Control")]
	public float SpawnInterval;
	public float SpawnIntervalVariation;
	
	[Header("Spawn Position")]
	//public float StartX = 20f;
	public float Altitude;
	public float AltitudeVariation;
	public float Depth;
	public float DepthVariation;
	
	[Header("Behavour")]
	public float Speed;
	public float SpeedVariation;

	private float StartX;
	private float StopX;

	void Start ()
	{
		if (Speed < 0 )
		{
			StartX = WorldManager.LowerRightBoundary.x + 2 + Depth * 2 + DepthVariation * 2;
            StopX = WorldManager.TopLeftBoundary.x - 2 - Depth * 2 - DepthVariation * 2;
		}
		else
		{
            StartX = WorldManager.TopLeftBoundary.x - 2 - Depth * 2 - DepthVariation * 2;
            StopX = WorldManager.LowerRightBoundary.x + 2 + Depth * 2 + DepthVariation * 2;
		}
		//Start spawning clouds
		if (Clouds.Length > 0)
			SpawnCloud();
		else
			Debug.Log("No clouds selected!");
	}

	void SpawnCloud()
	{
		//Creates cloud object and sprite renderer
		GameObject cloudObject = new GameObject("Cloud");
		cloudObject.transform.localScale *= CloudScale;
		SpriteRenderer renderer = cloudObject.AddComponent<SpriteRenderer>();

		//Select random sprite from array
		renderer.sprite = Clouds[Random.Range(0, Clouds.Length - 1)];

		//Fast clouds should appear closed than slower
		//Syncs Z position with speed and sprite sorting layer
		//Slects random wind-position to apply to depth- and speed variations
		float windPosition = Random.Range(-1f, 1f);

		//Cloud start position
		float startY = Altitude + Random.Range(-AltitudeVariation, AltitudeVariation);
		float startZ = Depth + DepthVariation * windPosition;
		cloudObject.transform.position = new Vector3(StartX, startY, startZ);

		//Clouds need rigidbody to apply velocity
		Rigidbody2D body = cloudObject.AddComponent<Rigidbody2D>();
		body.isKinematic = true;

		//Randomizes cloud velocity
		Vector2 cloudVelocity = new Vector2(0 , 0);
		cloudVelocity.x = Speed + SpeedVariation * windPosition; //Slow clouds further back
		body.velocity = cloudVelocity;

		//Sets sprite sorting layer based on wind-position
		renderer.sortingLayerName = "Clouds";
		renderer.sortingOrder = 100 - (int)(windPosition * 100);

		//Calculates cloud time to live based on world width and speed
		float WorldWidth = Mathf.Abs( StartX - StopX );
		float timeToLive = WorldWidth / Mathf.Abs(cloudVelocity.x);
		Destroy(cloudObject, timeToLive);

		//Spawns next cloud in...
		float nextCloud = SpawnInterval + Random.Range(-SpawnIntervalVariation, SpawnIntervalVariation);
		Invoke("SpawnCloud", nextCloud);
	}
}
