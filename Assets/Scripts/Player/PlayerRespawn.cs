﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerRespawn : MonoBehaviour
{
	public float RespawnTime = 5;
    public Vector3 StaticLocation = Vector3.zero;

	private List<MonoBehaviour> _scriptsToEnable;
	private float previousCameraSize;
    
	void Start()
	{
		_scriptsToEnable = new List<MonoBehaviour>();
	}

	public void RemoveAndRespawn()
	{
	    gameObject.tag = "Untagged";
        //Hide player
        GetComponent<Rigidbody2D>().isKinematic = true;

        //Remove weapon
        GetComponent<WeaponChanger>().SetWeapon("");

	    foreach (Renderer r in GetComponentsInChildren<Renderer>())
            r.enabled = false;

		//Disable scripts
		MonoBehaviour[] scripts = GetComponents<MonoBehaviour>();
		foreach (MonoBehaviour script in scripts)
		{
			if (script is PlayerRespawn || script is PlayerManager)
				continue;

			if (script.enabled)
			{
				_scriptsToEnable.Add(script);
				script.enabled = false;
			}
		}

        //Detach camera
        if (GetComponent<PlayerManager>().CurrentPlayer)
        {
            if (Camera.main.GetComponent<CameraFollow>() != null)
            { 
                previousCameraSize = Camera.main.GetComponent<CameraFollow>().size;
                Camera.main.GetComponent<CameraFollow>().ZoomOutOnPosition(transform.position, RespawnTime, previousCameraSize + 1);
            }
        }
        //Invoke("ZoomOutCamera", 3);


        //Deactivate all colliders and child gameobjects
        foreach (Collider2D collider in GetComponents<Collider2D>())
            collider.enabled = false;
		
		transform.Find("TopCollider").gameObject.SetActive(false);
		transform.Find("ParticleCollider").gameObject.SetActive(false);
		if (transform.Find("JetPack") != null)
			transform.Find("JetPack").gameObject.SetActive(false);

        //Respawn in...
	    if (Woldat.Settings.IsNetwork || Woldat.Settings.PlayMode == WoldatSettings.WoldatPlayMode.Soldat)
        {
	        StartCoroutine(Respawn());
        }
        else
        {
            foreach (SpriteRenderer sr in GetComponentsInChildren<SpriteRenderer>())
                sr.enabled = false;

            gameObject.tag = "Player";

	        if (GetComponent<PlayerManager>().CurrentPlayer)
                TurnController.NextPlayer();
	    }
	}

    private void ZoomOutCamera()
    {
        //Camera.main.GetComponent<CameraFollow>().ZoomOutOnPosition(transform.position, RespawnTime - 2);
    }

    private IEnumerator Respawn()
    {
        yield return new WaitForSeconds(RespawnTime);

        if (StaticLocation == Vector3.zero)
            transform.position = WorldManager.RandomObjectPosition;
        else
            transform.position = StaticLocation;

        yield return new WaitForSeconds(1.25f);
        Instantiate(Resources.Load("PlayerSpawnEffect"), transform.position, Quaternion.identity);
        yield return new WaitForSeconds(0.9f);

        foreach (Collider2D collider in GetComponents<Collider2D>())
            collider.enabled = true;

		
		transform.Find("TopCollider").gameObject.SetActive(true);
		transform.Find("ParticleCollider").gameObject.SetActive(true);
		//transform.Find("Worm").gameObject.SetActive(false);
		//transform.Find("GammalGubbe").gameObject.SetActive(false);
		if (transform.Find("JetPack") != null)
			transform.Find("JetPack").gameObject.SetActive(true);

        gameObject.tag = "Player";

        //Attach camera
		if (GetComponent<PlayerManager>().CurrentPlayer)
        {
            //Camera.main.GetComponent<CameraFollow>().targetSize = previousCameraSize;
		}

        //Enable body and renderer and reset rotation

        foreach (Renderer r in GetComponentsInChildren<Renderer>())
            r.enabled = true;
        //GetComponent<Renderer>().enabled = true;

        GetComponent<Rigidbody2D>().isKinematic = false;
        GetComponent<Rigidbody2D>().fixedAngle = true;
        transform.rotation = Quaternion.identity;

		//Enable script
		foreach(MonoBehaviour script in _scriptsToEnable)
		{
			script.enabled = true;
		}

        GetComponent<PlayerManager>().Live();
	}
}
