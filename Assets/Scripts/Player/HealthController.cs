﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthController : MonoBehaviour 
{
	public Vector2 offset;

	//Healthbar
    private PlayerManager _playerMovement;
    private GameObject _healthbarSpriteContainer;
    private GameObject _healthbarCanvasContainer;
	private GameObject _health;
	private Transform _healthBar;
	private SpriteRenderer _healthBarSpriteRenderer;
	private Vector3 _healthBarScale;
	private Vector3 _tempVector;

    private Image _barImage;

    private RectTransform _canvasTransform;
    private Transform _spriteTransform;
    private GameObject _foreGroundObject;
    private float _xScaleCanvas;
    private float _xScaleSprite;

	private float _HP;
	
	//spelarens start _HP
	private float _maxHP;

    private bool _isPlayerDead;

	// Use this for initialization
	void Start ()
	{
	    _maxHP = 100;

		_playerMovement = GetComponent<PlayerManager>();
        _playerMovement.ReSpawn += ResetPlayer;
        _playerMovement.Death += PlayerDies;

		//skapar HealthBarCanvas i hörnet
        _healthbarCanvasContainer = Instantiate(Resources.Load("HealtBar/HealthCanvas")) as GameObject;
        _canvasTransform = _healthbarCanvasContainer.transform.GetChild(1).GetComponent<RectTransform>();
        _xScaleCanvas = _canvasTransform.localScale.x;
        _barImage = _healthbarCanvasContainer.transform.GetChild(1).GetComponent<Image>();

        //Skapar Health bar på spelaren
        _healthbarSpriteContainer = Instantiate(Resources.Load("HealtBar/HealthBarContainer")) as GameObject;
        _healthBarSpriteRenderer = _healthbarSpriteContainer.GetComponentsInChildren<SpriteRenderer>()[1];
        _spriteTransform = _healthBarSpriteRenderer.transform;
        _xScaleSprite = _spriteTransform.localScale.x;

        //Set player as dependency of sprite renderer
        _healthbarSpriteContainer.GetComponent<ObjectDependency>().SetObject(gameObject);

        ResetPlayer();
        _HP = _playerMovement.Health;
	}

    private void PlayerDies()
    {
        _isPlayerDead = true;
        _healthbarCanvasContainer.SetActive(false);
        _healthbarSpriteContainer.SetActive(false);
    }

    private void ResetPlayer()
    {
        _isPlayerDead = false;
        //_healthbarContainer.SetActive(true);
    }

    private Color BarColor()
    {
        float x = 1 - (_HP / _maxHP);
        return new Color(2.0f * x, 2.0f * (1 - x), 0);
    }

    // Update is called once per frame
	void Update ()
	{
	    if (_isPlayerDead)
	        return;

		//testar tar 5 damage på spelaren när C trycks ner
		/*
		if (Input.GetKeyDown(KeyCode.C))
		{
			GetComponent<DealDamage>().TakeDamage(10);
		}
		*/

		_HP = _playerMovement.Health;

        _healthbarSpriteContainer.transform.position = _playerMovement.transform.position + (Vector3)offset;

        if (_playerMovement.CurrentPlayer && Woldat.Settings.PlayMode == WoldatSettings.WoldatPlayMode.Soldat)
        {
            _healthbarCanvasContainer.SetActive(true);
            _healthbarSpriteContainer.SetActive(false);

            _tempVector = _canvasTransform.localScale;
            _tempVector.x = Mathf.Clamp(_HP / _maxHP, 0, 1) * _xScaleCanvas;
            _canvasTransform.localScale = Vector3.MoveTowards(_canvasTransform.localScale, _tempVector, Time.deltaTime * 1.1f);

            _barImage.color = BarColor();
        }
        else
        {
            _healthbarCanvasContainer.SetActive(false);
            _healthbarSpriteContainer.SetActive(true);

            _tempVector = _spriteTransform.localScale;
            _tempVector.x = Mathf.Clamp(_HP / _maxHP, 0, 1) * _xScaleSprite;
            _spriteTransform.localScale = Vector3.MoveTowards(_spriteTransform.localScale, _tempVector, Time.deltaTime * 1.1f);

            _healthBarSpriteRenderer.color = BarColor();
        }
	}
}
