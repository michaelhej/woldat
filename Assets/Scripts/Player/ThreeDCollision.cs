﻿using UnityEngine;
using System.Collections;

public class ThreeDCollision : MonoBehaviour
{
    /// <summary>
    /// When colliding with invisible damagin particles from ray weapons, take damage
    /// </summary>
    /// <param name="other">Particle system game object</param>
    private void OnParticleCollision(GameObject other)
    {
        //Abort if colliding with own particles
        if (Woldat.Settings.IsNetwork && Equals(other.GetComponentInParent<PhotonView>().owner, GetComponentInParent<PhotonView>().owner))
            return;

        //Check in local games
        if (!Woldat.Settings.IsNetwork && Equals(other.GetComponentInParent<PlayerManager>(), GetComponentInParent<PlayerManager>()))
            return;

        //Get damage from attached weapon script
        float damage = other.GetComponentInParent<RayWeapon>().Damage;

        //Get shooting player from owner of weapon
        PhotonPlayer player = other.GetComponentInParent<PhotonView>().owner;

        //Apply damage to ourself
        gameObject.GetComponentInParent<DealDamage>().TakeDamage(damage, player);
    }
}
