﻿using UnityEngine;
using System.Collections;

public class WeaponChanger : MonoBehaviour {

	
	public string StartWeapon;
	
	private GameObject _weapon;
    
	public void SetWeapon(string wpn)
	{
		if (!GetComponent<PlayerManager>().IsMine)
			return;

        //Destroy previous weapon
		if (_weapon != null)
		{
			if (Woldat.Settings.IsNetwork)
				PhotonNetwork.Destroy(_weapon);
            Destroy(_weapon);
		}

		//Do not add a weapon, just destroy previous
		if (wpn == "" || !this.enabled)
		{
			return;
		}
        
        //Force player to be facing right
        //Weapon prefabs are facing right by default
		bool wasFacingRight = false;
		if (GetComponent<PlayerManager>().FacingRight)
		{
			wasFacingRight = true;
			transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, 1);
			GetComponent<PlayerManager>().FacingRight = !GetComponent<PlayerManager>().FacingRight;
		}

        //Instantiate weapon
	    _weapon = Woldat.Instantiate("Weapon/" + wpn, transform.position, Quaternion.identity);
        
        //Apply weapon as child of player
		if (Woldat.Settings.IsNetwork)
        	_weapon.GetComponent<PhotonView>().RPC("SetParent", PhotonTargets.AllBuffered, _weapon.GetComponent<PhotonView>().viewID, GetComponent<PhotonView>().viewID);
		else
			_weapon.transform.parent = transform;

        //Set right arm
        _weapon.GetComponent<WeaponController>().SwitchCharacter(GetComponent<PlayerManager>().Character);

        //Change to original facing
		if (wasFacingRight)
		{
			transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, 1);
			GetComponent<PlayerManager>().FacingRight = !GetComponent<PlayerManager>().FacingRight;
		}
        
        //Log this
        Logdat.WeaponChange(wpn);
	}
}
