﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class AI : MonoBehaviour
{
	public float speed = 4;
    private PlayerManager movement;
    private MovementControl movementControl;
    private Rigidbody2D body;

    private Vector3 lastTarget = Vector3.zero;
    private RaycastHit2D[] hits = new RaycastHit2D[2];

    public LayerMask solidLayers;

    private LineRenderer line;
    private float WaitUntil = 0;
    private AIMODE mode = AIMODE.AGGRESSIVE;

    private Dictionary<string, float> _AIEvents;
    private Vector3 lastPosition;


    private float TimeSince(string ev, float na = float.PositiveInfinity)
    {
        if (!_AIEvents.ContainsKey(ev))
			return na;
        else
            return Time.time - _AIEvents[ev];
    }

    private void SetEvent(string ev)
    {
        if (_AIEvents.ContainsKey(ev))
            _AIEvents[ev] = Time.time;
        else
            _AIEvents.Add(ev, Time.time);
    }

    void Awake()
    {
        movement = GetComponent<PlayerManager>();
        movementControl = GetComponent<MovementControl>();
        body = GetComponent<Rigidbody2D>();
    }
	// Use this for initialization
	void Start ()
    {
        _AIEvents = new Dictionary<string, float>();
	    lastPosition = transform.position;
	    ChangeWeapon();
        InvokeRepeating("CheckPosition", 5, 2);
    }

    private void CheckPosition()
    {
        if (DistanceTo(lastPosition) < 1)
            SetEvent("Stuck");
		lastPosition = transform.position;
    }

    private void ChangeWeapon()
    {
        GetComponent<WeaponChanger>().SetWeapon(RandomWeapon());
        //Invoke("ChangeWeapon", UnityEngine.Random.Range(5,20));
    }

    private string RandomWeapon()
    {
		float v = UnityEngine.Random.Range(0, 100);
		if (v < 5)
			return "Banooka";
		if (v < 10)
			return "WpnBanana";
		if (v < 15)
			return "WpnDragonball";
		if (v < 40)
			return "WpnGrenade";
		if (v < 50)
			return "WpnSmokeGrenade";
        return "Bazooka";
    }

    private float DistanceTo(GameObject o)
    {
        return Vector3.Distance(transform.position, o.transform.position);
    }

    private float DistanceTo(Vector3 position)
    {
        return Vector3.Distance(transform.position, position);
    }

    private Vector3 FindNearest(string tag)
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag(tag);

        if (players.Length == 0)
            return Vector3.zero;;

        GameObject nearest = null;
        float nearestDistance = float.PositiveInfinity;
        foreach (GameObject player in players)
		{
			//Ignore players from same team
			if (player.tag == "Player")
			{
				if (player.GetComponent<PlayerManager>().PlayerTeam.Id == movement.PlayerTeam.Id)
					continue;
			}
            float distance = DistanceTo(player);
            if (distance > 0 && distance < nearestDistance)
            {
                nearest = player;
                nearestDistance = distance;
            }
        }

        return nearest.transform.position;
    }

    private int Direction(Vector3 position)
    {
        return (position.x < transform.position.x) ? -1 : 1;
    }
    private int DirectionY(Vector3 position)
    {
        return (position.y < transform.position.y) ? -1 : 1;
    }

    /// <summary>
    /// Checks whether there is something solid blocking the path to a position
    /// </summary>
    /// <param name="position"></param>
    /// <returns>Whether path is clear</returns>
    private bool ClearPathTo(Vector3 position)
    {
        return !(Physics2D.Linecast(transform.position, position, solidLayers));
    }

    private bool ClearInFront(Vector3 position)
    {
        return !(Physics2D.Raycast(transform.position, new Vector2(Direction(position), 0), 0.6f, solidLayers));
    }

    private bool FacingGap(Vector3 position)
    {
        Collider2D h = Physics2D.OverlapPoint(transform.position + new Vector3(Direction(position) * 0.05f, -0.5f), solidLayers);
        if (h != null)
            Debug.Log(h.gameObject.name);
        return (h == null);
    }

    private AITargetDirection TargetDirection(Vector3 position)
    {
        if (transform.position.y > position.y + 2)
            return AITargetDirection.DOWNMULTI;
        if (transform.position.y > position.y + 0.1f)
            return AITargetDirection.DOWN;
        if (transform.position.y < position.y - 2)
            return AITargetDirection.UPMULTI;
        if (transform.position.y < position.y - 0.1f)
            return AITargetDirection.UP;
        return AITargetDirection.LEVEL;
    }

    /// <summary>
    /// Walk-on-the-ground-towards-target function
    /// </summary>
    /// <param name="position"></param>
    private void WalkTo(Vector3 position, float addForce = 0, bool reverse = false)
    {
        lastTarget = position;
        Vector2 velocity;
        float dir = Direction(position);

        if (mode == AIMODE.HIDE)
            reverse = true;

        if (reverse)
            dir *= -1;

        velocity = new Vector2(speed * dir, body.velocity.y);
        body.velocity = velocity;

        if (addForce > 0)
            body.AddForce(new Vector2(addForce * dir, 0));
    }

    /// <summary>
    /// Returns a position of the nearest higher level ledge
    /// </summary>
    /// <returns></returns>
    private Vector3 PositionOfNearestLedge(Vector3 position)
    {

        int numberOfChecks = (1 + (int)Mathf.Abs(position.x - transform.position.x)) * 2;
        float direction = (position.x - transform.position.x > 0) ? 1 : -1;

        //line.SetVertexCount(numberOfChecks);
        for (int i = 0; i < numberOfChecks; i++)
        {
            Vector2 checkPositionStart = (Vector2)transform.position + new Vector2(i * 0.5f * direction, 0);
            Vector2 checkPositionTop = checkPositionStart + new Vector2(0, 2f);
            if (Physics2D.LinecastNonAlloc(checkPositionStart, checkPositionTop, hits, solidLayers) > 0)
            {
                Debug.Log("Hit at " + checkPositionStart);

                //DrawLine(Vector3.back, Vector3.back);
                return (Vector3)checkPositionStart;
            }
            /*
            if (i % 2 == 0)
            { 
                line.SetPosition(i, checkPositionStart);
                line.SetPosition(i + 1, checkPositionTop);
           }
             * */
        }

        Debug.Log("No ledge found!");
        return Vector3.zero;
    }


    /// <summary>
    /// Returns a position of the nearest higher level ledge
    /// </summary>
    /// <returns></returns>
    private Vector3 PositionOfNearestShooter(Vector3 position)
    {
        int numberOfChecks = (1 + (int)Mathf.Abs(position.x - transform.position.x)) * 2;
        float direction = (position.x - transform.position.x > 0) ? 1 : -1;

        for (int i = 0; i < numberOfChecks; i++)
        {
            Vector2 checkPositionStart = (Vector2)transform.position + new Vector2(i * 0.5f * direction, 0);
            Vector2 checkPositionTop = checkPositionStart + new Vector2(0, 2f);
            if (Physics2D.LinecastNonAlloc(checkPositionStart, checkPositionTop, hits, (1 << 12)) > 0)
            {
                if (hits[0].collider.tag == "WorldShooter")
                { 
                    Debug.Log("Shooter at " + checkPositionStart);
                    return (Vector3)checkPositionStart;
                }
            }
        }

        Debug.Log("No ledge found!");
        return Vector3.zero;
    }
    
    private void WaitFor(float t)
    {
        if (mode == AIMODE.DEFENSIVE)
            t *= 1.25f;

        if (WaitUntil <= Time.time)
            WaitUntil = Time.time + t;
        else
            WaitUntil += t;
    }

    private void MoveTowards(Vector3 position)
    {
        //float distance = DistanceTo(position);
        //float speed = Time.deltaTime * 2;
        //transform.position = Vector3.MoveTowards(transform.position, position, speed);
        Vector3 nearestLedge = Vector3.zero;
        AITargetDirection direction = TargetDirection(position);

        Debug.Log("Moving towards " + position + " dir: " + direction);
        //If clear path and not far up, move towards position
        if (ClearPathTo(position))
        {
            WalkTo(position);
        }

        if (DistanceTo(position) > 2)
        {
            if (!ClearInFront(position))
            {
                Debug.Log("Jumping for obsicle");
                JumpOn(position, 2, 20);
            }
            if (FacingGap(position) && TimeSince("GapJump") > 2 && movement.Grounded)
            {
                Debug.Log("Jumping gap");
                SetEvent("GapJump");
                JumpOn(position, 1);
            }
        }
        
        if ((direction == AITargetDirection.UP || direction == AITargetDirection.DOWN ||
                  direction == AITargetDirection.LEVEL) &&
                 (nearestLedge = PositionOfNearestLedge(position)) != Vector3.zero)
        {
            //Too close to ledge, back up
            if (DistanceTo(nearestLedge) < 0.7f)
            {
                Debug.Log("Moving back from edge");
                WalkTo(nearestLedge, 3, true);
            }
            //Perfect, jump on
            else if (DistanceTo(nearestLedge) < 1.6f)
            {
                Debug.Log("Jumping edge");
                JumpOn(nearestLedge, 1);
            }
            //Far to ledge, walk towards it
            else
            {
                Debug.Log("Walking to edge");
                WalkTo(nearestLedge);
            }
        }
        else if (direction == AITargetDirection.DOWN || direction == AITargetDirection.DOWNMULTI)
        {
            WalkTo(position);
        }
        else if (direction == AITargetDirection.LEVEL)
        {
            WalkTo(position);
        }
        else if (lastTarget != Vector3.zero)
        {
            Debug.Log("Walking to last position");
            WalkTo(lastTarget);
        }
		else
		{
			WalkTo(position);
		}

        float move = body.velocity.x;

        if (move > 0 && !movement.FacingRight)
            movementControl.Flip();
        if (move < 0 && movement.FacingRight)
            movementControl.Flip();

    }

    private void JumpOn(Vector3 position, float waitFor = 1.5f, float addForce = 15)
    {
        body.AddForce(new Vector2(0, movementControl.JumpSpeed));
        WalkTo(position, addForce);
        WaitFor(waitFor);
    }


    private void EnterWorldShooter(Vector3 targetPosition)
    {
        Debug.Log("Looking for shooter");
        Vector3 shooterPosition = PositionOfNearestShooter(targetPosition);
        if (shooterPosition == Vector3.zero)
            shooterPosition = FindNearest("WorldShooter");

        if (shooterPosition == Vector3.zero)
            MoveTowards(targetPosition);
        if (DistanceTo(shooterPosition) < 1f && TargetDirection(shooterPosition) == AITargetDirection.DOWN)
        {
            WalkTo(shooterPosition);
            WaitFor(0.2f);
        }
        else if (DistanceTo(shooterPosition) < 1.00f)
        {
            WalkTo(shooterPosition, 3.5f, true);
            WaitFor(0.5f);
        }
        else if (DistanceTo(shooterPosition) < 1.5f)
        {
            JumpOn(shooterPosition, 2f, 19);
        }
        else
        {
            MoveTowards(shooterPosition);
        }
    }

    private void GoToNearestPlayer()
    {
        Vector3 playerPosition = FindNearest("Player");
        AITargetDirection direction = TargetDirection(playerPosition);
        if (TimeSince("TargetShooter") < 3 && TimeSince("Stuck") > 1 && direction == AITargetDirection.UPMULTI && mode != AIMODE.RANDOM)
        {
            SetEvent("TargetShooter");
            EnterWorldShooter(playerPosition);
        }
        else
        {
            SetEvent("TargetPlayer");
            if (mode == AIMODE.HIDE)
                MoveTowards(playerPosition);
			else if (TimeSince("Stuck") < 1 && Mathf.Abs(transform.position.x - playerPosition.x) < 0.5f)
				ShootAt(playerPosition);
            else if (mode == AIMODE.AGGRESSIVE && DistanceTo(playerPosition) < 7 && direction != AITargetDirection.DOWNMULTI && movement.Grounded)
                ShootAt(playerPosition);
            else if (DistanceTo(playerPosition) < 11 && (direction == AITargetDirection.DOWN || direction == AITargetDirection.DOWNMULTI) && FacingGap(playerPosition) && movement.Grounded)
                ShootAt(playerPosition);
            else if (DistanceTo(playerPosition) < 8 && ClearPathTo(playerPosition) && body.velocity.sqrMagnitude < 1)
                if (DistanceTo(playerPosition) < 1.6f) //If too close to player, back up
                    WalkTo(playerPosition, 1, true);
                else
                    ShootAt(playerPosition);
            else
                MoveTowards(playerPosition);

        }
    }

    private void ShootAt(Vector3 position)
    {
        if (GetComponentInChildren<WeaponController>() == null)
            return;
        SetEvent("Shoot");

        AimAt(position);

        float force = Mathf.Pow(DistanceTo(position), 1.05f) * 0.062f;
        if (TargetDirection(position) == AITargetDirection.UP)
            force *= 1.3f;
        if (TargetDirection(position) == AITargetDirection.UPMULTI)
            force *= 2f;

		Mathf.Clamp(force, 0.25f, 1);
		Debug.Log("AI force: " + force);

        GetComponentInChildren<WeaponController>().Fire(force);

        WaitFor(0.5f);
        mode = AIMODE.HIDE;
		SetEvent("Hiding");
		Invoke("ChangeMode", UnityEngine.Random.Range(2f, 20f));
    }

    private void ChangeMode()
    {
        mode = (AIMODE)UnityEngine.Random.Range(0, 1);
        Debug.Log("Setting mode: " + mode);
    }

    public void AimAt(Vector3 position)
    {
		AITargetDirection direction = TargetDirection(position);
        if (transform.position.x < position.x && !movement.FacingRight)
            movementControl.Flip();
        if (transform.position.x > position.x && movement.FacingRight)
            movementControl.Flip();

        float aim = 0;
		/*
        aim -= DistanceTo(position) * 5;
		if (direction == AITargetDirection.DOWNMULTI)
	        aim += (transform.position.y - position.y) * 2;
		else
			aim += (transform.position.y - position.y) * 3;
*/
		aim = Vector3.Angle(transform.position, position);

		Debug.Log("Aiming: " + aim + ", dis: " + DistanceTo(position) + ", dir: " + direction);

		if (Direction(position) < 0)
			aim *= -1;

		if (direction == AITargetDirection.LEVEL)
			aim -= DistanceTo(position) / 1.75f;
		if ( (direction == AITargetDirection.DOWN || direction == AITargetDirection.DOWNMULTI))
			aim += DistanceTo(position) * 4;
		else if (direction == AITargetDirection.UP)
			aim -= DistanceTo(position) * 6;
		else
			aim -= DistanceTo(position) * 4;

		if (!ClearPathTo(position))
		{
			Debug.Log("No clear path, aiming up");
			aim -= (55 - DistanceTo(position) * 4);
		}

		aim = Mathf.Clamp(aim, -90, 0);

		if (Mathf.Abs(transform.position.x - position.x) < 0.5f)
			aim = 270;

        SetAim(aim);

    }

    public void SetAim(float angle)
    {
        Debug.Log("Aimin: " + angle);
        GetComponentInChildren<AimController>().enabled = false;
        WeaponController weaponController = GetComponentInChildren<WeaponController>();

        GameObject weapon = weaponController.gameObject;
        weapon.transform.eulerAngles = new Vector3(0, 0, angle);
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time < WaitUntil)
            return;

        if (Input.GetKey(KeyCode.J) && movement.CurrentPlayer)
        {
			if (TimeSince("Hiding", float.NegativeInfinity) > 30)
			{
				mode = AIMODE.HIDE;
				Invoke("ChangeMode", 10);
				SetEvent("Hiding");
			}
            GoToNearestPlayer();
            WaitFor(0.1f);
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            FacingGap(FindNearest("Player"));
        }
    }


    enum AITargetDirection { UP, DOWN, LEVEL, UPMULTI, DOWNMULTI }
    enum AIMODE { AGGRESSIVE, DEFENSIVE, HIDE, RANDOM }
}
