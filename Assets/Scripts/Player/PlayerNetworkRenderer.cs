﻿using UnityEngine;
using System.Collections;

public class PlayerNetworkRenderer : MonoBehaviour
{
	float smoothness = 12f;

	Vector3 position;
	Quaternion rotation;
	Vector3 scale;

	PlayerManager movement;

	bool netSending = false;

	void Awake()
	{
		if (!Woldat.Settings.IsNetwork || !PhotonNetwork.connected)
			PhotonNetwork.offlineMode = true;

		movement = GetComponent<PlayerManager>();

		//If network player, remove controls and physics
		if (!GetComponent<PhotonView>().isMine)
		{
			GetComponent<Rigidbody2D>().isKinematic = true;
			GetComponent<MovementControl>().enabled = false;
			//Destroy(GetComponent<CircleCollider2D>());

			StartCoroutine("UpdateData");
		}
	}

	//Smoothly transition to receiving location
	IEnumerator UpdateData()
	{
		while (true)
		{
			if (netSending)
			{
				transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * smoothness);
				transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * smoothness * 2);
				transform.localScale = scale;
			}

			yield return null;
		}
	}

	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting) //Send data
		{
			
			stream.SendNext(true);

			//Position
			stream.SendNext(transform.position);
			stream.SendNext(transform.rotation);
			stream.SendNext(transform.localScale);

			stream.SendNext(movement.FacingRight);

			//Animation parameters
			stream.SendNext(movement.Velocity);
			stream.SendNext(movement.Grounded);

			//Health for healthbar
			stream.SendNext(movement.Health);
		}
		else //Receive data from network player
		{	
			netSending = (bool)stream.ReceiveNext();

			position = (Vector3)stream.ReceiveNext();
			rotation = (Quaternion)stream.ReceiveNext();
			scale = (Vector3)stream.ReceiveNext();

			movement.FacingRight = (bool)stream.ReceiveNext();

			//Apply player animation parameters
			Vector2 velocity = (Vector2)stream.ReceiveNext();
			bool grounded = (bool)stream.ReceiveNext();

			//Health for healthbar
			movement.Health = (float)stream.ReceiveNext();

			movement.SetAnimationParameters(velocity, grounded);

			movement.MakeKinematic();
		}
	}
}
