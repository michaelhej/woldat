using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class PlayerManager : MonoBehaviour
{
    private Rigidbody2D _body;
    private BoxCollider2D _groundCollider;
    private GameObject _nameCanvas;
    private bool _retreating;
    private string _wormName = "";
    public Animator Anim;
    public int Character = 1;
    public bool FacingRight;
    public bool Grounded;
    public LayerMask GroundLayer;
    public float Health;
    public bool IsDead;
    public bool IsMine;
    public Vector2 Velocity;
    public Team PlayerTeam { get; set; }
    public int InventoryId { get; set; }
    public string PlayerName
    {
        get
        {
            if (Woldat.Settings.PlayMode == WoldatSettings.WoldatPlayMode.Worms && _wormName != "")
                return _wormName;

            return (string) GetData("Name");
        }
        set
        {
            _wormName = value;
            UpdateData("Name", value);
        }
    }
    public int PlayerTeamNumber { get; set; }
    public bool CurrentPlayer { get; set; }
    public string PlayerPrefix { get; set; }
    public bool Moving
    {
        get { return (_body != null) && (_body.velocity.sqrMagnitude > 0.1f); }
    }
    public bool Tumbling
    {
        get { return (_body != null) && !_body.fixedAngle; }
    }
    public event Action Death;
    public event Action ReSpawn;
    private float _startHealth;
    private Dictionary<String, String> _input;
    private int _playerNumber;

    private void Awake()
    {
        Character = 1;
        _body = GetComponent<Rigidbody2D>();
        Anim = transform.Find("Worm").GetComponent<Animator>();
        //GetComponent<Animator>() ?? GetComponentInChildren<Animator>();
        IsMine = GetComponent<PhotonView>() == null || GetComponent<PhotonView>().isMine;

        SetGroundCheck();

        Anim.speed = 1f;

        //Defaults to current player
        CurrentPlayer = IsMine;


        if (PlayerTeam == null)
        {
            var team = new Team();
            team.TeamColor = Color.black;
            //new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
            PlayerTeam = team;
		}
		UpdateInputKeys();
    }

    private void Start()
    {
        //velocity = new Vector2(0, 0);

        if (IsMine && CurrentPlayer)
        {
            if (PlayerPrefix == "")
                Woldat.Player = gameObject;

            if (Camera.main.GetComponent<CameraFollow>() != null)
                Camera.main.GetComponent<CameraFollow>().SetMainTarget(transform);
        }
        _startHealth = Health;
        Invoke("AttachName", 1);
        UpdateInputKeys();
    }

    public void UpdateInputKeys()
    {
        _input = new Dictionary<string, string>();
        _input.Add("Fire", PlayerPrefix + "Fire");
        _input.Add("Aim", PlayerPrefix + "Vertical");
        _input.Add("SideAim", PlayerPrefix + "AltHorizontal");
        _input.Add("Move", PlayerPrefix + "Horizontal");
        _input.Add("Jump", PlayerPrefix + "Jump");
        _input.Add("Inventory", PlayerPrefix + "LR1");
        _input.Add("NavX", PlayerPrefix + "NavHorizontal");
        _input.Add("NavY", PlayerPrefix + "NavVertical");
    }

    public string InputKey(string inpt)
    {
        if (_input == null || !_input.ContainsKey(inpt))
        {
			return "Fire";
        }
        return _input[inpt];
    }
    public bool GetButtonDown(string btn)
    {
        return Input.GetButtonDown(_input[btn]);
    }
    public bool GetButtonUp(string btn)
    {
        return Input.GetButtonUp(_input[btn]);
    }
    public bool GetButton(string btn)
    {
        return Input.GetButton(_input[btn]);
    }

    public float GetAxis(string axis)
    {
        return Input.GetAxis(_input[axis]);
    }

    private void SetGroundCheck()
    {
        foreach (var c in GetComponents<BoxCollider2D>())
        {
            if (c.isTrigger)
            {
                _groundCollider = c;
                break;
            }
        }
    }

    private void AttachName()
    {
        _nameCanvas = Instantiate(Resources.Load("Player/NameCanvas")) as GameObject;
        _nameCanvas.GetComponent<NamePlate>().SetPlayer(gameObject);
    }

    public void Recoil(float recoil, float angle)
    {
        var forceVector = Woldat.EulerToVector2(angle, FacingRight);
        forceVector *= recoil;

        _body.AddForce(forceVector);
    }

    private void Update()
    {
        if (!IsMine)
            return;

        //Check if player is grounded
        Vector2 groundCheckTopLeft = _groundCollider.transform.position + (Vector3) _groundCollider.offset;
        var groundCheckLowerRight = groundCheckTopLeft;
        groundCheckTopLeft -= _groundCollider.size / 2;
        groundCheckLowerRight += _groundCollider.size / 2;

        Grounded = Physics2D.OverlapArea(groundCheckTopLeft, groundCheckLowerRight, GroundLayer) && _body.fixedAngle;

        //If player out of bounds, push back
        if (WorldManager.Initiated)
        {
            if (transform.position.x < WorldManager.TopLeftBoundary.x)
                _body.AddForce(new Vector2((WorldManager.TopLeftBoundary.x - transform.position.x)*15 + 20, 0));
            if (transform.position.x > WorldManager.LowerRightBoundary.x)
                _body.AddForce(new Vector2((WorldManager.LowerRightBoundary.x - transform.position.x)*15 - 20, 0));

            if (transform.position.y < WorldManager.LowerRightBoundary.y)
                _body.AddForce(new Vector2(0, 200)); //Bounce
            if (transform.position.y > WorldManager.TopLeftBoundary.y)
                _body.AddForce(new Vector2(0, (WorldManager.TopLeftBoundary.y - transform.position.y)*2));
        }

        Velocity = _body.velocity;
        SetAnimationParameters(_body.velocity, Grounded);
    }

    public void SetAnimationParameters(Vector2 velocity, bool grounded)
    {
        if (Anim == null || !Anim.isActiveAndEnabled)
            return;

        Anim.SetFloat("Speed", Mathf.Abs(velocity.x));
        Anim.SetFloat("vSpeed", velocity.y);
        Anim.SetBool("Grounded", grounded);
    }

    //Call everytime data is received from network player
    public void MakeKinematic()
    {
        if (!_body.isKinematic)
            _body.isKinematic = true;
    }

    [PunRPC]
    public void SendChat(string message, string color = "black", string sender = "", string senderColor = "",
        int sendToTeam = -1)
    {
        if (color != "custom")
        {
            message = message.Replace("<", "(");
            message = message.Replace(">", ")");

            sender = sender.Replace("<", "(");
            sender = sender.Replace(">", ")");
        }
        var msg = "";

        if (sender != "")
            msg += "<color=" + senderColor + ">" + sender + ": </color>";


        msg += "<color=" + color + ">" + message + "</color>";

        if (sendToTeam == -1 || sendToTeam == (int) PhotonNetwork.player.customProperties["Team"])
            Woldat.PrintChat(msg);
    }

    [PunRPC]
    private void SetTeam(int team)
    {
        if (team == 1)
        {
            Anim.runtimeAnimatorController =
                Resources.Load("AnimatorController/NinjaController") as RuntimeAnimatorController;
            PlayerTeam.TeamColor = new Color(255f/255f, 107f/255f, 226f/255f);
            SendChat(PlayerName + " changed team to Ninjas", "magenta");
        }
        if (team == 2)
        {
            Anim.runtimeAnimatorController = Resources.Load("AnimatorController/WormCtrl") as RuntimeAnimatorController;
            PlayerTeam.TeamColor = new Color(240f/255f, 255f/255f, 0);
            SendChat(PlayerName + " changed team to Bananas", "yellow");
        }
    }

    [PunRPC]
    public void DisplayDamageTaken(int dmg)
    {
        GetComponent<DealDamage>().DisplayDamageCanvas(dmg);
    }

    [PunRPC]
    public void Die(PhotonPlayer killedBy = null)
    {
        if (IsDead)
            return;

        if (IsMine)
            GetComponent<WeaponChanger>().SetWeapon("");

        StartCoroutine("Kill");
    }

    public void SwitchCharacter(int character)
    {
        Character = character;

        if (character == 2)
        {
            transform.Find("Ninja").gameObject.SetActive(true);
            transform.Find("Worm").GetComponent<Animator>().enabled = false;
            transform.Find("Worm").GetComponent<SpriteRenderer>().enabled = false;

            Anim = transform.Find("Ninja").GetComponent<Animator>();
        }

        if (character == 1)
        {
            transform.Find("Ninja").gameObject.SetActive(false);

            transform.Find("Worm").GetComponent<Animator>().enabled = true;
            transform.Find("Worm").GetComponent<SpriteRenderer>().enabled = true;

            Anim = transform.Find("Worm").GetComponent<Animator>();
        }
        if (GetComponentInChildren<WeaponController>() != null)
            GetComponentInChildren<WeaponController>().SwitchCharacter(character);
    }

    private IEnumerator Kill()
    {
        //Stop controls
        var wasMovementEnabled = GetComponent<MovementControl>().enabled;
        GetComponent<MovementControl>().enabled = false;
        GetComponent<WeaponChanger>().SetWeapon("");

        //Wait...
        yield return new WaitForSeconds(1);

        //Spawn explosion
        Instantiate(Resources.Load("Explosions/ExplosionPlayer"), transform.position, Quaternion.identity);

        //Kill and hide
        GetComponent<MovementControl>().enabled = wasMovementEnabled;

        if (Death != null)
            Death();

        IsDead = true;

        GetComponent<PlayerRespawn>().RemoveAndRespawn();
    }

    public IEnumerator SwitchTo()
    {
        yield return new WaitForSeconds(2);
        //Debug.Log("Switched to " + PlayerName);
        CurrentPlayer = true;
    }

    public IEnumerator Retreat(float fireTime)
    {
        if (!_retreating && GameObject.Find("MenuPlayer") == null) //Not in menu
        {
            _retreating = true;
            //Turn time ticking off
            TurnController.TickTime = 5;

            //Prevent weapon change
            GetComponent<WeaponChanger>().enabled = false;

            //Finish firetime with weapon
            yield return new WaitForSeconds(fireTime);

            //Remove weapon
            GetComponent<WeaponChanger>().SetWeapon("");

            //Retreat time
            yield return new WaitForSeconds(4 - fireTime);

            //Wait
            var previousCameraSize = Camera.main.GetComponent<CameraFollow>().size;
            Camera.main.GetComponent<CameraFollow>()
                .ZoomOutOnPosition(Camera.main.transform.position, 2, previousCameraSize + 3);
            CurrentPlayer = false;

            yield return new WaitForSeconds(2);
            GetComponent<WeaponChanger>().enabled = true;
            _retreating = false;
        }
    }

    public void Live()
    {
        Health = _startHealth;

        if (ReSpawn != null)
            ReSpawn();

        IsDead = false;
        ReSpawn();
    }

    public void UpdateData(string data, object value)
    {
        if (GetComponent<PhotonView>() == null || !IsMine || GetComponent<PhotonView>().owner == null)
            return;

        var someCustomPropertiesToSet = new Hashtable {{data, value}};
        PhotonNetwork.player.SetCustomProperties(someCustomPropertiesToSet);

        if (data == "Team")
        {
            GetComponent<PhotonView>().RPC("SetTeam", PhotonTargets.AllBufferedViaServer, (int) value);
        }
    }

    public object GetData(string data)
    {
        if (GetComponent<PhotonView>() == null || GetComponent<PhotonView>().owner == null)
            return "";

        if (data == "Name" && PhotonNetwork.inRoom)
            return GetComponent<PhotonView>().owner.name;

        return GetComponent<PhotonView>().owner.customProperties[data];
    }

    [PunRPC]
    public void EnableJetpack()
    {
        //Aktiverar jetpacket
        var jetPack = transform.Find("Jetpack");
        jetPack.gameObject.SetActive(true);
    }
}