﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DamageIndicator : MonoBehaviour {

	private GameObject _player;
	private GameObject _canvasText;
	private Quaternion _rotation;
	private GameObject _playerTransform;

	public GameObject _playerObject;
	private float _count;

	void Start () 
	{
		_canvasText = transform.GetChild (0).gameObject;
		_canvasText.GetComponent<Text> ().CrossFadeAlpha (0, 1.5f, true);
		
		_count = 0f;
	    GetComponent<Canvas>().worldCamera = Camera.main;
	}
	
	void Update ()
	{
	    if (_playerObject == null)
	        return;
        
	    Vector3 tempVector = _playerObject.transform.position;
		tempVector.y += 0.30f;
		transform.position = tempVector + new Vector3(0, _count, 0);
		_count += 0.02f;

	}
}
