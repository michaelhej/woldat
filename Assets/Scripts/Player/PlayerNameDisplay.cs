﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerNameDisplay : MonoBehaviour
{
	GameObject MyPlayer;
    private bool _isPlayerSet = false;
    
    private Canvas canvas;
    private Text _text;

    private PlayerManager _movement;
    private PhotonPlayer _photonPlayer;
    private int _networkTeam;

    private bool _playerIsDead;
    private void Awake()
    {
        canvas = GetComponent<Canvas>();
        _text = GetComponentInChildren<Text>();
    }

    void Start()
	{
        Destroy(gameObject);
		canvas.renderMode = RenderMode.WorldSpace;
        canvas.worldCamera = Camera.main;
	}

	public void SetPlayer(GameObject player)
	{
		MyPlayer = player;
        _movement = player.GetComponent<PlayerManager>();
        _photonPlayer = player.GetComponent<PhotonView>() ? player.GetComponent<PhotonView>().owner : PhotonNetwork.player;
        _movement.Death += HidePlayerName;
	    _movement.ReSpawn += ShowPlayerName;
        _isPlayerSet = true;

	    StartCoroutine(UpdateData());
	}

    private void HidePlayerName()
    {
        _playerIsDead = true;
    }

    private void ShowPlayerName()
    {
        _playerIsDead = false;
    }

    private IEnumerator UpdateData()
    {
        while (true)
        { 
            _text.text = _movement.PlayerName;
            _text.color = _movement.PlayerTeam.TeamColor;
            if (PhotonNetwork.inRoom)
            { 
                _networkTeam = _photonPlayer.customProperties.ContainsKey("Team") ? (int)_photonPlayer.customProperties["Team"] : -1;
                ChangeColor(_networkTeam);
            }
            yield return new WaitForSeconds(0.1f);
        }
    }

    private void ChangeColor(int team)
    {
        if (team == 1)
            _movement.PlayerTeam.TeamColor = new Color(255f / 255f, 107f / 255f, 226f / 255f);

        if (team == 2)
            _movement.PlayerTeam.TeamColor = new Color(240f / 255f, 255f / 255f, 0);
    }

    // Update is called once per frame
	void Update ()
	{
	    if (MyPlayer == null)
	    {
	        if (_isPlayerSet)
	            Destroy(gameObject);
	        
            return;
	    }

        if (Camera.main.GetComponent<CameraFollow>().size > 8 || _playerIsDead)
		{
            canvas.enabled = false;
		}
		else
		{
            canvas.enabled = true;
		}
		transform.position = MyPlayer.transform.position + new Vector3(0, 0.35f, 0);
	
	}
}
