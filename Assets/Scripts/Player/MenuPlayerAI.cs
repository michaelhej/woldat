﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;

public class MenuPlayerAI : MonoBehaviour
{

    public float AimAngle = 270;
    public bool Left = false;

    private WeaponController _weaponController;

    private void Awake()
    {
        Woldat.Settings.IsNetwork = false;
    }

    private void Start()
    {
        Woldat.Settings.IsNetwork = false;

        GetComponent<PlayerManager>().IsMine = true;
        GetComponent<PlayerManager>().PlayerName = "Demo";

        if (Left)
            GetComponent<PlayerManager>().SwitchCharacter(2);

        GetComponent<PlayerManager>().Death += Death;
        GetComponent<PlayerManager>().ReSpawn += ReSpawn;

        GetComponent<PlayerManager>().Anim.SetFloat("Speed", 0);
        GetComponent<PlayerManager>().Anim.SetFloat("vSpeed", 0);
        GetComponent<PlayerManager>().Anim.SetBool("Grounded", true);

        if (Left)
            GetComponent<MovementControl>().Flip();

        GetComponent<PlayerManager>().CurrentPlayer = false;
        StartCoroutine(Shooter());
    }

    private void Death()
    {
        StopAllCoroutines();
    }

    private void ReSpawn()
    {
        StopAllCoroutines();
        StartCoroutine(Shooter());
    }

    private IEnumerator Shooter()
    {
        float angle = NextAngle();
        yield return new WaitForSeconds(2);
        while (true)
        {
            //Set weapon
            string wpn = RandomWeapon();
            AmmoController.WeaponAmmo(0)[wpn] = -1; //Menu cannot be out of ammo

            Woldat.Player = gameObject;
            GetComponent<WeaponChanger>().SetWeapon("");
            yield return new WaitForEndOfFrame();

            Woldat.Player = gameObject;
            GetComponent<WeaponChanger>().SetWeapon(wpn);
            yield return new WaitForEndOfFrame();
            _weaponController = GetComponentInChildren<WeaponController>();

            SetAim(angle);

            //Aim...
            yield return new WaitForSeconds(0.5f);
            angle = NextAngle();
            while (Mathf.Abs(angle - SetAim(angle, true)) > 1f)
            {
                yield return new WaitForFixedUpdate();
            }

            //Shoot!
            yield return new WaitForSeconds(Random.Range(0.5f, 1.5f));
            Shoot();
            yield return new WaitForSeconds(Random.Range(1.5f, 2.5f));
        }
    }

    public float NextAngle()
    {
        if (Left)
            return AimAngle + Random.Range(10, 80);
        else
            return AimAngle + Random.Range(10, 80);
    }

    private void Shoot()
    {
        if (_weaponController == null)
            return;

        float force = Random.Range(0.6f, 0.75f);
        _weaponController.Fire(force);
    }

    public float SetAim(float angle, bool lerp = false)
    {
        if (_weaponController == null)
            return angle;

        GameObject weapon = _weaponController.gameObject;

        Vector3 angles = weapon.transform.eulerAngles;

        if (lerp)
            angles.z = Mathf.MoveTowardsAngle(angles.z, angle, 1f);
        else
            angles.z = angle;

        weapon.transform.eulerAngles = angles;
        return weapon.transform.eulerAngles.z;
    }

    private string RandomWeapon()
    {
        List<string> weaponList = new List<string>();
        weaponList.Add("Banooka");
        weaponList.Add("Bazooka");
        weaponList.Add("WpnBanana");
        weaponList.Add("WpnHolyGrenade");
        weaponList.Add("WpnGrenade");
        weaponList.Add("WpnSmokeGrenade");
        weaponList.Add("GrenadeLauncher");
        weaponList.Add("WpnDragonball");
        return weaponList[Random.Range(0, weaponList.Count)];
    }
}