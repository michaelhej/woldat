﻿using UnityEngine;
using System.Collections;

public class NamePlate : MonoBehaviour {

    GameObject MyPlayer;

    private TextMesh _text;
    private PlayerManager _movement;

    private PhotonPlayer _photonPlayer;
    private int _networkTeam;

    private bool _hide;
    private void Awake()
    {
        _text = GetComponent<TextMesh>();
        _text.GetComponent<Renderer>().sortingLayerName = "ForegroundObjects";
    }

	
	// Update is called once per frame
	void Update ()
    {
        if (MyPlayer != null)
            transform.position = MyPlayer.transform.position + new Vector3(0, 0.35f, 0);
	}

    public void SetPlayer(GameObject player)
    {
        if (player.name == "MenuPlayer")
            return;

        MyPlayer = player;
        _movement = player.GetComponent<PlayerManager>();
        _photonPlayer = player.GetComponent<PhotonView>() ? player.GetComponent<PhotonView>().owner : PhotonNetwork.player;
        
        _movement.Death += HidePlayerName;
        _movement.ReSpawn += ShowPlayerName;

        StartCoroutine(UpdateData());
    }

    private void ChangeColor(int team)
    {
        if (team == 1)
            _movement.PlayerTeam.TeamColor = new Color(255f / 255f, 107f / 255f, 226f / 255f);

        if (team == 2)
            _movement.PlayerTeam.TeamColor = new Color(240f / 255f, 255f / 255f, 0);
    }

    private IEnumerator UpdateData()
    {
        while( true )
        {
            if (PhotonNetwork.inRoom && _photonPlayer != null)
            {
                _networkTeam = _photonPlayer.customProperties.ContainsKey("Team") ? (int)_photonPlayer.customProperties["Team"] : -1;
                ChangeColor(_networkTeam);
            }

            _text.text = _movement.PlayerName;
            _text.color = _movement.PlayerTeam.TeamColor;

            yield return new WaitForSeconds(0.1f);
        }
    }

    private void HidePlayerName()
    {
        GetComponent<Renderer>().enabled = false;
    }

    private void ShowPlayerName()
    {
        GetComponent<Renderer>().enabled = true;
    }
}
