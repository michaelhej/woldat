﻿using UnityEngine;
using System.Collections;

public class MovementControl : MonoBehaviour
{

	public float WalkSpeed;
	public float JumpSpeed;

	private float maxVeloxity = 2.3f;

	private Rigidbody2D body;

    private bool grounded;
    private bool _hasDoubleJumped;
    private bool _hasDived;
    private PhysicsMaterial2D _material;
    private bool _lastGrounded;

	private PlayerManager movement;
	void Awake()
	{
		body = GetComponent<Rigidbody2D>();
	    _material = GetComponent<CircleCollider2D>().sharedMaterial;

	}
	void Start ()
	{
		movement = GetComponent<PlayerManager>();
	}
	
    void FixedUpdate()
    {
        bool onGround = Physics2D.OverlapCircle((Vector2)transform.position + new Vector2(0, -0.05f), 0.15f, (1 << 11));

        if (!body.fixedAngle && body.rotation != 0)
		{
		    bool stopSpinning = false;

            body.angularDrag += 0.5f; //Adds drag as long as the player is spinning so it doesn't spin annoyingly long

            if (onGround && body.angularDrag > 10)
                stopSpinning = true;

            if (onGround && body.velocity.sqrMagnitude < 0.25f && Mathf.Abs(body.angularVelocity) < 30)
				stopSpinning = true;
			
			if (onGround && Mathf.Abs(body.velocity.y) < 0.25f && Mathf.Abs(body.velocity.x) < 0.15f)
				stopSpinning = true;

            if (body.velocity.sqrMagnitude < 0.1f)
				stopSpinning = true;

            if (onGround && Mathf.Abs(body.angularVelocity) < 30)
				stopSpinning = true;

			//Stop player from spinning
			if (stopSpinning)
			{
			    body.angularDrag = 2;
				body.rotation = 0;
				transform.localRotation = Quaternion.identity;
				body.fixedAngle = true;
			}
		}

        //Not grounded? Remove friction.
        _material.friction = !grounded ? 0 : 1;
        if (!_lastGrounded && grounded) //Flick off and on for it to work
        {
            GetComponent<CircleCollider2D>().enabled = false;
            GetComponent<CircleCollider2D>().enabled = true;
        }
        _lastGrounded = grounded;

        //Turn off controls if not current
        if (!movement.CurrentPlayer && body.fixedAngle)
			return;

        //Movement
		float move = 0;

        if (body.fixedAngle && !Woldat.DisableInput) //Unable to move while spinning
		{
		    if (grounded)
		    {
                move = movement.GetAxis("Move");
		    }
		    else
		    {
                move = movement.GetAxis("Move") * 0.2f; //Small movement control while in the air
		    }
		}

        move *= WalkSpeed;
		//Calculate max velocity
		move /= (Mathf.Max(body.velocity.x, 1) / maxVeloxity);
		
		float velocity = Mathf.Clamp(
			body.velocity.x + move,
			-maxVeloxity, maxVeloxity);
		
		//Apply velocity
		body.velocity = new Vector2(velocity, body.velocity.y);

		
		//Flip player
		if (move > 0 && !movement.FacingRight)
			Flip ();
		if (move < 0 && movement.FacingRight)
			Flip();

        //Make sure did not stop at some akward angle
        if (body.fixedAngle)
        {
            body.rotation = 0;
            transform.rotation = Quaternion.identity;
        }
	}

	void Update()
	{
        grounded = GetComponent<PlayerManager>().Grounded;

		if (!movement.CurrentPlayer)
			return;

		if (grounded && !Woldat.DisableInput)
		{
		    _hasDoubleJumped = false;
		    _hasDived = false;
            if (movement.GetButtonDown("Jump") && movement.GetAxis("Jump") > 0)
                Jump();
		}
        else if (!grounded && !Woldat.DisableInput)
        {
            if (movement.GetButtonDown("Jump"))
                if (movement.GetAxis("Jump") > 0)
                    DoubleJump();
                if (movement.GetAxis("Jump") < 0)
                    DiveJump();
        }
	}

    //Double jump with 75% of jump force
    private void DoubleJump()
    {
        float yForce = JumpSpeed * 0.75f;

        if (!body.fixedAngle || grounded || _hasDoubleJumped || _hasDived)
            return;

        movement.Anim.SetTrigger("Jump");
        StartCoroutine(Spin(new Vector2(0, yForce)));
    }

    //Dive forward and downwards
    private void DiveJump()
    {
        float xForce = 0;

        if (movement.GetAxis("Move") > .2)
            xForce = 2.2f;
        if (movement.GetAxis("Move") < -.2)
            xForce = -2.2f;

        float yForce = JumpSpeed * -0.2f;

        if (!body.fixedAngle || grounded || _hasDived)
            return;

		movement.Anim.SetTrigger("Jump");
        StartCoroutine(Spin(new Vector2(xForce, yForce)));
    }

    //Roll forward
    private void Roll()
    {
        float xForce = 0;

        if (movement.GetAxis("Move") > 0)
            xForce = 3.5f;
        if (movement.GetAxis("Move") < 0)
            xForce = -3.5f;

        float yForce = JumpSpeed * 0.2f;

        if (!body.fixedAngle || _hasDoubleJumped)
            return;

        StartCoroutine(Spin(new Vector2(xForce, yForce)));
    }

    private IEnumerator Spin(Vector2 jumpForce)
    {

        _hasDoubleJumped = true;
        body.fixedAngle = false;
        body.AddForce(jumpForce, ForceMode2D.Impulse);

        if (jumpForce.y < 0)
            maxVeloxity = 5.5f;

        for (int i = 0; i < 24; i++)
        {
            body.rotation += (movement.FacingRight) ? -15 : 15;
            yield return new WaitForSeconds(0.001f * i);
        }

        maxVeloxity = 2.1f;
        body.rotation = 0;
        body.fixedAngle = true;
    }

    void Jump()
	{
		body.AddForce(new Vector2(0, JumpSpeed), ForceMode2D.Impulse);
		movement.Anim.SetTrigger("Jump");
		grounded = false;
	}

	public void Flip()
	{
	    if (movement == null)
	        return;

        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
		movement.FacingRight = !movement.FacingRight;
	}
}
