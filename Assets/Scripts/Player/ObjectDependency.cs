﻿using UnityEngine;

/// <summary>
///     Destroys object when depent object disappears
/// </summary>
public class ObjectDependency : MonoBehaviour
{
    private Object _dependOn;
    private bool _isSet;

    public void SetObject(Object o)
    {
        _dependOn = o;
        _isSet = true;
    }

    private void Update()
    {
        if (_isSet && _dependOn == null)
            Destroy(gameObject);
    }
}