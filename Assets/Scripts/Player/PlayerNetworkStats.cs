﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class PlayerNetworkStats : MonoBehaviour
{
    public Button BtnTeam1;
    public Button BtnTeam2;

    private Canvas canvas;

    private void Awake()
    {
        canvas = GetComponentInParent<Canvas>();
        canvas.enabled = false;
    }

    public void AssignTeam(int team)
    {
        Woldat.Player.GetComponent<PlayerManager>().UpdateData("Team", team);
        UpdateStats();
    }

    private void UpdateStats()
    {
        foreach (Transform child in transform)
            Destroy(child.gameObject);

        if (!Woldat.Settings.IsNetwork || !PhotonNetwork.inRoom)
            return;

        int playerIndex = 0;
        List<int> teamCount = new List<int>(new int[Woldat.Settings.Teams + 1]); //Fill with 0
        foreach (PhotonPlayer player in PhotonNetwork.playerList)
        {
            GameObject stats = Instantiate(Resources.Load("MenuButtons/PlayerStats"), Vector3.zero, Quaternion.identity) as GameObject;
			stats.transform.SetParent(transform, false);

            int team = player.customProperties.ContainsKey("Team") ? (int)player.customProperties["Team"] : 0;
            if (team == 1)
                stats.transform.localPosition = new Vector3(1, 10 + -20 * teamCount[team], 0);
            else if (team == 2)
                stats.transform.localPosition = new Vector3(1, -150 + -20 * teamCount[team], 0);
            else
                stats.transform.localPosition = new Vector3(1, -320 + 20 * teamCount[team], 0);
            teamCount[team] = teamCount[team] + 1;

            Text[] labels = stats.GetComponentsInChildren<Text>();

            labels[0].text = player.name;
            labels[1].text = player.customProperties["Kills"].ToString();
            labels[2].text = player.customProperties["Deaths"].ToString();
            labels[3].text = player.customProperties["DmgDealt"].ToString();
            labels[4].text = player.customProperties["DmgTaken"].ToString();

            playerIndex++;
        }
        
        int scoreNinjas = PhotonNetwork.room.customProperties.ContainsKey("ScoreTeam1") ? (int)PhotonNetwork.room.customProperties["ScoreTeam1"] : 0;
        int scoreBananas = PhotonNetwork.room.customProperties.ContainsKey("ScoreTeam2") ? (int)PhotonNetwork.room.customProperties["ScoreTeam2"] : 0;
        GameObject.Find("ScoreNinjas").GetComponent<Text>().text = scoreNinjas.ToString();
        GameObject.Find("ScoreBananas").GetComponent<Text>().text = scoreBananas.ToString();

    }

    // Use this for initialization
	void Start ()
	{
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Tab) && Woldat.Settings.IsNetwork)
        {
            UpdateStats();
            canvas.enabled = true;
        }

	    if (Input.GetKeyUp(KeyCode.Tab))
	        canvas.enabled = false;

    }
}
