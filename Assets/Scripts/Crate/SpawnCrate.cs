﻿using UnityEngine;
using System.Collections;

public class SpawnCrate : MonoBehaviour {
	
	public float SpawnInterval;
	public float spawnMinX;
	public float spawnMaxX;
	public float spawnHeight;

	private float _time;
	private float _spawnPositionX;

	SpawnPlane sp;
	BackgroundScroller bs;

	// Use this for initialization
	void Start () 
	{
		_time = SpawnInterval;
	}
	
	// Update is called once per frame
	void Update () 
	{
		_time += Time.deltaTime;
		if (_time >= SpawnInterval) 
		{
			//slumpar X positionen där lådan ska skapas
			_spawnPositionX = Random.Range (spawnMinX, spawnMaxX);
			_time = 0f;

			// if planeposition.x = _spawnPositionX do: ELLER bs.transform.position = = _spawnPositionX do: .... gameObject createdCrate

			//skapar lådan på en random plats på kartan
			Instantiate (Resources.Load("SpawningObject/DropDownCrate"), new Vector3 (_spawnPositionX, spawnHeight, 0), Quaternion.identity);
			//Debug.Log ("Spawning rotation: " + createdCrate.transform.rotation.eulerAngles);
		}
	}
}
