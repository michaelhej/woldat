﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CrateHandler : MonoBehaviour {

	public string Parachute;

	public AudioClip CrateLandingAudio;
	public AudioClip RefilAmmoAudio;
	
	private GameObject _crateObject;
	private GameObject _parachuteObject;
	private Vector2 _cratePosition;
	private Vector3 _tempVector;
	
	private ParticleSystem _particleSystemSmoke;
	private bool _particleSystemPlayed;

	private bool _hasPlayed;

	private bool _stopRaycast;
    private bool _grounded;
    private Vector3 _lastPosition;

	// Use this for initialization
	void Start () 
	{
		_stopRaycast = false;
		GetComponent<Rigidbody2D> ().fixedAngle = true;
		_crateObject = GetComponent<Rigidbody2D> ().gameObject;
		_particleSystemSmoke = _crateObject.GetComponentInChildren<ParticleSystem>();
	    _lastPosition = transform.position;
        InvokeRepeating("CheckPosition", 4, 1);
	}

    private void CheckPosition()
    {
        if (Vector3.Distance(transform.position, _lastPosition) < 0.1f)
            _grounded = true;

        _lastPosition = transform.position;
    }

    //Player collide with a Crate
	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "Player")
		{
            //Players are not able to take a crate passivly
		    if (other.gameObject.GetComponent<PlayerManager>() == null)
		        return;
		    //if (!other.gameObject.GetComponent<PlayerManager>().CurrentPlayer)
		    //    return;

			_stopRaycast = true;
			if(_crateObject.transform.childCount > 1)
				Destroy (_crateObject.transform.GetChild (1).gameObject);

			if(!_particleSystemSmoke.isPlaying && !_particleSystemPlayed)
			{
				//Play crate smoke effect
				_particleSystemSmoke.transform.position = transform.position;
				_particleSystemSmoke.Play();
				_particleSystemPlayed = true;
				_crateObject.GetComponent<Rigidbody2D>().isKinematic = true;
				_crateObject.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
				_crateObject.GetComponent<BoxCollider2D>().enabled = false;
                
                //Stopp glow effect
                _crateObject.GetComponentsInChildren<ParticleSystem>()[1].Stop();

				//Slay pick-up sound
				GetComponent<AudioSource>().clip = RefilAmmoAudio;
				GetComponent<AudioSource>().Play();
				_hasPlayed = true;


                //Add crate content to player
                if (other.gameObject.GetComponent<PhotonView>().isMine)
                { 
					bool gotJetpack = false;
					//Change at receiving jetpack
					if(other.gameObject.transform.Find("Jetpack").gameObject.GetActive() == false)
					{
						float jetpackRandom = Random.Range(1, 10);

						if(jetpackRandom > 8 || Woldat.KjellKod) //If cheating, always receive jetpack (has all weapons anyway)
						{
							Woldat.PrintChat("<color=brown>You got a <b>Jetpack</b></color>");
                            if (PhotonNetwork.inRoom && other.gameObject.GetComponent<PhotonView>().viewID > 0)
							    other.gameObject.GetComponent<PhotonView>().RPC("EnableJetpack", PhotonTargets.All);
                            else
                                other.gameObject.GetComponent<PlayerManager>().EnableJetpack();
							gotJetpack = true;
						}
					}

                    //No jetpack, receive random ammo
					if(!gotJetpack)
					{
					    string[] allWeapons = GetAllAvaliableWeapons();

					    float randomNumber = Random.Range(0, 112);
					    int inventoryId = other.gameObject.GetComponent<PlayerManager>().InventoryId;

					    //SmokeGrenade
					    if(randomNumber < 15)
						    AmmoController.AddAmmo(inventoryId, allWeapons[2], 5);
					    //Sniper
					    else if(randomNumber < 29)
						    AmmoController.AddAmmo(inventoryId, allWeapons[3], 10);
					    //Bazooka
					    else if(randomNumber < 41)
						    AmmoController.AddAmmo(inventoryId, allWeapons[4], 10);
					    //Dynamite
					    else if(randomNumber < 52)
						    AmmoController.AddAmmo(inventoryId, allWeapons[5], 5);
					    //FlameThrower
					    else if(randomNumber < 62)
						    AmmoController.AddAmmo(inventoryId, allWeapons[6], 20);
					    //Minigun
					    else if(randomNumber < 70)
						    AmmoController.AddAmmo(inventoryId, allWeapons[7], 50);
					    //Dragonball
					    else if(randomNumber < 77)
	                        AmmoController.AddAmmo(inventoryId, allWeapons[8], 5);
	                    //Banana
	                    else if (randomNumber < 81)
	                        AmmoController.AddAmmo(inventoryId, allWeapons[9], 5);
					    //Lazer
					    else if(randomNumber < 88)
						    AmmoController.AddAmmo(inventoryId, allWeapons[10], 10);
					    //HolyGrenade
					    else if(randomNumber < 92)
						    AmmoController.AddAmmo(inventoryId, allWeapons[11], 3);
					    //Banooka
					    else if(randomNumber < 96)
						    AmmoController.AddAmmo(inventoryId, allWeapons[12], 3);
					    //CrazyCat
					    else if(randomNumber < 98)
						    AmmoController.AddAmmo(inventoryId, allWeapons[13], 2);
					    //Airstrike
					    else if(randomNumber < 101)
                            AmmoController.AddAmmo(inventoryId, allWeapons[14], 2);
                        //GrenadeLauncher
                        else if (randomNumber < 103)
                            AmmoController.AddAmmo(inventoryId, allWeapons[15], 5);
                        //RayBay
                        else if (randomNumber < 105)
                            AmmoController.AddAmmo(inventoryId, allWeapons[16], 5);
                        //Shotgun
                        else if (randomNumber < 112)
                            AmmoController.AddAmmo(inventoryId, allWeapons[17], 5);
					}
                }
			}
			Destroy(_crateObject, 2);
		}

		if (other.gameObject.tag == "World")
		{
		    _grounded = true;
			if(_crateObject.transform.childCount > 1)
				Destroy (_crateObject.transform.GetChild (1).gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "World") 
		{
			if(_crateObject.transform.GetChild (1).gameObject != null)
			{
				Destroy (_crateObject.transform.GetChild (1).gameObject);
			}
		}
	}
	
	void Update ()
	{
		//Raycasting for deploying parachute
		RaycastHit2D farDistance = Physics2D.Raycast(transform.position, -Vector2.up, 4.5f, (1 << 11));
        RaycastHit2D closeDistance = Physics2D.Raycast(transform.position, -Vector2.up, 1.2f, (1 << 11));
        RaycastHit2D nearlyGrounded = Physics2D.Raycast(transform.position, -Vector2.up, 0.3f, (1 << 11));

        //Landing
		if (nearlyGrounded.collider != null && !_stopRaycast) 
		{
			if(_crateObject.transform.childCount > 1)
				Destroy (_crateObject.transform.GetChild (1).gameObject);

			if (!GetComponent<AudioSource>().isPlaying && !_hasPlayed) {
				GetComponent<AudioSource>().clip = CrateLandingAudio;
				GetComponent<AudioSource>().Play();
				_hasPlayed = true;
			}
			_crateObject.GetComponent<Rigidbody2D>().velocity = new Vector2 (0f, 0f);
		}
		//Remove parachute
		else if ((closeDistance.collider != null && !_stopRaycast) || _grounded || GetComponent<Rigidbody2D>().velocity.sqrMagnitude < 0.1f) 
		{
			if(_crateObject.transform.childCount > 1)
				Destroy (_crateObject.transform.GetChild (1).gameObject);

            _crateObject.GetComponent<Rigidbody2D>().drag = 0;
		}
		//Deploy chute
		else if (farDistance.collider != null && _parachuteObject == null && !_stopRaycast)
        {
			_parachuteObject = Instantiate(Resources.Load ("SpawningObject/" + Parachute), transform.position, transform.rotation) as GameObject;

            _parachuteObject.transform.parent = transform;
            _parachuteObject.transform.localPosition = new Vector3(0.03f, 0.86f, 0);

			Rigidbody2D crateBody = GetComponent<Rigidbody2D> ();
		    crateBody.drag = 20;
		}
	}

	string[] GetAllAvaliableWeapons()
	{
		Dictionary<string, int> ammo = new Dictionary<string, int>();
		ammo = AmmoController.WeaponAmmo(0);
		Dictionary<string, int>.KeyCollection ammoKeys = ammo.Keys;
		string[] allWeapons = new string[ammo.Count];
		ammoKeys.CopyTo(allWeapons, 0);

		return allWeapons;
	}	
}