﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {
	
	[Header("World boundaries")]
	public float WorldMinY = -4;
	public float WorldMaxY = 13;
	
	public float WorldMinX = -16;
    public float WorldMaxX = 26;
    public float TargetSize;

	public float size {
		get
		{
			if (GetComponent<Camera>().orthographic)
				return GetComponent<Camera>().orthographicSize;
			else
				return GetComponentInParent<Camera>().fieldOfView / 10;
		}
		set
		{
			if (GetComponent<Camera>().orthographic)
				GetComponent<Camera>().orthographicSize = value;
			else
				GetComponentInParent<Camera>().fieldOfView = value * 10;
		}
	}

	private Transform _target;
	private Transform _defaultTarget;
	private Vector3 _targetPosition = Vector3.zero;

    private bool _targeting;
    private float _velx;
    private float _vely;

    private float _maxTargetSize = 14;
    private float _minTargetSize = 2;

    private float _previousTargetSize;
    private bool _quickTargeting;
    private bool _firstTarget = true;
    private float _zoomSpeed = 2;

	Vector3 position;
	void Awake()
	{
	    TargetSize = size;
		_defaultTarget = _target;

		_maxTargetSize = Mathf.Min(WorldMaxY - WorldMinY, WorldMaxX - WorldMinX);

        _maxTargetSize = Mathf.Clamp(_maxTargetSize, 1, 14);
	 	size = _maxTargetSize;
		//if (!GetComponent<Camera>().orthographic)
		//	MaxTargetSize *= 1.3f;
	}

    public void ReBound()
    {
        _maxTargetSize = WorldMinY + WorldMaxY;
        _maxTargetSize = Mathf.Clamp(_maxTargetSize, 1, 14);
    }

    void Start()
	{
		transform.position = ClampCamera(transform.position);
	}

	public void SetMainTarget(Transform _target)
	{
		this._target = _defaultTarget = _target;
		_targeting = true;
	}
	
	public void SetDefaultTarget()
	{
		_targeting = true;
		_target = _defaultTarget;
	}

	public void SetSize(float _tSize)
	{
		TargetSize = _tSize;
	}

	public void ZoomOutOnPosition(Vector3 position, float seconds, float _cameraSize = -1)
	{
		_previousTargetSize = TargetSize;

		if (_cameraSize == -1)
			TargetSize = _maxTargetSize;
		else
			TargetSize = _cameraSize;

		_targeting = true;
		_quickTargeting = true;
		_targetPosition = position;

		Invoke("ZoomBack", seconds);
	}

	void ZoomBack()
	{
		_targetPosition = Vector3.zero;
		_targeting = true;
		TargetSize = _previousTargetSize;
		_quickTargeting = false;
	}

	//Follow projectiles, WORMS-mode
	public void OverrideTarget(Transform _target, bool _targeting = false)
	{
		if (Woldat.Settings.PlayMode == WoldatSettings.WoldatPlayMode.Soldat)
			return;

		if (_target == null)
			_target = _defaultTarget;

		this._target = _target;
		this._targeting = _targeting;
	}

	public void GoToTarget(Transform _target)
	{
		OverrideTarget(_target);
	}

    private void FitMultiplayer()
    {
        Vector2 pos1 = Woldat.LocalPlayer[0].transform.position;
        Vector2 pos2 = Woldat.LocalPlayer[1].transform.position;

        Vector3 camPos = new Vector3((pos1.x + pos2.x) / 2, (pos1.y + pos2.y) / 2, -10f);
        Vector2 camSize = new Vector2(Mathf.Abs(pos1.x - pos2.x), Mathf.Abs(pos1.y - pos2.y));

        camSize.x += 3f;
        camSize.y += 3f;
        camSize.x *= 1.25f;
        camSize.y *= 1.25f;
        camSize.x = Mathf.Clamp(camSize.x, 4, 250);
        
		Camera camera = GetComponent<Camera>();

        var frustumHeight = Mathf.Max(camSize.x/camera.aspect, camSize.y);
        var fieldOfView = 2.0f * Mathf.Atan(frustumHeight * 0.5f / 10) * Mathf.Rad2Deg;

        camera.fieldOfView = Mathf.MoveTowards(camera.fieldOfView, fieldOfView, Time.deltaTime * 2);

        camPos = ClampCamera(camPos);

        transform.position = Vector3.MoveTowards(transform.position, camPos, Time.deltaTime * 4);
        //transform.position = camPos;
    }

    void Update()
	{

        if (Woldat.LocalPlayer != null && Woldat.LocalPlayer.Count > 1)
	    {
	        FitMultiplayer();
	        return;
	    }
	    float zoomAxis = 0;

	    if (Woldat.Settings.InputMode == WoldatSettings.WoldatInputMode.GamePad)
	        zoomAxis = Input.GetAxis("LR2") * -1;
        else
            zoomAxis = Input.GetAxis("Mouse ScrollWheel");


        if (zoomAxis > 0)
        {
            TargetSize -= 0.1f * _zoomSpeed;
        }
        if (zoomAxis < 0)
        {
            TargetSize += 0.1f * _zoomSpeed;
        }

        if (Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            _quickTargeting = true;
            TargetSize = Mathf.Min(3, _maxTargetSize);
        }
        if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            _quickTargeting = true;
            TargetSize = _maxTargetSize;
        }

		TargetSize = Mathf.Clamp(TargetSize, _minTargetSize, _maxTargetSize);

		if (_target == null)
			return;

        //Get target position
		Vector3 position = _target.position;
		position.z = transform.position.z;
		
		position = ClampCamera(position);

		if (_targetPosition != Vector3.zero)
		{
			position.x = _targetPosition.x;
			position.y = _targetPosition.y;
		}

		if (_targeting && !_firstTarget && transform.position != position) //Targeting projectile
		{
			//Fast tareting in Y-axis
			position.x = Mathf.SmoothDamp(transform.position.x, position.x, ref _velx, 0.75f);
			position.y = Mathf.SmoothDamp(transform.position.x, position.x, ref _vely, 0.25f);

			transform.position = position;
			//transform.position = Vector3.SmoothDamp(transform.position, position, ref vel, 0.75f);
			//transform.position = Vector3.MoveTowards(transform.position, position, Time.deltaTime * 10);

			float distance = Vector3.Distance(position, transform.position);
			if (distance < 0.1f)
				_targeting = false;

		}
		else
		{
			transform.position = position;//Vector3.SmoothDamp(transform.position, position, ref vel, 0.25f);
			_firstTarget = false;
		}
		//Debug.Log("Camera n: " + transform.position + " -> " + position + ", size: " + size + ", targeting: " + targeting + ", distance: " + Vector3.Distance(position, transform.position));

		//Zoom out on jump
		if (_target == _defaultTarget || true)
		{
			float _size = size;
			float speed = 1;
			if (_quickTargeting)
				speed = 2;

			if (_target.GetComponent<Rigidbody2D>().velocity.y > 1f)
			{
				_size = Mathf.MoveTowards(_size, Mathf.Clamp(_maxTargetSize + 2, _minTargetSize, _maxTargetSize), Time.deltaTime * speed);
			}
            else if (_size != TargetSize)
			{
				_size = Mathf.MoveTowards(_size, TargetSize, Time.deltaTime * Mathf.Max(Mathf.Abs(TargetSize - _size), 1f) * speed);
			}
			
			size = _size;
		}

		transform.position = ClampCamera(transform.position);
	}

	Vector3 ClampCamera(Vector3 position)
	{
		Camera camera = GetComponent<Camera>();
		float distance = 10;

		
		//Clamp camera-position in world plane
		float cameraMinX;
		float cameraMaxX;
		float cameraMinY;
		float cameraMaxY;

	calculate:
		if (camera.orthographic)
		{
			cameraMinX = WorldMinX + size * camera.aspect;
			cameraMaxX = WorldMaxX - size * camera.aspect;
			
			cameraMinY = WorldMinY + size;
			cameraMaxY = WorldMaxY - size;
		}
		else
		{
			float frustumHeight = 2.0f * distance * Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad);
			float frustumWidth = frustumHeight * camera.aspect;

			cameraMinX = WorldMinX + frustumWidth / 2;
			cameraMaxX = WorldMaxX - frustumWidth / 2;
			
			cameraMinY = WorldMinY + frustumHeight / 2;
			cameraMaxY = WorldMaxY - frustumHeight / 2;
		}

		int x = 0;
		if (  ( (cameraMinX > cameraMaxX) || (cameraMinY > cameraMaxY) )  && x < 100) 
		{
			_maxTargetSize -= 0.1f;
			size = _maxTargetSize;
			x++;
			goto calculate;
		}
		
		position.x = Mathf.Clamp(position.x, cameraMinX, cameraMaxX);
		position.y = Mathf.Clamp(position.y, cameraMinY, cameraMaxY);

		return position;
	}
}
