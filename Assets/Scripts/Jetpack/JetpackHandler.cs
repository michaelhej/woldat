﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class JetpackHandler : MonoBehaviour {

	public float Fuel;
	public float Consumtion;
	public float Power;
	public float RechargeSpeed;

	private GameObject _player;
	private Rigidbody2D _rigidBody;
	private PlayerManager _playerMovement;
	private GameObject _canvasObject;
	private RectTransform _foreGround;

	private GameObject Engine1;
	private GameObject Engine2;
	private ParticleSystem _engine1PSystem;
	private ParticleSystem _engine2PSystem;

	private bool _jetpackActive;
	private float XScale;
	private float OneFuel;
	private Vector3 tempVector;


	// Use this for initialization
	void Start ()
	{
	    _engine1PSystem = GetComponentsInChildren<ParticleSystem>()[0];
        _engine2PSystem = GetComponentsInChildren<ParticleSystem>()[1];

		_player = transform.parent.gameObject;
		_rigidBody = _player.GetComponent<Rigidbody2D>();
		_playerMovement = _player.GetComponent<PlayerManager>();

		_canvasObject = Instantiate(Resources.Load("Jetpack/JetpackFuel")) as GameObject;

		_foreGround = _canvasObject.transform.GetChild(1).GetComponent<RectTransform>();
		XScale = _foreGround.localScale.x;
		OneFuel = XScale / Fuel;
	}

	void FixedUpdate()
	{
		//om spelaren är i luften och håller ned hopp-knappen kan jetpacket användas
        if (!_playerMovement.Grounded && _rigidBody.velocity.y < 2f && (Input.GetMouseButton(1) || Input.GetButton("Jump") || Input.GetKey(KeyCode.W))) 
		{
			UseJetpack ();
		}
        else if (!(_playerMovement.GetButton("Jump")) && Fuel < 100.0f) 
		{
			Fuel += RechargeSpeed;
			tempVector = _foreGround.localScale;
			tempVector.x -= OneFuel * -RechargeSpeed; 
			_foreGround.localScale = tempVector;
		}
	}

	void UseJetpack()
	{
        _engine1PSystem.Play();
		_engine2PSystem.Play();
		if (Fuel >= Consumtion) 
		{
			Vector2 velocity = _rigidBody.velocity;
			float v = 3.8f;
            if (velocity.y > v)
                v = velocity.y;

            velocity.y = Mathf.Lerp(velocity.y, v, Time.deltaTime * 15f);
            velocity.x += _playerMovement.GetAxis("Move") * 3;
            _rigidBody.velocity = velocity;

            if (!Woldat.KjellKod)
            { 
			    Fuel -= Consumtion;
                tempVector = _foreGround.localScale;
			    tempVector.x -= OneFuel * Consumtion; 
			    _foreGround.localScale = tempVector;
            }
		}
		_engine1PSystem.Stop ();
		_engine2PSystem.Stop ();
	}
}
